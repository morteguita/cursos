## Comandos PowerShell de Windows para compilar
```PowerShell
$env:GOOS = "linux"
$env:CGO_ENABLED = "0"
$env:GOARCH = "amd64"
go build -o main main.go
```

## Comandos de AWS

Se crean las políticas, la función basada en el archivo zip y se invoca la función lambda

```PowerShell
aws iam create-role --role-name lambda-ex --assume-role-policy-document file://trust-policy.json

aws iam attach-role-policy --role-name lambda-ex --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole

aws lambda create-function --function-name go-lambda2 --zip-file fileb://function.zip --handler main --runtime go1.x --role arn:aws:iam::334569527123:role/lambda-ex

aws lambda invoke --function-name go-lambda2 --cli-binary-format raw-in-base64-out --payload '{\"What is your name?\": \"Manuel\", \"How old are you?\": 36}' output.txt
```