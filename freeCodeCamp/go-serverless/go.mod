module gitlab.com/morteguita/cursos/freeCodeCamp/go-serverless

go 1.18

require (
	github.com/aws/aws-lambda-go v1.28.0
	github.com/aws/aws-sdk-go v1.43.27
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	github.com/urfave/cli/v2 v2.4.0 // indirect
)
