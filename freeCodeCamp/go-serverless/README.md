# Golang-Serverless-Project
API Gateway + DynamoDB + Lambda Complete serveless stack

---

## URL API AWS
https://dvi5prvsv7.execute-api.us-east-1.amazonaws.com/staging

---

## Comandos Linux
curl --header "Content-Type: application/json" --request POST --data '{"email": "morteguita@gmail.com", "firstName": "Manuel", "lastName": "Ortega"}' https://dvi5prvsv7.execute-api.us-east-1.amazonaws.com/staging

curl -X GET https://dvi5prvsv7.execute-api.us-east-1.amazonaws.com/staging

curl -X GET https://dvi5prvsv7.execute-api.us-east-1.amazonaws.com/staging\?email\=morteguita@gmail.com

curl --header "Content-Type: application/json" --request PUT --data '{"email": "morteguita@gmail.com", "firstName": "Manuel Alejandro", "lastName": "Ortega Henao"}' https://dvi5prvsv7.execute-api.us-east-1.amazonaws.com/staging

curl -X DELETE https://dvi5prvsv7.execute-api.us-east-1.amazonaws.com/staging\?email\=morteguita@gmail.com

---

## Comandos Windows
& 'C:\Program Files\curl\bin\curl.exe' --header "Content-Type: application/json" --request POST --data '{\"email\": \"morteguita@gmail.com\", \"firstName\": \"Manuel\", \"lastName\": \"Ortega\"}' https://dvi5prvsv7.execute-api.us-east-1.amazonaws.com/staging

& 'C:\Program Files\curl\bin\curl.exe' -X GET https://dvi5prvsv7.execute-api.us-east-1.amazonaws.com/staging

& 'C:\Program Files\curl\bin\curl.exe' -X GET https://dvi5prvsv7.execute-api.us-east-1.amazonaws.com/staging?email=morteguita@gmail.com

& 'C:\Program Files\curl\bin\curl.exe' --header "Content-Type: application/json" --request PUT --data '{\"email\": \"morteguita@gmail.com\", \"firstName\": \"Manuel Alejandro\", \"lastName\": \"Ortega Henao\"}' https://dvi5prvsv7.execute-api.us-east-1.amazonaws.com/staging

& 'C:\Program Files\curl\bin\curl.exe' -X DELETE https://dvi5prvsv7.execute-api.us-east-1.amazonaws.com/staging?email=morteguita@gmail.com

---

## Comandos PowerShell de Windows para compilar
```PowerShell
$env:GOOS = "linux"
$env:CGO_ENABLED = "0"
$env:GOARCH = "amd64"
go build -o main main.go
```