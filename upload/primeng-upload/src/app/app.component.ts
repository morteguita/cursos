import { Component } from '@angular/core';
import { MessageService } from 'primeng/api';
import { environment } from './../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title: string = 'primeng-upload';
  uploadedFiles: any[] = [];
  apiURL: string = environment.apiURL + '/multiple-upload';

  constructor(private messageService: MessageService) {}

  onUpload(event: any) {
    for(let file of event.files) {
      this.uploadedFiles.push(file);
    }

    this.messageService.add({severity: 'info', summary: 'Files Uploaded', detail: ''});
  }

  onError(event: any) {
    let msg = typeof event.error.error.message === 'undefined' ? event.error.message : event.error.error.message
    this.messageService.add({severity: 'error', summary: msg, detail: ''});
  }
}
