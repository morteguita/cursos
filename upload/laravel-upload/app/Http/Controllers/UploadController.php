<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Item;
use App\Models\ItemDetails;

class UploadController extends Controller
{
    public function uploadSubmit(Request $request)
    {
        $code = Response::HTTP_BAD_REQUEST;
        $msg = 'Files are required';

        $this->validate($request, [
            'files' => 'required',
        ]);

        if($request->hasFile('files'))
        {
            $allowedfileExtension = ['jpg', 'png', 'gif', 'jpeg'];
            $files = $request->file('files');

            $items = Item::create(array_merge($request->all(), ['name' => 'Upload '.time()]));

            foreach($files as $file)
            {
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);
                
                if($check)
                {
                    $filename = $file->store('photos', 'local');
                    ItemDetails::create([
                        'item_id' => $items->id,
                        'filename' => $filename
                    ]);
                }
                else
                {
                    $msg = 'Sorry, wrong extension';
                    return new Response(['message' => $msg], $code);
                }
            }
            
            $code = Response::HTTP_OK;
            $msg = "Upload successful";
        }

        return new Response(['message' => $msg], $code);
    }
}
