import { Router } from 'express';
import apiExample from './api/index.js';

const apiRouter = Router();

apiRouter.use('/example', apiExample);

export { apiRouter };
