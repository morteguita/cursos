import { Router } from 'express';

const api = Router();

api.get('/', async (req, res) => {
	try {
		res.json({ message: "TODO" });
	} catch (err) {
		res.status(500).json({ error: err.message });
	}
});

export default api;
