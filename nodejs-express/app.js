import express from 'express';
import cors from 'cors';
import { logger } from './middleware/logEvents.js';
import errorHandler from './middleware/errorHandler.js';
import { apiRouter } from './routes/api.js';

const app = express();

app.use(logger);
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use('/api', apiRouter);
app.use(errorHandler);

export default app;