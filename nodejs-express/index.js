/*
  "type": "module" in package.json uses ES modules (imports and exports)
  Example: import express from "express"

  Not having a type, uses require
  Example: const express = require('express');
*/
import app from './app.js';
const _PORT = 8000;

app.listen(_PORT, () => {
	console.log('Server running on port ' + _PORT);
});
