package main

import (
	"fmt"
)

type Car struct {
	Model string
}

func main() {
	c := make(chan *Car, 3)

	go func() {
		c <- &Car{"Chevrolet"}
		c <- &Car{"Mazda"}
		c <- &Car{"Renault"}
		c <- &Car{"Ferrari"}
		close(c)
	}()

	for i := range c {
		fmt.Println(i.Model)
	}
}
