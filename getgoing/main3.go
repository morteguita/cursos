package main

import (
	"fmt"
	"strings"
)

func main() {
	//Strings
	m1 := "my name"
	m2 := "name"

	fmt.Println(strings.Contains(m1, m2))
	fmt.Println(strings.ReplaceAll(m1, m2, "my"))
	fmt.Println(strings.Split(m1, " "))
	fmt.Println(strings.ToUpper(m1))
	fmt.Println(m1 + " " + m2)
}
