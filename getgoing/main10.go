package main

import (
	"fmt"
)

func main() {
	//var <name> chan <datatype>
	var ch chan int
	c := make(chan int)

	fmt.Println(ch, c)

	//send in a goroutine
	go func() {
		c <- 1
	}()

	//sniff
	val := <-c
	fmt.Println(val)

	//send in a goroutine
	go func() {
		c <- 2
	}()

	val = <-c
	fmt.Println(val)

	fmt.Println(c)
}
