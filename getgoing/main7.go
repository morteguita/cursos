package main

import (
	"fmt"
)

func main() {
	//Control structures
	f := true
	flag := &f

	if flag == nil {
		fmt.Println("Value is NIL")
	} else if *flag {
		fmt.Println("TRUE")
	} else {
		fmt.Println("FALSE")
	}

	for i := 10; i < 15; i++ {
		fmt.Println(i)
	}

	//For infinito
	/*for {
		fmt.Println("heavy")
	}*/

	arr := []string{"Manuel", "Alejandro", "Ortega", "Henao"}
	for i, value := range arr {
		fmt.Println(i, value)
	}

	mymap := make(map[string]interface{})
	mymap["name"] = "Alejandro"
	mymap["Age"] = 36

	for k, v := range mymap {
		fmt.Printf("Key: %s, Value: %v", k, v)
		fmt.Println()
	}

	for i := 0; i < 10; i++ {
		if i%2 == 0 {
			continue
		}

		fmt.Println(i)
	}

	flag2 := true
	for i := 0; i < 10; i++ {
		if i%2 == 0 {
			flag2 = false
			break
		} else if i == 1 {
			continue
		}

		fmt.Println(i)
	}

	fmt.Println(flag2)

	day := "Fri"
	switch day {
	case "Fri":
		fmt.Println("WEEKEND")
	case "Mon", "Tue", "Wed":
		fmt.Println("BORING!")
	default:
		fmt.Println("WTF")
	}

	switch {
	case day == "Fri":
		fmt.Println("WEEKEND")
		break
	case day != "Fri":
		fmt.Println("BORING")
		break
	}
}
