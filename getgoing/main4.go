package main

import (
	"fmt"
)

func todo() {
	arr := []int{1, 2, 3, 4}
	arr2 := []string{"hi", "my", "name"}

	arr2 = append(arr2, "is", "Manuel")
	fmt.Println(arr)
	fmt.Println(arr2)
}

func swap(m1, m2 *int) {
	var temp int
	temp = *m2
	*m2 = *m1
	*m1 = temp
}

func main() {
	//Arrays
	todo()

	//Pointers
	m := 2
	ptr := &m
	fmt.Println(ptr)  //Memory address
	fmt.Println(*ptr) //Value

	m1, m2 := 2, 3
	fmt.Println(m1, m2)

	swap(&m1, &m2)
	fmt.Println(m1, m2)
}
