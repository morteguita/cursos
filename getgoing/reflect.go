package main

import (
	"fmt"
	"reflect"
	"unsafe"
)

func main() {
	a := []int{1, 2, 3}
	hdr := (*reflect.SliceHeader)(unsafe.Pointer(&a))
	data := *(*[3]int)(unsafe.Pointer(hdr.Data))
	fmt.Println(data)
	a = change(a)
	hdr = (*reflect.SliceHeader)(unsafe.Pointer(&a))
	newData := *(*[4]int)(unsafe.Pointer(hdr.Data))
	fmt.Println(newData)
}

func change(a []int) []int {
	a = append(a, 5)
	return a
}
