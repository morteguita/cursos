package main

import (
	"fmt"
	"sync"
)

func main() {
	//Wait groups
	wg := &sync.WaitGroup{}

	//Mutexes (critical section)
	/*mut := &sync.Mutex{}
	mut.Lock()
	mut.Unlock()*/

	//Add, done and wait
	wg.Add(2)

	go func() {
		fmt.Println("Hello")
		wg.Done()
	}()

	go func() {
		fmt.Println("World")
		wg.Done()
	}()

	fmt.Println("Fin")
	wg.Wait()
	fmt.Println("Exit")
}
