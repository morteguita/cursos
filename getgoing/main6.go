package main

import (
	"fmt"
)

type Car interface {
	Drive()
	Stop()
}

type Lambo struct {
	LamboModel string
}

func NewModel(arg string) Car {
	return &Lambo{arg}
}

type Chevy struct {
	ChevyModel string
}

func (l *Lambo) Drive() {
	fmt.Println("Lamborghini " + l.LamboModel + " on the road...")
}

func (l *Lambo) Stop() {
	fmt.Println("Lamborghini " + l.LamboModel + " is stopping...")
}

func (c *Chevy) Drive() {
	fmt.Println("Chevrolet " + c.ChevyModel + " on the road...")
}

func Anything(anything interface{}) {
	fmt.Println(anything)
}

func main() {
	//Interfaces I
	l := Lambo{"Diablo"}
	c := Chevy{"Camaro"}

	l.Drive()
	c.Drive()

	//Interfaces II
	Anything(2.455)
	Anything("Manuel")
	Anything(struct{}{})
	Anything(false)

	mymap := make(map[string]interface{})
	mymap["name"] = "Alejandro"
	mymap["Age"] = 36
	Anything(mymap)
}
