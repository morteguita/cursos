package main

import (
	"fmt"
)

type Car struct {
	Name    string
	Age     int
	ModelNo int
}

func (c Car) Print() {
	fmt.Println(c)
}

func (c Car) Drive() {
	fmt.Println("Driving...")
}

func (c Car) GetName() string {
	return c.Name
}

func main() {
	//Structures
	c := Car{
		Name:    "Chevrolet",
		Age:     3,
		ModelNo: 2021,
	}
	c.Print()
	c.Drive()
	fmt.Println(c.GetName())
}
