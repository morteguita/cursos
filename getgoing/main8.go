package main

import (
	"fmt"
	"time"
)

func heavy() {
	for {
		time.Sleep(time.Second * 1)
		fmt.Println("heavy")
	}
}

func superHeavy() {
	for {
		time.Sleep(time.Second * 2)
		fmt.Println("superHeavy")
	}
}

func main() {
	go heavy()
	go superHeavy()
	fmt.Println("Fin")
	select {}
}
