package main

import "fmt"

type Numero struct {
	Valor int
}

func setVal(a *Numero, val int) {
	a.Valor = val

	fmt.Println("Changed values 1")
	fmt.Println("A: ", *a)

	setVal2(a, 4)
}

func setVal2(a *Numero, val int) {
	a.Valor = val
}

func main() {
	a := Numero{Valor: 1}
	//b := &a

	fmt.Println("Init values")
	fmt.Println("A: ", a)

	setVal(&a, 2)

	fmt.Println("Changed values 2")
	fmt.Println("A: ", a)
}
