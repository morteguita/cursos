import { render } from '@testing-library/react';
import React from 'react';

import HomePage from '../index';

describe('Home Page tests', () => {
  it('renders main component', () => {
    const { getByTestId } = render(<HomePage />);
    expect(getByTestId('home-page-main-container')).toBeTruthy();
  });
});
