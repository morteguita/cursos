import { render } from '@testing-library/react';
import React from 'react';

import LoanSimulator from '../index';

describe('Other Page tests', () => {
  it('renders main component', () => {
    const { getByTestId } = render(<LoanSimulator />);
    expect(getByTestId('other-page-main-container')).toBeTruthy();
  });
});
