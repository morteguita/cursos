import fs from "fs";
import path from "path";
import matter from "gray-matter";

const postsFolder = path.join(process.cwd(), "posts");

export function getPostFiles() {
  const postFiles = fs.readdirSync(postsFolder);
  return postFiles;
}

export function getAllPosts() {
  const postFiles = getPostFiles();
  const allPosts = postFiles.map((fileName) => {
    return getPostData(fileName);
  });

  const sortedPosts = allPosts.sort((a, b) => (a.date > b.date ? -1 : 1));

  return sortedPosts;
}

export function getPostData(postIdentifier) {
  const slug = postIdentifier.replace(/\.md$/, "");
  const filePath = path.join(postsFolder, `${slug}.md`);
  const fileContent = fs.readFileSync(filePath, "utf-8");
  const { data, content } = matter(fileContent);

  const postData = {
    slug,
    ...data,
    content,
  };

  return postData;
}

export function getFeaturedPosts() {
  const allPosts = getAllPosts();
  const featuredPosts = allPosts.filter((post) => post.isFeatured);

  return featuredPosts;
}
