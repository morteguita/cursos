import { MongoClient } from "mongodb";

function createConnectionString() {
  const {
    MONGODB_PROTOCOL,
    MONGODB_SERVER,
    MONGODB_PORT,
    MONGODB_DB,
    MONGODB_USER,
    MONGODB_PASSWORD,
    MONGODB_OPTIONS,
  } = process.env;

  const username =
    MONGODB_USER && MONGODB_PASSWORD
      ? `${MONGODB_USER}:${MONGODB_PASSWORD}@`
      : "";
  const port = MONGODB_PORT ? `:${MONGODB_PORT}` : "";
  const options = MONGODB_OPTIONS ? `?${MONGODB_OPTIONS}` : "";

  const connectionString = `${MONGODB_PROTOCOL}://${username}${MONGODB_SERVER}${port}/${MONGODB_DB}${options}`;
  return connectionString;
}

export async function connectDB() {
  const connectionString = createConnectionString();
  const client = new MongoClient(connectionString);
  await client.connect();
  return client;
}

export async function insert(client, collection, data) {
  const db = client.db();
  let result = null;

  if (Array.isArray(data)) {
    result = await db.collection(collection).insertMany(data);
  } else {
    result = await db.collection(collection).insertOne(data);
  }

  return result;
}

export async function find(client, collection, query = {}, options = {}) {
  const db = client.db();
  let result = await db.collection(collection).find(query);

  if (options.sort) result = result.sort(options.sort);
  if (options.array) result = result.toArray();

  return result;
}

export function disconnectDB(client) {
  client.close();
}
