---
title: Getting started with NextJS - Session 2
date: "2023-07-15"
image: getting-started-nextjs.png
excerpt: NextJS is a React framework for production development - It makes building fullstack React apps and sites a breeze and ships with built-in SSR.
isFeatured: false
---

# This is a title

This is some regular text with a [link](https://google.com/)
