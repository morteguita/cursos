import ReactMarkdown from "react-markdown";
import { PrismLight as SyntaxHighlighter } from "react-syntax-highlighter";
import atomDark from "react-syntax-highlighter/dist/cjs/styles/prism/atom-dark";
import js from "react-syntax-highlighter/dist/cjs/languages/prism/javascript";
import css from "react-syntax-highlighter/dist/cjs/languages/prism/css";
import Image from "next/image";

import classes from "./post-content.module.css";
import PostHeader from "./post-header";

SyntaxHighlighter.registerLanguage("javascript", js);
SyntaxHighlighter.registerLanguage("css", css);

function PostContent(props) {
  const { post } = props;
  const imagePath = getImagePath(post.slug, post.image);

  const customRenderers = {
    p(paragraph) {
      const { node } = paragraph;
      const element = node.children[0];

      if (element.tagName === "img") {
        const imageRender = getImagePath(post.slug, element.properties.src);

        return (
          <div className={classes.image}>
            <Image
              src={imageRender}
              alt={element.properties.alt}
              width={600}
              height={300}
            />
          </div>
        );
      }

      return <p>{paragraph.children}</p>;
    },
    code(code) {
      const { className, children } = code;
      const language = className.split("-")[1]; // className is something like language-js => We need the "js" part here

      return (
        <SyntaxHighlighter
          style={atomDark}
          language={language}
          children={children}
        />
      );
    },
    a(link) {
      const { href, children } = link;
      return (
        <a href={href} target="_blank">
          {children}
        </a>
      );
    },
  };

  return (
    <article className={classes.content}>
      <PostHeader title={post.title} image={imagePath} />
      <ReactMarkdown components={customRenderers}>{post.content}</ReactMarkdown>
    </article>
  );
}

export default PostContent;

function getImagePath(slug, imageUrl) {
  const imagePath = `/images/posts/${slug}/${imageUrl}`;
  return imagePath;
}
