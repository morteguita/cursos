import { useEffect, useState } from "react";

import classes from "./contact-form.module.css";
import Notification from "../ui/notification";

function ContactForm() {
  const [enteredEmail, setEnteredEmail] = useState("");
  const [enteredName, setEnteredName] = useState("");
  const [enteredMessage, setEnteredMessage] = useState("");
  const [requestStatus, setRequestStatus] = useState();

  useEffect(() => {
    if (
      requestStatus &&
      (requestStatus.status === "success" || requestStatus.status === "error")
    ) {
      const timer = setTimeout(() => {
        setRequestStatus(null);
      }, 4000);

      return () => clearTimeout(timer);
    }
  }, [requestStatus]);

  const handleSendMessage = async (event) => {
    event.preventDefault();

    const formData = {
      email: enteredEmail,
      name: enteredName,
      message: enteredMessage,
    };

    setRequestStatus({
      status: "pending",
      title: "Sending message...",
      message: "Your message is on its way",
    });

    try {
      await sendContactData(formData);

      setRequestStatus({
        status: "success",
        title: "Success!",
        message: "Message sent successfully!",
      });

      setEnteredEmail("");
      setEnteredName("");
      setEnteredMessage("");
    } catch (error) {
      setRequestStatus({
        status: "error",
        title: "Error!",
        message: error.message || "Something went wrong",
      });
    }
  };

  return (
    <section className={classes.contact}>
      <h1>How can I help you?</h1>
      <form className={classes.form} onSubmit={handleSendMessage}>
        <div className={classes.controls}>
          <div className={classes.control}>
            <label htmlFor="email">Your Email</label>
            <input
              type="email"
              name="email"
              id="email"
              required
              value={enteredEmail}
              onChange={(event) => setEnteredEmail(event.target.value)}
            />
          </div>
          <div className={classes.control}>
            <label htmlFor="name">Your Name</label>
            <input
              type="text"
              name="name"
              id="name"
              required
              value={enteredName}
              onChange={(event) => setEnteredName(event.target.value)}
            />
          </div>
        </div>
        <div className={classes.control}>
          <label htmlFor="message">Your Message</label>
          <textarea
            name="message"
            id="message"
            rows={5}
            required
            value={enteredMessage}
            onChange={(event) => setEnteredMessage(event.target.value)}
          ></textarea>
        </div>
        <div className={classes.actions}>
          <button>Send Message</button>
        </div>
      </form>
      {requestStatus && <Notification notification={requestStatus} />}
    </section>
  );
}

export default ContactForm;

async function sendContactData(contactDetails) {
  const response = await fetch("/api/contact", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(contactDetails),
  });

  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.message || "Something went wrong");
  }
}
