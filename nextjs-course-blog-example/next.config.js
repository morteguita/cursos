const { PHASE_DEVELOPMENT_SERVER } = require("next/constants");

module.exports = (phase) => {
  if (phase === PHASE_DEVELOPMENT_SERVER) {
    return {
      env: {
        MONGODB_PROTOCOL: "mongodb",
        MONGODB_SERVER: "localhost",
        MONGODB_PORT: "27017",
        MONGODB_DB: "nextjs-course",
        MONGODB_USER: "",
        MONGODB_PASSWORD: "",
        MONGODB_OPTIONS: "",
      },
    };
  }

  return {
    env: {
      MONGODB_PROTOCOL: "mongodb",
      MONGODB_SERVER: "localhost",
      MONGODB_PORT: "27017",
      MONGODB_DB: "nextjs-course",
      MONGODB_USER: "",
      MONGODB_PASSWORD: "",
      MONGODB_OPTIONS: "",
    },
  };
};
