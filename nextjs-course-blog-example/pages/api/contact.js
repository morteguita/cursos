import { connectDB, disconnectDB, insert } from "../../util/db-util";

async function handler(req, res) {
  if (req.method === "POST") {
    const { email, name, message } = req.body;

    if (
      !email ||
      !email.includes("@") ||
      !name ||
      name.trim() === "" ||
      !message ||
      message.trim() === ""
    ) {
      res.status(422).json({ message: "Invalid input" });
      return;
    }

    let client = null;
    const collection = "contact";
    const newMessage = { email, name, message };

    try {
      client = await connectDB();
    } catch (err) {
      res
        .status(500)
        .json({ message: `Could not connect to database: ${err.message}` });
      return;
    }

    try {
      const insertedData = await insert(client, collection, newMessage);
      newMessage.id = insertedData.insertedId;

      res
        .status(201)
        .json({ message: "Message saved successfully", comment: newMessage });
    } catch (err) {
      res
        .status(500)
        .json({ message: `Could not save message: ${err.message}` });
    }

    disconnectDB(client);
  }
}

export default handler;
