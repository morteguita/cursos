import { Fragment } from "react";
import Head from "next/head";

import AllPosts from "../../components/posts/all-posts";
import { getAllPosts } from "../../util/posts-util";

function AllPostsPage(props) {
  return (
    <Fragment>
      <Head>
        <title>All my posts</title>
        <meta
          name="description"
          content="A list of all programming-related tutorials and posts!"
        />
      </Head>
      <AllPosts posts={props.posts} />
    </Fragment>
  );
}

export default AllPostsPage;

export const getStaticProps = async (context) => {
  const allPosts = getAllPosts();

  return {
    props: { posts: allPosts },
  };
};
