import { Fragment } from "react";
import Head from "next/head";

import PostContent from "../../components/posts/post-detail/post-content";
import { getPostFiles, getPostData } from "../../util/posts-util";

function PostDetailPage(props) {
  return (
    <Fragment>
      <Head>
        <title>{props.post.title}</title>
        <meta name="description" content={props.post.excerpt} />
      </Head>
      <PostContent post={props.post} />
    </Fragment>
  );
}

export default PostDetailPage;

export const getStaticProps = async (context) => {
  const { slug } = context.params;
  const post = getPostData(slug);

  return {
    props: { post: post },
    revalidate: 600,
  };
};

export const getStaticPaths = async () => {
  const allPosts = getPostFiles();
  const slugs = allPosts.map((post) => post.replace(/\.md$/, ""));

  return {
    paths: slugs.map((slug) => ({ params: { slug: slug } })),
    fallback: "blocking",
  };
};
