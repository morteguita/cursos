import { Component, OnInit } from '@angular/core';
import { Operacion } from './operacion.model';
import { Operaciones } from './operaciones.model';

@Component({
  selector: 'app-calculadora',
  templateUrl: './calculadora.component.html',
  styleUrls: ['./calculadora.component.css']
})
export class CalculadoraComponent {
  titulo: string = "App Calculadora en Angular";
  operacionesPermitidas: Operaciones[] = [
    new Operaciones(0, 'Suma', '+', 'Sumar'),
    new Operaciones(1, 'Resta', '-', 'Restar'),
    new Operaciones(2, 'Multiplicación', '*', 'Multiplicar'),
    new Operaciones(3, 'División', '/', 'Dividir'),
  ];
  operacion: string = "-";
  resultado: number = 0;

  realizarOperacion(operacionForm: Operacion): void {
    const operacionRealizada = this.operacionesPermitidas.filter(f => f.id === operacionForm.operador);

    if(operacionRealizada.length > 0){
      this.operacion = operacionRealizada[0].nombre;
  
      switch(operacionRealizada[0].signo){
        case '+':
          this.resultado = operacionForm.numero1 + operacionForm.numero2;
          break;
        case '-':
          this.resultado = operacionForm.numero1 - operacionForm.numero2;
          break;
        case '*':
          this.resultado = operacionForm.numero1 * operacionForm.numero2;
          break;
        case '/':
          this.resultado = (operacionForm.numero2 != 0) ? operacionForm.numero1 / operacionForm.numero2 : 0;
          break;
      }
    } else {
      this.operacion = "Operación no válida";
      this.resultado = 0;
    }
  }
}
