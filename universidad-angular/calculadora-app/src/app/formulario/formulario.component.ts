import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Operacion } from '../calculadora/operacion.model';
import { Operaciones } from '../calculadora/operaciones.model';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent { 
  @Input() operacionesPermitidas: Operaciones[];
  @Output() operacionForm = new EventEmitter<Operacion>();
  numero1: number = 0;
  numero2: number = 0;

  operar(idOperacion: number): void {
    this.operacionForm.emit(new Operacion(this.numero1, this.numero2, idOperacion));
  }
}
