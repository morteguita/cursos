import { Component } from "@angular/core";

@Component({
    selector: 'app-personas',
    templateUrl: './personas.component.html',
    styleUrls: ['./personas.component.css']
})

export class PersonasComponent{
    deshabilitar: boolean = false;
    mensaje: string = "";
    titulo: string = "N/A";
    mostrarMensaje: boolean = false;

    agregarPersona() {
        this.mensaje = "Persona agregada";
        this.mostrarMensaje = true;
    }
}