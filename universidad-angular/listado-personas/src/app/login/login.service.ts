import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import * as firebase from 'firebase';

@Injectable()
export class LoginService {
    token: string = '';

    constructor(private router: Router){}
    
    login(email: string, password: string): void {
        firebase.auth().signInWithEmailAndPassword(email, password).then(
            response => {
                firebase.auth().currentUser?.getIdToken().then(
                    token => {
                        this.token = token;
                        this.router.navigate(['/']);
                    }
                )
            }
        );
    }

    getIdToken(): string {
        return this.token;
    }

    estaAutenticado(): boolean {
        return typeof this.token !== undefined && this.token != null && this.token != '';
    }

    logout(): void {
        firebase.auth().signOut().then(
            () => {
                this.token = '';
                this.router.navigate(['login']);
            }
        );
    }
}