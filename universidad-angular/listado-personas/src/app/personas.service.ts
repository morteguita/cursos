import { EventEmitter, Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { DataServices } from "./data.services";
import { Persona } from "./persona.model";

@Injectable()
export class PersonaService {
    private personas: Persona[] = [];
    saludar = new EventEmitter<string>();

    constructor(private dataService: DataServices){}

    setPersonas(personas: Persona[]): void{
        this.personas = personas;
    }

    obtenerPersonas(): Observable<Persona[]>{
        return this.dataService.cargarPersonas();
    }

    obtenerPersona(indice: number): Persona{
        return this.personas[indice];
    }

    agregarPersona(persona: Persona): void{
        if(this.personas == null){
            this.personas = [];
        }

        this.personas.push(persona);
        this.dataService.guardarPersonas(this.personas);
    }

    modificarPersona(indice: number, persona: Persona): void {
        const personaOrig: Persona = this.obtenerPersona(indice);
        personaOrig.nombre = persona.nombre;
        personaOrig.apellido = persona.apellido;
        this.dataService.modificarPersona(indice, persona);
    }

    eliminarPersona(indice: number): void{
        this.personas.splice(indice, 1);
        this.dataService.eliminarPersona(indice);
        this.modificarPersonas(); //Se guarda el arreglo de nuevo para regenerar los indices
    }

    modificarPersonas(): void {
        if(this.personas != null){
            this.dataService.guardarPersonas(this.personas);
        }
    }
}