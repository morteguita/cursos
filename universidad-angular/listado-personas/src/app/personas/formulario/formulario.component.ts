import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Persona } from '../../persona.model';
import { PersonaService } from '../../personas.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit{
  nombreInput: string = '';
  apellidoInput: string = '';
  indice: number;
  modoEdicion: number;

  constructor(private personasService: PersonaService, private router: Router, private route: ActivatedRoute){
    this.personasService.saludar.subscribe((indice: number) => alert('El indice es: ' + indice));
  }

  ngOnInit(): void {
    this.indice = this.route.snapshot.params['id'];
    this.modoEdicion = +this.route.snapshot.queryParams['modoEdicion']; //+ para hacer cast a entero

    if(this.modoEdicion != null && this.modoEdicion === 1){
      const persona: Persona = this.personasService.obtenerPersona(this.indice);
      this.nombreInput = persona.nombre;
      this.apellidoInput = persona.apellido;
    }
  }

  guardarPersona(): void {
    if(this.nombreInput != '' && this.apellidoInput != ''){
      const persona = new Persona(this.nombreInput, this.apellidoInput);
      if(this.modoEdicion != null && this.modoEdicion === 1){
        this.personasService.modificarPersona(this.indice, persona);
      } else {
        this.personasService.agregarPersona(persona);
      }
      this.router.navigate(['personas']);
    } else {
      alert('Datos incompletos');
    }
  }

  eliminarPersona(): void {
    if(this.modoEdicion != null && this.modoEdicion === 1){
      this.personasService.eliminarPersona(this.indice);
    }
    this.router.navigate(['personas']);
  }
}
