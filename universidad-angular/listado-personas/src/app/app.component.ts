import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { LoginService } from './login/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  titulo = 'Listado de Personas';

  constructor(private loginService: LoginService){}

  ngOnInit(): void {
    firebase.initializeApp({
      apiKey: "AIzaSyCFcdAbHVKVQV6WBkBIcdUwRhJ9SmyeizA",
      authDomain: "listado-personas-c2c18.firebaseapp.com",
    });
  }

  estaAutenticado(): boolean {
    return this.loginService.estaAutenticado();
  }

  salir(): void {
    this.loginService.logout();
  }
}
