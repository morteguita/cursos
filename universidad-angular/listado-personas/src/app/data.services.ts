import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginService } from './login/login.service';
import { Persona } from './persona.model';

@Injectable()
export class DataServices {
    servidor: string = 'https://listado-personas-c2c18-default-rtdb.firebaseio.com/datos{{indice}}.json?auth={{token}}';

    constructor(private httpClient: HttpClient, private loginService: LoginService){}

    cargarPersonas(): Observable<Persona[]> {
        const token: string = this.loginService.getIdToken();
        const url: string = this.servidor.replace(`{{indice}}`, ``).replace(`{{token}}`, `${token}`);
        return this.httpClient.get<Persona[]>(url);
    }

    guardarPersonas(personas: Persona[]): void {
        const token: string = this.loginService.getIdToken();
        const url: string = this.servidor.replace(`{{indice}}`, ``).replace(`{{token}}`, `${token}`);

        //Tipo observable, por eso se requiere el subscribe
        this.httpClient.put(url, personas).subscribe(
            response => {
                console.log('Respuesta de guardarPersonas', response);
            },
            error => {
                console.log('Error en guardarPersonas', error);
            }
        );
    }

    modificarPersona(indice: number, persona: Persona): void {
        const token: string = this.loginService.getIdToken();
        const url: string = this.servidor.replace(`{{indice}}`, `/${indice}`).replace(`{{token}}`, `${token}`);
        
        //Tipo observable, por eso se requiere el subscribe
        this.httpClient.put(url, persona).subscribe(
            response => {
                console.log('Respuesta de modificarPersona', response);
            },
            error => {
                console.log('Error en modificarPersona', error);
            }
        );
    }

    eliminarPersona(indice: number): void {
        const token: string = this.loginService.getIdToken();
        const url: string = this.servidor.replace(`{{indice}}`, `/${indice}`).replace(`{{token}}`, `${token}`);

        //Tipo observable, por eso se requiere el subscribe
        this.httpClient.delete(url).subscribe(
            response => {
                console.log('Respuesta de eliminarPersona', response);
            },
            error => {
                console.log('Error en eliminarPersona', error);
            }
        );
    }
}