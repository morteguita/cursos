import { Egreso } from "./egreso.model";

export class EgresoService {
    private egresos: Egreso[] = [
        new Egreso('Renta', 900),
        new Egreso('Comida', 450),
        new Egreso('Ropa', 210)
    ];

    getEgresos(): Egreso[] {
        return this.egresos;
    }

    addEgreso(descripcion: string, valor: number) {
        const egreso: Egreso = new Egreso(descripcion, valor);
        const egresoFind: Egreso = this.getEgresos().find(e => e.descripcion === descripcion);

        if(typeof egresoFind === 'undefined'){
            this.getEgresos().push(egreso);
        } else {
            egresoFind.valor += egreso.valor;
        }
    }

    deleteEgreso(egreso: Egreso){
        const indice: number = this.getEgresos().indexOf(egreso);
        this.getEgresos().splice(indice, 1);
    }
}