import { Component, Input, OnInit } from '@angular/core';
import { Egreso } from './egreso.model';
import { EgresoService } from './egreso.service';

@Component({
  selector: 'app-egreso',
  templateUrl: './egreso.component.html',
  styleUrls: ['./egreso.component.css']
})
export class EgresoComponent implements OnInit {
  @Input() ingresoTotal: number;
  egresos: Egreso[] = [];

  constructor(private egresoServicio: EgresoService) { }

  ngOnInit(): void {
    this.egresos = this.egresoServicio.getEgresos();
  }

  eliminarRegistro(egreso: Egreso): void {
    this.egresoServicio.deleteEgreso(egreso);
  }

  calcularPorcentaje(egreso: Egreso): number {
    return this.ingresoTotal > 0 ? egreso.valor / this.ingresoTotal : 0;
  }
}
