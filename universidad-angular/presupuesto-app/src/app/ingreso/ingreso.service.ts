import { Ingreso } from "./ingreso.model";

export class IngresoService {
    private ingresos: Ingreso[] = [
        new Ingreso('Salario', 4000),
        new Ingreso('Venta de coche', 500)
    ];

    getIngresos(): Ingreso[]{
        return this.ingresos;
    }

    addIngreso(descripcion: string, valor: number) {
        const ingreso: Ingreso = new Ingreso(descripcion, valor);
        const ingresoFind: Ingreso = this.getIngresos().find(i => i.descripcion === descripcion);

        if(typeof ingresoFind === 'undefined'){
            this.getIngresos().push(ingreso);
        } else {
            ingresoFind.valor += ingreso.valor;
        }
    }

    deleteIngreso(ingreso: Ingreso){
        const indice: number = this.getIngresos().indexOf(ingreso);
        this.getIngresos().splice(indice, 1);
    }
}