import { Component, OnInit } from '@angular/core';
import { EgresoService } from '../egreso/egreso.service';
import { IngresoService } from '../ingreso/ingreso.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {
  tipo: string;
  descripcionInput: string;
  valorInput: number;
  mostrarError: boolean;
  mensajeError: string;

  constructor(private ingresoServicio: IngresoService, private egresoServicio: EgresoService) { }

  ngOnInit(): void {
    this.tipo = 'ingresoOperacion';
    this.mostrarError = false;
    this.mensajeError = '';
  }

  tipoOperacion(evento): void {
    this.tipo = evento.target.value;
  }

  agregarRegistro(): void {
    this.mostrarError = false;

    if(this.valorInput > 0){
      if(this.tipo === 'ingresoOperacion'){
        this.ingresoServicio.addIngreso(this.descripcionInput, this.valorInput);
      }
  
      if(this.tipo === 'egresoOperacion'){
        this.egresoServicio.addEgreso(this.descripcionInput, this.valorInput);
      }
  
      this.limpiarFormulario();
    } else {
      this.mostrarError = true;
      this.mensajeError = 'Datos incorrectos/inválidos';
    }
  }

  private limpiarFormulario(): void {
    this.descripcionInput = '';
    this.valorInput = 0;
  }
}
