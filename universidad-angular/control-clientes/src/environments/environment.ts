// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firestore: {
    apiKey: "AIzaSyA4nKXoFnr7MBxQSsSkOtWrWT3eTJbRsuQ",
    authDomain: "control-clientes-b6421.firebaseapp.com",
    projectId: "control-clientes-b6421",
    storageBucket: "control-clientes-b6421.appspot.com",
    messagingSenderId: "249445776873",
    appId: "1:249445776873:web:7bea7ea0318d8cb0ef9f78"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
