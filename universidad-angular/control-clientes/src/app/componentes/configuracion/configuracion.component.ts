import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Configuracion } from 'src/app/modelo/configuracion.model';
import { ConfiguracionServicio } from 'src/app/servicios/configuracion.service';

@Component({
  selector: 'app-configuracion',
  templateUrl: './configuracion.component.html',
  styleUrls: ['./configuracion.component.css']
})
export class ConfiguracionComponent implements OnInit {
  permitirRegistro: boolean = false;

  constructor(private router: Router, private configuracionServicio: ConfiguracionServicio) { }

  ngOnInit(): void {
    this.configuracionServicio.getConfiguracion().subscribe(
      (configuracion: Configuracion | undefined) => {
        this.permitirRegistro = configuracion ? (configuracion.permitirRegistro ? configuracion.permitirRegistro : false) : false
      }
    );
  }

  guardar(): void {
    const configuracion = {permitirRegistro: this.permitirRegistro};
    this.configuracionServicio.editConfiguracion(configuracion);
    this.router.navigate(['/']);
  }
}
