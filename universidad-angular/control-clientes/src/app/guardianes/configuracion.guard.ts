import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { CanActivate, Router } from "@angular/router";
import { Observable } from "rxjs";
import { ConfiguracionServicio } from "../servicios/configuracion.service";
import { map } from "rxjs/operators";

@Injectable()
export class ConfiguracionGuard implements CanActivate {
    constructor(private router: Router, private afAuth: AngularFireAuth, private configuracionServicio: ConfiguracionServicio){}

    canActivate(): Observable<boolean> {
        return this.configuracionServicio.getConfiguracion().pipe(
            map(configuracion => {
                if(configuracion){
                    if(configuracion.permitirRegistro){
                        return true;
                    } else {
                        this.router.navigate(['/login']);
                        return false;
                    }
                } else {
                    this.router.navigate(['/login']);
                    return false;
                }
            })
        )
    }
}