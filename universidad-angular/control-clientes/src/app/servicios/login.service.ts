import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import firebase from "firebase";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable()
export class LoginService {
    constructor(private authService: AngularFireAuth) {}

    login(email: string, password: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.authService.signInWithEmailAndPassword(email, password).then(
                datos => resolve(datos),
                error => reject(error)
            )
        });
    }

    logout(): void {
      this.authService.signOut();
    }

    getAuth(): Observable<firebase.User | null> {
        return this.authService.authState.pipe(
            map(auth => auth)
        );
    }

    register(email: string, password: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.authService.createUserWithEmailAndPassword(email, password).then(
                datos => resolve(datos),
                error => reject(error)
            )
        });
    }
}