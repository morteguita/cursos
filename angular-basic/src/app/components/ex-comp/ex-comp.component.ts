import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ex-comp',
  templateUrl: './ex-comp.component.html',
  styleUrls: ['./ex-comp.component.css']
})
export class ExCompComponent implements OnInit {
  age: number;
  names: Array<string>;

  constructor() {
    this.age = 16;
    this.names = ['Manuel', 'Alexander', 'Peter', 'Robert'];
  }

  ngOnInit(): void {
    console.log('Component loaded!');
  }

  addYear(): void {
    this.age += 1;
  }

  decYear(): void {
    this.age -= 1;
  }
}
