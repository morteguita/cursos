import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ExCompComponent } from './components/ex-comp/ex-comp.component';

@NgModule({
  declarations: [
    AppComponent,
    ExCompComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [], //Services
  bootstrap: [AppComponent]
})
export class AppModule { }
