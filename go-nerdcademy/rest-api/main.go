package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

var client *http.Client

type CatFact struct {
	Fact   string `json:"fact"`
	Length int    `json:"length"`
}

type RandomUser struct {
	Results []UserResult
}

type UserResult struct {
	Name    UserName `json:"name"`
	Email   string
	Picture UserPicture
}

type UserName struct {
	Title string
	First string
	Last  string
}

type UserPicture struct {
	Large     string
	Medium    string
	Thumbnail string
}

func GetRandomUser() {
	url := "https://randomuser.me/api/?inc=name,email,picture"
	var randomUser RandomUser

	err := GetJson(url, &randomUser)
	if err != nil {
		fmt.Printf("Error getting user: %s\n", err.Error())
	} else {
		user := randomUser.Results[0]
		fmt.Printf("User: %s %s %s\nEmail: %s\nThumbnail: %s\n", user.Name.Title, user.Name.First, user.Name.Last, user.Email, user.Picture.Thumbnail)
	}
}

func GetCatFact() {
	url := "https://catfact.ninja/fact"
	var catFact CatFact

	err := GetJson(url, &catFact)
	if err != nil {
		fmt.Printf("Error getting Cat Fact: %s\n", err.Error())
	} else {
		fmt.Printf("A super interesting Cat Fact: %s (%d)\n", catFact.Fact, catFact.Length)
	}
}

func GetJson(url string, target interface{}) error {
	resp, err := client.Get(url)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	return json.NewDecoder(resp.Body).Decode(target)
}

func main() {
	client = &http.Client{Timeout: 10 * time.Second}

	GetCatFact()

	catFact := CatFact{
		Fact:   "A random Cat Fact",
		Length: 17,
	}

	jsonStr, err := json.Marshal(catFact)
	if err != nil {
		fmt.Printf("Error marshaling: %s\n", err.Error())
	} else {
		fmt.Printf("Test JSON: %s\n", string(jsonStr))
	}

	fmt.Println("======================================================================")

	GetRandomUser()
}
