package controller

import (
	"fmt"
	"log"
	"net/http"

	controller "simple-blog/app/controller/blog"

	"github.com/gorilla/mux"
)

var router *mux.Router

func initHandlers() {
	router.HandleFunc("/api/posts", controller.GetAllPosts).Methods("GET")
	router.HandleFunc("/api/post/{id}", controller.GetPost).Methods("GET")
	router.HandleFunc("/api/post", controller.CreatePost).Methods("POST")
	router.HandleFunc("/api/post", controller.UpdatePost).Methods("PUT")
	router.HandleFunc("/api/post", controller.UpdatePost).Methods("PATCH")
	router.HandleFunc("/api/post/{id}", controller.DeletePost).Methods("DELETE")
}

func Start() {
	router = mux.NewRouter()
	port := ":3200"

	initHandlers()
	fmt.Printf("Router initialized and listening on %s\n", port)
	log.Fatal(http.ListenAndServe(port, router))
}
