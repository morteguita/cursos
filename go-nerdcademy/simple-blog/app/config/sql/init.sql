CREATE TABLE IF NOT EXISTS posts (
    id INT(11) NOT NULL AUTO_INCREMENT,
    title VARCHAR(64),
    content TEXT,
    PRIMARY KEY (id)
);

INSERT INTO posts (title, content) VALUES
('Hello World', 'The obligatory hello world post...'),
('Another Post', 'Yet another blog post about something exciting'),
('Third Post', 'Lets play...');