package main

import (
	"simple-blog/app/controller"
	"simple-blog/app/model"
)

func main() {
	model.Init()
	controller.Start()
}
