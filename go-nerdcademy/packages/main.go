package main

import (
	"fmt"

	"example.com/packages/util"
)

func main() {
	greeting := fmt.Sprintf("Hello, %s", "Manuel")
	fmt.Println(greeting)

	word := "greeting"
	fmt.Printf("The length of %s is %d", word, util.StringLength(word))
	fmt.Println()

	fmt.Println(util.GetGreeting())
}
