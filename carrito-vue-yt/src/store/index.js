import { createStore } from 'vuex'

export default createStore({
    state: {
        productos: [],
        carrito: {}
    },
    mutations: {
        setProducto(state, payload) {
            //Se calcula el impuesto
            for(const prod of payload){
                prod.total = prod.precio + (prod.precio * (prod.impuesto / 100))
                prod.total = prod.total.toFixed(2) //Se colocan 2 decimales
                state.productos.push(prod)
            }
        },
        setAgregarProducto(state, payload) {
            state.carrito[payload.id] = payload
        },
        vaciarCarro(state) {
            state.carrito = {}
        },
        aumentar(state, id) {
            state.carrito[id].cantidad = state.carrito[id].cantidad + 1
        },
        disminuir(state, id) {
            state.carrito[id].cantidad = state.carrito[id].cantidad - 1

            //Se quita el producto del carro si no tiene cantidad
            if (state.carrito[id].cantidad === 0){
                delete state.carrito[id]
            }
        }
    },
    actions: {
        async fetchData({ commit }) {
            try {
                const res = await fetch('api.json')
                const data = await res.json()
                commit('setProducto', data)
            } catch (error) {
                console.log(error)
            }
        },
        agregarCarrito({ commit, state }, producto) {
            producto.cantidad = state.carrito.hasOwnProperty(producto.id) ? state.carrito[producto.id].cantidad + 1 : 1
            commit('setAgregarProducto', producto)
        }
    },
    getters: {
        totalCantidad(state) {
            return Object.values(state.carrito).reduce((acum, { cantidad }) => acum + cantidad, 0)
        },
        totalPrecio(state) {
            return Object.values(state.carrito).reduce((acum, { total, cantidad }) => acum + (total * cantidad), 0)
        }
    },
    modules: {
    }
})