# carrito-vue-yt

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.vscode-typescript-vue-plugin).

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### URL

http://separate-month.surge.sh/

### Deploy

surge --domain separate-month.surge.sh

## Vuex

Vuex (libreria para gestionar los estados de una app) tiene algo llamado store, que es un objeto global que contiene los cuatro elementos principales que Vuex necesita para administrar el estado de una aplicación:

* State: Es el objeto JavaScript que contiene los datos de la aplicación.
* Getters: Son funciones que devuelven los datos contenidos en el estado.
* Mutations: Son funciones síncronas que actualizan el estado, y son el único mecanismo existente para hacerlo.
* Actions: Son funciones asíncronas que actualizan el estado a través de una mutación existente. Pueden invocar mutaciones múltiples, otras acciones y admitir operaciones asincrónicas.