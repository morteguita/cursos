package main

import "fmt"

/*type persona struct {
	nombre   string
	apellido string
	edad     int
}

func (p persona) saludar(saludo string) {
	fmt.Printf("Hola %s %s", p.nombre, p.apellido)
	fmt.Println()
	fmt.Printf("%s amigo", saludo)
	fmt.Println()
}

func (p persona) cumple() int {
	return p.edad + 1
}*/

func main() {
	frutas := []string{"Pera", "Manzana", "Naranja"}
	fmt.Println(frutas[1])

	frutas = append(frutas, "Banano", "Melocotón")
	fmt.Println(frutas)

	for i := 0; i < len(frutas); i++ {
		fmt.Println(frutas[i])
	}

	for i := 0; i < len(frutas); i++ {
		if frutas[i] == "Melocotón" {
			fmt.Println(frutas[i], "encontrado")
		}
	}

	frutas[0] = "Sandía"
	fmt.Println(frutas)

	/*pers1 := persona{nombre: "Alejandro", apellido: "Ortega", edad: 36}
	pers2 := persona{nombre: "James", apellido: "Bond", edad: 52}

	fmt.Println(pers1)
	fmt.Printf("Me llamo %s %s y tengo %d años", pers1.nombre, pers1.apellido, pers1.edad)
	fmt.Println()
	fmt.Printf("Me llamo %s %s y tengo %d años", pers2.nombre, pers2.apellido, pers2.edad)
	fmt.Println()

	pers2.edad = 61
	fmt.Printf("Me llamo %s %s y ahora tengo %d años", pers2.nombre, pers2.apellido, pers2.edad)
	fmt.Println()

	pers1.saludar("Hello")

	pers1.edad = pers1.cumple()
	fmt.Printf("Me llamo %s %s y ahora tengo %d años", pers1.nombre, pers1.apellido, pers1.edad)
	fmt.Println()*/
}
