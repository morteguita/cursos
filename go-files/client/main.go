package main

import (
	"compress/gzip"
	"fmt"
	"io"
	"log"
	"net"
	"os"
)

func main() {
	uploadFile("./prueba.txt", "localhost:4040")
}

func uploadFile(srcFile, serverAddr string) {
	// connect to server
	conn, err := net.Dial("tcp", serverAddr)
	CheckError(err)
	defer conn.Close()

	in, err := os.Open(srcFile)
	if err != nil {
		log.Fatal(err)
	}
	pr, pw := io.Pipe()
	gw, err := gzip.NewWriterLevel(pw, 7)
	fmt.Println(&pr, &gw)
	CheckError(err)
	go func() {
		n, err := io.Copy(gw, in)
		gw.Close()
		pw.Close()
		log.Printf("copied %v %v", n, err)
	}()
	_, err = io.Copy(conn, pr)
	CheckError(err)
}

func CheckError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
		os.Exit(1)
	}
}
