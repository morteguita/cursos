package main

import (
	"compress/gzip"
	"io"
	"log"
	"net"
	"os"
)

func main() {
	// Create a listener on a random port.
	listener, err := net.Listen("tcp", "127.0.0.1:")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Server listening on: " + listener.Addr().String())
	done := make(chan struct{})
	go func() {
		defer func() { done <- struct{}{} }()
		for {
			conn, err := listener.Accept()
			if err != nil {
				log.Println(err)
				return
			}
			go func(c net.Conn) {
				defer func() {
					c.Close()
					done <- struct{}{}
				}()
				buf := make([]byte, 1024)
				for {
					n, err := c.Read(buf)
					if err != nil {
						if err != io.EOF {
							log.Println("EOF:", err)
						}

						return
					}
					log.Printf("received: %q", buf[:n])
					log.Printf("bytes: %d", n)

				}

			}(conn)
		}
	}()

	conn, err := net.Dial("tcp", listener.Addr().String())
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Connected to server.")

	file, err := os.Open("./prueba3.txt")
	if err != nil {
		log.Fatal(err)
	}

	pr, pw := io.Pipe()
	w, err := gzip.NewWriterLevel(pw, 7)
	if err != nil {
		log.Fatal(err)
	}
	go func() {
		n, err := io.Copy(w, file)
		if err != nil {
			log.Fatal(err)
		}
		w.Close()
		pw.Close()
		log.Printf("copied to piped writer via the compressed writer: %d", n)
	}()

	n, err := io.Copy(conn, pr)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("copied to connection: %d", n)

	conn.Close()
	<-done
	listener.Close()
	<-done
}

/*
package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"
)

func main() {
	dstFile := "./prueba2.txt"
	serverAddr := "localhost:4040"
	server, err := net.Listen("tcp", serverAddr)
	CheckError(err)
	defer server.Close()
	recieveFile(server, dstFile)
}

func recieveFile(server net.Listener, dstFile string) {
	conn, err := server.Accept()
	CheckError(err)
	fo, err := os.Create(dstFile)
	CheckError(err)
	defer fo.Close()
	fmt.Println(&fo, &conn)
	writenBytes, err := io.Copy(fo, conn)
	log.Printf("- Saved %v bytes in file", writenBytes)
	CheckError(err)
}

func CheckError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
		os.Exit(1)
	}
}
*/
