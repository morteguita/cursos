import { createStore } from 'vuex'
import axios from 'axios'
import moment from 'moment'
import router from '@/router'

export default createStore({
  state: {
    recommended: [],
    inventory: [],
    orders: [],
    cart: {},
    currencies: [],
    currency: '',
    showSidebar: false
  },
  mutations: {
    // Modifican los states
    setRecommended (state, payload) {
      state.recommended = payload
    },
    setInventory (state, payload) {
      state.inventory = payload
    },
    setOrders (state, payload) {
      state.orders = payload
    },
    addCartItem (state, { id, quantity }) {
      state.cart[id] = state.cart[id] + quantity
    },
    removeCartItem (state, id) {
      delete state.cart[id]
    },
    deleteCart (state) {
      state.cart = {}
    },
    setCurrency (state, currency) {
      state.currency = currency
    },
    setCurrencies (state, payload) {
      state.currencies = payload
    },
    setShowSidebar (state) {
      state.showSidebar = !state.showSidebar
    }
  },
  actions: {
    // Acciones para modificar los states con las mutations
    async getAllProducts ({ commit }) {
      try {
        const backendServer = process.env.VUE_APP_BACKEND_SERVER
        const data = await axios.get(backendServer + '/products')
        commit('setInventory', data.data)
      } catch (error) {
        console.log(error)
      }
    },
    async getRecommendedProducts ({ commit }) {
      try {
        const size = process.env.VUE_APP_RECOMMENDED_PRODUCTS_LENGTH
        const backendServer = process.env.VUE_APP_BACKEND_SERVER
        const data = await axios.get(backendServer + '/recommended-products/' + size)
        commit('setRecommended', data.data)
      } catch (error) {
        console.log(error)
      }
    },
    async getAllOrders ({ commit }) {
      try {
        const backendServer = process.env.VUE_APP_BACKEND_SERVER
        const data = await axios.get(backendServer + '/orders')
        commit('setOrders', data.data)
      } catch (error) {
        console.log(error)
      }
    },
    async getAllCurrencies ({ commit }) {
      try {
        const backendServer = process.env.VUE_APP_BACKEND_SERVER
        const data = await axios.get(backendServer + '/currencies')
        commit('setCurrencies', data.data)
      } catch (error) {
        console.log(error)
      }
    },
    addProductToCart ({ commit, state }, { id, quantity }) {
      if (!state.cart[id]) state.cart[id] = 0
      if (quantity > 0) commit('addCartItem', { id, quantity })
    },
    removeProductFromCart ({ commit }, id) {
      commit('removeCartItem', id)
    },
    async purchaseOrder ({ commit, state, getters }) {
      try {
        const backendServer = process.env.VUE_APP_BACKEND_SERVER
        const products = []

        for (const id in state.cart) {
          const product = getters.getProduct(id)
          const quantity = state.cart[id]
          const price = getters.getProductValue(product, false)
          const prod = {
            _id: product._id,
            price: parseFloat(price),
            quantity: quantity
          }
          products.push(prod)
        }

        const purchase = {
          products: products,
          date: moment().format('YYYY-MM-DD HH:mm:ss'),
          currency: state.currency,
          total: parseFloat(getters.calculateTotal(false))
        }

        await axios.post(backendServer + '/order', purchase).then(() => {
          commit('deleteCart')
          commit('setShowSidebar')
          this.dispatch('getAllOrders')
          router.push('past-orders')
        }).catch((err) => {
          console.log(err)
        })
      } catch (error) {
        console.log(error)
      }
    },
    changeCurrency ({ commit }, currency) {
      commit('setCurrency', currency)
      commit('deleteCart')
    },
    toggleSidebar ({ commit }) {
      commit('setShowSidebar')
    }
  },
  getters: {
    getProduct: (state) => (id) => {
      return state.inventory.find(p => p._id === id)
    },
    totalQuantity (state) {
      return Object.values(state.cart).reduce((acc, curr) => {
        return acc + curr
      }, 0)
    },
    getAttribute: (state, getters) => (id, attr) => {
      const product = getters.getProduct(id)
      return product[attr]
    },
    calculateTotal: (state, getters) => (currency = true) => {
      const total = Object.entries(state.cart).reduce((acc, curr) => {
        const product = getters.getProduct(curr[0])
        const price = getters.getProductValue(product, false)
        return acc + (curr[1] * price)
      }, 0)
      if (currency) {
        return state.currency + ' ' + total.toFixed(2)
      } else {
        return total.toFixed(2)
      }
    },
    getCurrency: (state) => (currency) => {
      return currency.toUpperCase()
    },
    getProductValue: (state) => (product, currency = true) => {
      const price = product.prices.find(p => p.currency === state.currency)
      if (currency) {
        return state.currency + ' ' + price.value.toFixed(2)
      } else {
        return price.value.toFixed(2)
      }
    }
  },
  modules: {
  }
})
