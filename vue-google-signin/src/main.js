import { createApp } from 'vue'
import App from './App.vue'
import gAuthPlugin from 'vue3-google-oauth2'

const client = '974762209090-25fkop98ncrmgp0sfnln5p5pu8uneenb.apps.googleusercontent.com'
// const secret = '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'

const gAuthOptions = { 
    clientId: client, 
    scope: 'email', 
    prompt: 'consent',
}

const app = createApp(App)
app.use(gAuthPlugin, gAuthOptions)
app.mount('#app')
