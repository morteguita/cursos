function truckTour(petrolpumps) {
  let totalFuel = 0;
  let startIndex = 0;
  let index = 0;

  while (startIndex < petrolpumps.length) {
      const [fuel, dist] = petrolpumps[index];
      totalFuel += fuel - dist;

      if (totalFuel < 0) {
          startIndex = index + 1;
          totalFuel = 0;
          index = startIndex;
      } else {
          index = (index + 1) % petrolpumps.length;
          
          if (index === startIndex){
            return startIndex;
          }
      }
  }

  return -1;
}

function truckTour2(petrolpumps) {
	let startIndex = 0;
	let totalFuel = 0;
	let index = 0;

	while (index < petrolpumps.length) {
		const [currentFuel, distance] = petrolpumps[index];

		totalFuel += currentFuel - distance;
		index++;

		if (totalFuel < 0) {
			const first = petrolpumps[0];
			const rest = petrolpumps.slice(1);
			petrolpumps = rest.concat([first]);
			startIndex++;

			totalFuel = 0;
			index = 0;
		}

		if (startIndex > petrolpumps.length) {
			return -1;
		}
	}

	return startIndex;
}

console.log(
	truckTour([
		[1, 5],
		[10, 3],
		[3, 4],
	])
);

console.log(
	truckTour([
		[200, 400],
		[300, 800],
		[400, 450],
		[500, 555],
		[46675, 40604],
		[95711, 49370],
		[25140, 76805],
		[60012, 40610],
		[31614, 50881],
	])
);