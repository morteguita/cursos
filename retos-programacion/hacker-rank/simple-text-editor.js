function processData(input) {
  //Enter your code here
  const lines = input.split("\n")
  const end = parseInt(lines[0], 10)
  let previous = []
  let string = ""
  
  for(let i = 1; i <= end; i++){
      const arrLine = lines[i].split(" ")
      const inst = arrLine[0]
      const str = arrLine[1] || "0"
      
      if(inst === "1") {
          previous.push(string)
          string += str
      } else if (inst === "2") {
          previous.push(string)
          const q = parseInt(str, 10) * (-1)
          string = string.slice(0, q)
      } else if (inst === "3") {
          const q = parseInt(str, 10) - 1
          console.log(string[q])
      } else if (inst === "4") {
          string = previous.pop()
      }
      //console.log(inst, str, string, previous)
  }
}

const text = `50
1 zsfncpxdzl
3 4
3 6
2 1
3 7
3 2
4
2 4
2 6
4
4
1 l
1 hpe
3 6
2 7
4
3 6
4
3 6
1 zipsqagri
1 vuqxstnj
4
3 13
4
3 10
3 6
1 uzdpy
1 bupqp
1 kn
2 6
3 8
1 iiuvfbn
4
2 1
2 12
4
3 7
4
2 9
3 1
1 axbhx
1 wovbfyvt
3 11
3 7
3 2
4
1 tjmqp
4
2 6
3 4`;
processData(text);