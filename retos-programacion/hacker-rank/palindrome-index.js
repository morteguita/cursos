/*
 * Complete the 'palindromeIndex' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts STRING s as parameter.
 */
function isPalindrome(s) {
  let p1 = 0;
  let p2 = s.length - 1;

  while(p1 < p2) {
    if(s[p1] !== s[p2]) {
      return false;
    }

    p1++;
    p2--;
  }

  return true;
}

function isPalindrome2(s) {
	let half = s.length / 2;
	half = s.length % 2 === 1 ? Math.floor(half) : half;
	const part1 = s.substr(0, half);
	const part2 = s.substr(half + (s.length % 2));

  for (let i = 0; i < part1.length; i++) {
    if (part1[i] !== part2[part2.length - 1 - i]) {
      return false;
    }
  }

  return true;
}

function isPalindrome3(s) {
	let half = s.length / 2;
	half = s.length % 2 === 1 ? Math.floor(half) : half;
	const part1 = s.substr(0, half);
	const part2 = s.substr(half + (s.length % 2));
  return part1 === part2.split("").reverse().join("");
}

function palindromeIndex(s) {
  if(isPalindrome(s)){
    return -1;
  }

  for(let i = 0; i < s.length; i++){
    const newString = s.slice(0, i) + s.slice(i + 1);
    if(isPalindrome(newString)){
      return i;
    }
  }

  return -1;
}

console.log(palindromeIndex('aaab'));
console.log(palindromeIndex('bcbc'));
console.log(palindromeIndex('baa'));
console.log(palindromeIndex('aaa'));
console.log(palindromeIndex('satorarepotenhetoperarotas'));
console.log(palindromeIndex('amanaplanacaretakeramokshalufkinajacinthgiledaniellivanovanodoranegativenessatsarevnamelanomasaniregiigerinasamonalemanverastassenevitagenarodonavonavilleinadelightnicajanikfulahskomarekateracanalpanamas'));
console.log(palindromeIndex('uyjjdcgsvvsgcdjjyq'));
console.log(palindromeIndex('qyjjdcgsvvsgcdjjyq'));
console.log(palindromeIndex('gygsvlfwcwnswtuhmyaljkqlqjjqlqkjlaymhutwsnwcflvsgygh'));
console.log(palindromeIndex('hygsvlfwcwnswtuhmyaljkqlqjjqlqkjlaymhutwsnwcflvsgygh'));
console.log(palindromeIndex('hggsvlfwcwnswtuhmyaljkqlqjjqlqkjlaymhutwsnwcflvsgygh'));
console.log(palindromeIndex('hgysvlfwcwnswtuhmyaljkqlqjjqlqkjlaymhutwsnwcflvsgygh'));
console.log(palindromeIndex('hgygvlfwcwnswtuhmyaljkqlqjjqlqkjlaymhutwsnwcflvsgygh'));
console.log(palindromeIndex('hgygslfwcwnswtuhmyaljkqlqjjqlqkjlaymhutwsnwcflvsgygh'));
console.log(palindromeIndex('hgygsvfwcwnswtuhmyaljkqlqjjqlqkjlaymhutwsnwcflvsgygh'));
console.log(palindromeIndex('hgygsvlwcwnswtuhmyaljkqlqjjqlqkjlaymhutwsnwcflvsgygh'));
console.log(palindromeIndex('hgygsvlfcwnswtuhmyaljkqlqjjqlqkjlaymhutwsnwcflvsgygh'));
console.log(palindromeIndex('gnfnidynhxebxxxfmxixhsruldhsaobhlcggchboashdlurshxixmfxxxbexhnydinfngf'));
console.log(palindromeIndex('fnfnidynhxebxxxfmxixhsruldhsaobhlcggchboashdlurshxixmfxxxbexhnydinfngf'));
console.log(palindromeIndex('fgfnidynhxebxxxfmxixhsruldhsaobhlcggchboashdlurshxixmfxxxbexhnydinfngf'));
console.log(palindromeIndex('fgnnidynhxebxxxfmxixhsruldhsaobhlcggchboashdlurshxixmfxxxbexhnydinfngf'));
console.log(palindromeIndex('fgnfidynhxebxxxfmxixhsruldhsaobhlcggchboashdlurshxixmfxxxbexhnydinfngf'));
console.log(palindromeIndex('fgnfndynhxebxxxfmxixhsruldhsaobhlcggchboashdlurshxixmfxxxbexhnydinfngf'));
console.log(palindromeIndex('fgnfniynhxebxxxfmxixhsruldhsaobhlcggchboashdlurshxixmfxxxbexhnydinfngf'));
console.log(palindromeIndex('fgnfnidnhxebxxxfmxixhsruldhsaobhlcggchboashdlurshxixmfxxxbexhnydinfngf'));
console.log(palindromeIndex('fgnfnidyhxebxxxfmxixhsruldhsaobhlcggchboashdlurshxixmfxxxbexhnydinfngf'));
console.log(palindromeIndex('fgnfnidynxebxxxfmxixhsruldhsaobhlcggchboashdlurshxixmfxxxbexhnydinfngf'));
console.log(palindromeIndex('fgnfnidynhebxxxfmxixhsruldhsaobhlcggclhboashdlurshxixmfxxxabehnydinfngf'));
console.log(palindromeIndex('hgygsvlfcwnswtuhmyaljkqlqjjqlqkjlaymhutwsnwcflvsgygh'));
console.log(palindromeIndex('mmbiefhflbeckaecprwfgqlydfroxrblulpasumubqhhbvlqpixvvxipqlvbhqbumusaplulbrxorfdylqmgfwrpceakceblfhfeibmm'));