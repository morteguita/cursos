var Tree = function () {
	this.root = null;
};

Tree.prototype.insert = function (node, data) {
	if (node == null) {
		node = new Node(data);
	} else if (data < node.data) {
		node.left = this.insert(node.left, data);
	} else {
		node.right = this.insert(node.right, data);
	}

	return node;
};

var Node = function (data) {
	this.data = data;
	this.left = null;
	this.right = null;
};

/* head ends */

/*
  Node is defined as
  var Node = function(data) {
      this.data = data;
      this.left = null;
      this.right = null;
  }
*/

// This is a "method-only" submission.
// You only need to complete this method.

function rec(node, arr) {
	arr.push(node.data);

	if (node.left) {
		rec(node.left, arr);
	}

	if (node.right) {
		rec(node.right, arr);
	}

	return arr;
}

function preOrder(root) {
	const arr = rec(root, []);
	console.log(arr.join(' '));
}

function solution() {
	var tree = new Tree();
	var m = [1, 2, 5, 3, 6, 4];
	var n = m.length;
	for (var i = 0; i < n; i++) {
		tree.root = tree.insert(tree.root, m[i]);
	}

	preOrder(tree.root);
}

solution();
