function getSumArrayHalves(array, length) {
	const half1 = array.slice(0, Math.ceil(length / 2));
	const half2 = array.slice(Math.ceil(length / 2));
	const sum1 = half1.reduce((acc, val) => acc + val, 0);
	const sum2 = half2.reduce((acc, val) => acc + val, 0);

	if (sum2 > sum1) {
		return { array: [...half2, ...half1], sum: sum2 };
	}

	return { array, sum: sum1 };
}

function flippingMatrix(matrix) {
	const len = matrix[0].length;
  const sums = [];

	for (let i = 0; i < len; i++) {
		const { array, sum } = getSumArrayHalves(matrix[i], len);
    matrix[i] = array;
    sums.push({ i, sum });
	}

  sums.sort((a, b) => b.sum - a.sum);
  const newSum = sums.slice(0, Math.ceil(len / 2)).reduce((acc, val) => val.sum + acc, 0);

  const newMatrix = [];

  for(const s of sums) {
    newMatrix.push(matrix[s.i]);
  }

	console.log(newMatrix, newSum);
  return newSum;
}

const matrix = [
	[112, 42, 83, 119],
	[56, 125, 56, 49],
	[15, 78, 101, 43],
	[62, 98, 114, 108],
];
const result = flippingMatrix(matrix);

const matrix2 = [[1, 2], [3, 4]];
//const result2 = flippingMatrix(matrix2);
