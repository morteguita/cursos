/*
 * Complete the 'pairs' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER k
 *  2. INTEGER_ARRAY arr
 */
function pairs(k, arr) {
	// Write your code here
	const elements = [];
	arr.sort((a, b) => a - b);
  console.log("ARR", arr);

	let p1 = 0;
	let p2 = 1;
  console.log("POINTERS", p1, p2);

  //NO FUNCIONA
	while (p1 <= p2) {
		const el1 = arr[p1];
		const el2 = arr[p2];

		console.log("ELEMENTS", el1, el2, " = ", el2 - el1);

		if (el2 - el1 === 0) {
			p2++;
		} else if (el2 - el1 === k) {
			const node = [el1, el2];
			if (!elements.includes(node)) {
        console.log("ENTRE")
				elements.push(node);
			}
			p1++;
			p2++;
		} else {
			p1++;
		}

		console.log("POINTERS", p1, p2);
	}

	/*while(arr.length > 1){
        const first = arr.shift()
        
        for(let i = 0; i < arr.length; i++) {
            const max = Math.max(arr[i], first)
            const min = Math.min(arr[i], first)
            const node = [min, max]
            
            if(max - min === k && !elements.includes(node)){
                elements.push(node)
            }
        }        
    }*/

	return elements.length;
}

const arr = [1, 5, 4, 2, 3];
const k = 3;

console.log(pairs(k, arr));
