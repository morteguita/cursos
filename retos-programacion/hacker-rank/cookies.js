function quickSelect(arr) {
  const k = 2;

  if (arr.length === 0 || arr.length < k || k === 0){
    return [];
  }

  if(arr.length === k) {
    return arr;
  }

  const pivot = arr[0];
  const menores = [];
  const mayores = [];

  for (let i = 1; i < arr.length; i++) {
      if (arr[i] < pivot) {
          menores.push(arr[i]);
      } else {
          mayores.push(arr[i]);
      }
  }

  if (menores.length === k - 1) {
      menores.push(pivot);
      return menores;
  } else if (menores.length >= k) {
      return quickSelect(menores, k);
  } else {
      menores.push(pivot);
      return menores.concat(quickSelect(mayores, k - menores.length));
  }
}

function findTwoSmallest(arr) {
  if (arr.length < 2) {
      return arr; // Devuelve el array si tiene menos de dos elementos
  }

  let smallest = Infinity;
  let secondSmallest = Infinity;

  for (let i = 0; i < arr.length; i++) {
      if (arr[i] <= smallest) {
          secondSmallest = smallest;
          smallest = arr[i];
      } else if (arr[i] <= secondSmallest && arr[i] !== smallest) {
          secondSmallest = arr[i];
      }
  }

  return [smallest, secondSmallest];
}

function searchSweetness(k, A) {
	if (A.length === 1) {
		return A[0] < k ? [-1, false] : [0, true];
	}

	if (A.filter((value) => value <= k).length == 0) {
		return [0, true];
	}

	//A.sort((a, b) => a - b);
  //const first = A.shift();
	//const second = A.shift();
  const [first, second] = findTwoSmallest(A);
  
  const indexFirst = A.indexOf(first);
  A.splice(indexFirst, 1);
  
  const indexSecond = A.indexOf(second);
  A.splice(indexSecond, 1);
	
  const sweet = first + 2 * second;
	A.push(sweet);

	const [rec, bool] = searchSweetness(k, A);
	return [1 + rec, bool];
}

function cookies(k, A) {
	let [count, bool] = searchSweetness(k, A);

	/*while(searchSweetness(k, A) > 0) {
    if(A.length === 1 && A[0] < k) {
      return -1;
    }

    A.sort((a, b) => a - b);
    const first = A.shift();
    const second = A.shift();
    const sweet = first + (2 * second);
    A.push(sweet);
    console.log(A);
    count++;
  }*/

	return bool ? count : -1;
}

const k = 9;
const A = [2, 7, 3, 6, 4, 6];
console.log(cookies(k, A));

const k2 = 10;
const A2 = [1, 1, 1];
console.log(cookies(k2, A2));

const k3 = 7;
const A3 = [1, 2, 3, 9, 10, 12];
console.log(cookies(k3, A3));
