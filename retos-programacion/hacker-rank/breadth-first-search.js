/*
 * Complete the 'bfs' function below.
 *
 * The function is expected to return an INTEGER_ARRAY.
 * The function accepts following parameters:
 *  1. INTEGER n
 *  2. INTEGER m
 *  3. 2D_INTEGER_ARRAY edges
 *  4. INTEGER s
 */

function bfs(n, m, edges, s) {
	// Write your code here
	const tree = [];
	const dist = [];

	/*for (let i = 0; i < edges.length; i++) {
		const nodeValues = edges[i];
		const existsNode = tree.filter((node) => node.value === nodeValues[0]);
		const existsLeaf = tree.filter((node) => node.value === nodeValues[1]);
		let node = {
      value: nodeValues[0],
      parent: null,
      depth: 0,
    };

		if (existsNode.length === 0) {
			tree.push(node);
		} else {
			node = existsNode[0];
		}

		if (existsLeaf.length === 0) {
			let leaf = {
				value: nodeValues[1],
				parent: node.value,
				depth: node.depth + 1,
			};
			tree.push(leaf);
		}
	}

	for (let i = tree.length; i < n; i++) {
		let leaf = {
			value: i + 1,
			parent: null,
			depth: -1,
		};
		tree.push(leaf);
	}*/

	for (let i = 0; i < n; i++) {
		const leaf = {
			value: i + 1,
			parents: [],
			//depth: -1,
		};
		tree.push(leaf);
	}

	for (let i = 0; i < edges.length; i++) {
		const nodeValues = edges[i];
		const node = tree.filter((node) => node.value === nodeValues[0])[0];
		const leaf = tree.filter((node) => node.value === nodeValues[1])[0];
		
		if(!leaf.parents.includes(node.value)){
			leaf.parents.push(node.value);
		}
		
		//leaf.depth = node.depth + 1;
	}

	for (let i = 0; i < tree.length; i++) {
    const node = tree[i];
		
		if (node.value !== s) {
    	//dist.push(node.depth > -1 ? (node.depth + 1) * w : -1);
			const distance = searchFromNode(s, tree, node.parents, 0);
			console.log(s, node.value, distance);
		}
  }

	console.log(edges, tree);
	return dist;
}

function searchFromNode(s, tree, parents, index) {
	if(parents.length === 0) {
		return 0;
	}

	if(parents.includes(s)) {
		return 1;
	} else {
		for(let i = 0; i < parents.length; i++) {
			const node = tree.filter((n) => n.value === parents[i])[0];
			index += searchFromNode(s, tree, node.parents, index);
		}
	}

	return index;
}

/*console.log(
	bfs(
		4,
		2,
		[
			[1, 2],
			[1, 3],
		],
		1
	)
);
console.log(
	bfs(
		5,
		3,
		[
			[1, 2],
			[1, 3],
			[3, 4],
		],
		1
	)
);
console.log(bfs(3, 1, [[2, 3]], 2));*/
console.log(bfs(10, 6, [ [ 3, 1 ], [ 10, 1 ], [ 10, 1 ], [ 3, 1 ], [ 1, 8 ], [ 5, 2 ] ], 3));