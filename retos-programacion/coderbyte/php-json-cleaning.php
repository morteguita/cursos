<?php

$ch = curl_init("https://coderbyte.com/api/challenges/json/json-cleaning");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$data = curl_exec($ch);
curl_close($ch);

$json_data = json_decode($data, true);
$json_data_keys = array_keys($json_data);

function phpJsonCleaning($json_data) {  
  foreach($json_data as $key => $val) {
    if(is_array($val)) {
      $json_data[$key] = phpJsonCleaning($val);
    } else {
      if($val == "N/A" || $val == "" || $val == "-") {
        unset($json_data[$key]);
      }
    }
  }

  return $json_data;
}

print_r($data);
$php_encode = phpJsonCleaning($json_data);
print_r("<br /><br />");
print_r(json_encode($php_encode));

/*$foo = 10;
$bar = (boolean)$foo;
echo $bar;*/
