function HTMLElementsPython(strParam) {
  const openTags = ['<b>', '<i>', '<em>', '<div>', '<p>'];
  const closeTags = ['</b>', '</i>', '</em>', '</div>', '</p>'];

  const stack = [];
  const tags = strParam.split(/(<[^>]*>)/);
  
  for (let tag of tags) {
    if (openTags.includes(tag)) {
      stack.push(tag);
    } else if (closeTags.includes(tag)) {
      const check = closeTags.indexOf(tag);
      if (stack.length > 0 && openTags[check] === stack[stack.length - 1]) {
        stack.pop();
      }
    }
  }

  if (stack.length > 0) {
    return stack[stack.length - 1].replace('<', '').replace('>', '');
  }

  return "true";
}

function HTMLElementsGPT(str) {
  const stack = [];
  const tags = ['b', 'i', 'em', 'div', 'p'];
  const tagMap = { 'b': 'b', 'i': 'i', 'em': 'em', 'div': 'div', 'p': 'p' };

  for (let i = 0; i < str.length; i++) {
    if (str[i] === '<') {
      const end = str.indexOf('>', i);
      if (end === -1) return str.slice(i + 1);
      const tag = str.slice(i + 1, end);

      if (tag[0] === '/') {
        const closingTag = tag.slice(1);
        if (stack.length === 0 || stack.pop() !== tagMap[closingTag]) return closingTag;
      } else {
        if (!tags.includes(tag)) return tag;
        stack.push(tag);
      }

      i = end;
    }
  }

  return "true";
}

function HTMLElements2(str) {
  const openingTags = str.match(/<\w+>/g)
  const closingTags = str.match(/(<\/\w+>)/g).reverse();
  
  const strObj = {
    '<div>': '</div>',
    '<p>': '</p>',
    '<i>': '</i>',
    '<p>': '</p>',
    '<em>': '</em>',
    '<b>': '</b>',
  };

  for(let i = 0; i < openingTags.length; i++) {
    const openingTag = openingTags[i];
    const closingTag = strObj[openingTag];
    
    if (closingTag) {
      const closingTagIndex = closingTags.indexOf(closingTag);

      if (closingTagIndex === 0) {
        closingTags.splice(closingTagIndex, 1);
      } else {
        return openingTags[i].replace(/<|>/g, '');
      }
    }
  }

  return "true";
}

function HTMLElementsFunciona(str) {
  const tagsAbrir = str.match(/<\w+>/g)
  const tagsCerrar = str.match(/(<\/\w+>)/g);
  
  const strObj = {
    '<div>': '</div>',
    '<p>': '</p>',
    '<i>': '</i>',
    '<p>': '</p>',
    '<em>': '</em>',
    '<b>': '</b>',
  };

  for(let i = 0; i < tagsAbrir.length; i++) {
    const abre = tagsAbrir[i];
    const cierra = strObj[abre];
    
    if (cierra) {
      const indiceCierre = tagsCerrar.indexOf(cierra);

      if (indiceCierre > -1) {
        tagsCerrar.splice(indiceCierre, 1);
      } else {
        return tagsAbrir[i].replace(/<|>/g, '');
      }
    }
  }

  return "true";
}

function HTMLElements(str) {
  const openTags = [];
  const closeTags = [];
  const tags = {
    '<div>': '</div>',
    '<p>': '</p>',
    '<i>': '</i>',
    '<p>': '</p>',
    '<em>': '</em>',
    '<b>': '</b>',
  };

  for(let i = 0; i < str.length - 1; i++) {
    if (str[i] === '<') {
      const tag = str.slice(i, str.indexOf('>', i) + 1);

      if(str[i + 1] === '/') {
        closeTags.push(tag);
      } else {
        openTags.push(tag);
      }
    }
  }

  for(let i = 0; i < openTags.length; i++) {
    const open = openTags[i];
    const close = tags[open];
    
    if (close) {
      const indexClose = closeTags.indexOf(close);

      if (indexClose > -1) {
        closeTags.splice(indexClose, 1);
      } else {
        return openTags[i].replace('<', '').replace('>', '');
      }
    }
  }

  return "true";
}

console.log(HTMLElements("<div><b><p>hello world</p></b></div>"));
console.log(HTMLElements("<div><i>hello</i>world</b>"));
console.log(HTMLElements("<div><div><b><b/></div><p/>"));
console.log(HTMLElements("<div>abc</div><p><em><i>test test test</b></em></p>"));
console.log(HTMLElements("<div><p></p><em><div></div></em></div>"));