const axios = require('axios');
const cheerio = require('cheerio');
const moment = require('moment');
const ExcelJS = require('exceljs');

const server = 'https://www.livefutbol.com';
const inicio = 2022;
const fin = 2024;
const partidos = [];

obtenerPartidosPorAnho(inicio, fin, partidos)
.then(() => {
  const nombreArchivo = 'Partidos Colombia Lorenzo.xlsx';
  guardarComoXLS(partidos, nombreArchivo);
})
.catch((error) => {
  console.error(error);
});

function convertirFecha(fecha) {
  const fechaLegible = moment(fecha, 'DD.MM.YYYY').format('YYYY-MM-DD');
  return fechaLegible;
}

function compararFechas(a, b) {
  return a.fecha.localeCompare(b.fecha);
}

function obtenerPartidos(anho) {
  return new Promise((resolve, reject) => {
    const url = `${server}/equipos/kolumbien-team/${anho}/3/`;

    axios.get(url)
      .then((response) => {
        const html = response.data;
        const $ = cheerio.load(html);

        const tiposPartidos = ["Mundial", "Amistosos", "Copa"];
        let tipoPartido = '';

        const partidosJugados = $('.standard_tabelle tr').map((index, element) => {
          const tipoPar = $(element).find('td:nth-child(1)').text().trim();
          const arrTipoPar = tipoPar.split(' ');

          if (tiposPartidos.includes(arrTipoPar[0])) {
            tipoPartido = (arrTipoPar[0] === 'Copa' || arrTipoPar[0] === 'Mundial') ? tipoPar : arrTipoPar[0] + ' ' + arrTipoPar[1];
          }

          if (index !== 0) {
            const fecha = convertirFecha($(element).find('td:nth-child(2)').text().trim());
            const rival = $(element).find('td:nth-child(6)').text().trim();
            const marc = $(element).find('td:nth-child(7)').text().trim();
            const enlace = $(element).find('td:nth-child(7) a').attr('href');
            const marcador = marc.split(' (')[0];

            if (fecha !== '' && rival !== '' && marcador !== '') {
              return {
                tipoPartido,
                fecha,
                rival,
                marcador,
                enlace: server + enlace,
              };
            }
          }
        }).get();

        partidosJugados.sort(compararFechas);
        resolve(partidosJugados);
      })
      .catch((error) => {
        console.error(error);
        reject([]);
      });
  });
}

async function obtenerPartidosPorAnho(inicio, fin, partidos) {
  for (let anho = inicio; anho <= fin; anho++) {
    partidos.push(...await obtenerPartidos(anho));
  }
}

async function guardarComoXLS(partidos, nombreArchivo) {
  const workbook = new ExcelJS.Workbook();
  const worksheet = workbook.addWorksheet('Partidos');

  worksheet.columns = [
    { header: 'Tipo', key: 'tipoPartido', width: 50 },
    { header: 'Fecha', key: 'fecha', width: 20 },
    { header: 'Rival', key: 'rival', width: 20 },
    { header: 'Marcador', key: 'marcador', width: 20 },
    { header: 'Enlace', key: 'enlace', width: 100 },
  ];

  partidos.forEach(partido => {
    worksheet.addRow(partido);
  });

  await workbook.xlsx.writeFile(nombreArchivo);
  console.log(`El archivo ${nombreArchivo} ha sido guardado.`);
}
