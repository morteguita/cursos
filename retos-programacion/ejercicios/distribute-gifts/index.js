//https://adventjs.dev/es/challenges/2023/20

function distributeGifts(weights) {
	const distributed = [];

	for (let i = 0; i < weights.length; i++) {
		const distributedLine = [];

		for (let j = 0; j < weights[i].length; j++) {
			let valid = 0;
			let values = 0;

			const left = weights[i][j - 1];
			const center = weights[i][j];
			const right = weights[i][j + 1];
			const above = weights[i - 1] && weights[i - 1][j];
			const below = weights[i + 1] && weights[i + 1][j];

			if (left) {
				valid++;
				values += left;
			}
			if (center) {
				valid++;
				values += center;
			}
			if (right) {
				valid++;
				values += right;
			}
			if (above) {
				valid++;
				values += above;
			}
			if (below) {
				valid++;
				values += below;
			}

			const avg = valid === 0 ? 0 : Math.round(values / valid);
			distributedLine.push(avg);
		}

		distributed.push(distributedLine);
	}

	return distributed;
}

const input = [
	[4, 5, 1],
	[6, null, 3],
	[8, null, 4],
];

console.log('INPUT:');
console.table(input);
console.log('RESULT:');
console.table(distributeGifts(input));
