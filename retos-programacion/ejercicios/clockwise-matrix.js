function clockwise(m) {
	const rows = m.length;

	if (rows === 0) {
		return [];
	}

	if (rows === 1) {
		return m[0];
	}

	const cols = m[0].length;
	const newMatrix = [...m];

	//step1 is the first row and step3 is the last row
	const step1 = m[0];
	const step2 = [];
	const step3 = m[rows - 1].reverse();
	const step4 = [];

	for (let i = 1; i <= rows - 2; i++) {
		step2.push(m[i][cols - 1]);
		step4.push(m[i][0]);

		//Remove first and last element of inner rows
		newMatrix[i].shift();
		newMatrix[i].pop();
	}

	//Remove first and last row
	newMatrix.shift();
	newMatrix.pop();

	const result = [];

	result.push(step1);

	if (step2.length > 0) {
		result.push(step2);
	}

	result.push(step3);

	if (step4.length > 0) {
		result.push(step4.reverse());
	}

	//Recursive call with new matrix
	const newResult = clockwise(newMatrix);

	if (newResult.length > 0) {
		result.push(newResult);
	}

	return result.join(',');
}

function readMatrix(m) {
	return clockwise(m);
}

const m = [
	[1, 2, 3],
	[4, 5, 6],
	[7, 8, 9],
];
console.log(readMatrix(m));

const m2 = [
	[10, 30],
	[70, 50],
];
console.log(readMatrix(m2));

const m3 = [
	[1, 2, 3, 4],
	[12, 13, 14, 5],
	[11, 16, 15, 6],
	[10, 9, 8, 7],
];
console.log(readMatrix(m3));

const m4 = [
	[1, 2, 3, 4, 5, 6],
	[7, 8, 9, 10, 11, 12],
	[13, 14, 15, 16, 17, 18],
];
console.log(readMatrix(m4));