const obj1 = {name:'Juan',gender:'male', address: {street:'ave 1'}}
const obj2 = JSON.parse(JSON.stringify(obj1));
obj2.address.street = 'ave 2';

console.log(obj1.address.street);
console.log(obj2.address.street);


for (var val = 0; val < 10; val++) {
  setTimeout(function() {
      console.log(`The number is ${val}`);
  }, 1000);
}

for (let val = 0; val < 10; val++) {
  setTimeout(function() {
      console.log(`The number is ${val}`);
  }, 1000);
}

console.log(typeof null);