const animals = [
	'Rat',
	'Ox',
	'Tiger',
	'Rabbit',
	'Dragon',
	'Snake',
	'Horse',
	'Sheep',
	'Monkey',
	'Rooster',
	'Dog',
	'Pig',
];
const elements = ['Wood', 'Fire', 'Earth', 'Metal', 'Water'];
const elementsInterval = 2;
let elementIndex = 0;

const startYear = 1984;
const currentYear = new Date().getFullYear();

for (let i = startYear; i <= startYear + 60; i++) {
  const mod = i % startYear;
  const mod2 = mod % animals.length;
  
  console.log(i, animals[mod2], elements[elementIndex]);
  
  if (i % 2 == 1) {
    elementIndex++;

    if (elementIndex >= elements.length) {
      elementIndex = 0;
    }
  }
}