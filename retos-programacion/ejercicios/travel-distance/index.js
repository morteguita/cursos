//https://adventjs.dev/es/challenges/2023/25

function findSanta(line) {
	return line.indexOf('S');
}

function findPersons(line, index) {
	const persons = [];
	const chars = line.split('');

	for (let i = 0; i < chars.length; i++) {
		if (parseInt(chars[i], 10) && !isNaN(chars[i])) {
			persons.push({ person: parseInt(chars[i], 10), x: i, y: index });
		}
	}

	return persons;
}

function travelDistance(map) {
	const mapLines = map.split('\n');
	const santaPos = { x: -1, y: -1 };
	const persons = [];
	const distances = [];

	mapLines.forEach((line, index) => {
		if (santaPos.x < 0) {
			santaPos.x = findSanta(line);
			santaPos.y = index;
		}

		persons.push(...findPersons(line, index));
	});

	const orderedPersons = persons.sort((a, b) => a.person - b.person);

	orderedPersons.forEach((person) => {
		distances.push(Math.abs(person.x - santaPos.x) + Math.abs(person.y - santaPos.y));
		santaPos.x = person.x;
		santaPos.y = person.y;
	});

	return distances.reduce((acc, dist) => { return acc + dist; }, 0);
}

const map = `.....1....
..S.......
..........
....3.....
......2...`;
console.log(map);
console.log(travelDistance(map));

console.log('');

const map2 = `..S.1...`;
console.log(map2);
console.log(travelDistance(map2));
