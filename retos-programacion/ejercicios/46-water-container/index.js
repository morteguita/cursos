/*
 * Dado un array de números enteros positivos, donde cada uno
 * representa unidades de bloques apilados, debemos calcular cuantas unidades
 * de agua quedarán atrapadas entre ellos.
 *
 * - Ejemplo: Dado el array [4, 0, 3, 6, 1, 3].
 *
 *         ⏹
 *         ⏹
 *   ⏹💧💧⏹
 *   ⏹💧⏹⏹💧⏹
 *   ⏹💧⏹⏹💧⏹
 *   ⏹💧⏹⏹⏹⏹
 *
 *   Representando bloque con ⏹︎ y agua con 💧, quedarán atrapadas 7 unidades
 *   de agua. Suponemos que existe un suelo impermeable en la parte inferior
 *   que retiene el agua.
 */

function waterContainer(array) {
  let units = 0;
  let wall = 0;
  let nextWall = 0;

  array.forEach((blocks, index) => {
    if (index !== array.length - 1 && (index === 0 || nextWall === blocks)) {
      wall = index === 0 ? blocks : nextWall;
      nextWall = 0;

      for (let nextBlocksIndex = index + 1; nextBlocksIndex < array.length; nextBlocksIndex++) {
        if (array[nextBlocksIndex] >= nextWall && wall >= nextWall) {
          nextWall = array[nextBlocksIndex];
        }
      }
    } else {
      const referenceWall = nextWall > wall ? wall : nextWall;
      const currentBlocks = referenceWall - blocks;
      units += currentBlocks >= 0 ? currentBlocks : 0;
    }
  });

  return units;
}

console.log(waterContainer([4, 0, 3, 6]));
console.log(waterContainer([4, 0, 3, 6, 1, 3]));
console.log(waterContainer([2, 5, 1, 2, 3, 4, 7, 7, 6]));
