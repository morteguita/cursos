const input = [
	1, 2, 3, 4, 5, 6, 7, 8, 9, 153, 370, 371, 407, 1634, 8208, 9474, 54748, 92727,
	2015, 4210817, 4210818, -3
];

function isArmstong(number) {
	const numberString = number.toString();
	const numberLength = numberString.length;
	const numberProcessed = numberString.split('').reduce(
		(acc, value) => acc + Math.pow(parseInt(value, 10), numberLength),
		0
	);
	return numberProcessed;
}

for (let i = 0; i < input.length; i++) {
	const number = isArmstong(input[i]);
	console.log(`${input[i]}: ${number === input[i]}`);
}
