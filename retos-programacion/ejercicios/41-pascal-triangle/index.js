function pascalTriangle(size){
  const triangle = [];

  for (let i = 0; i <= size; i++) {
    const line = [];

    for (let j = 0; j <= i; j++) {
      const number =
        triangle[i - 1] && triangle[i - 1][j - 1] && triangle[i - 1][j]
          ? triangle[i - 1][j - 1] + triangle[i - 1][j]
          : 1;
      line.push(number);
    }

    triangle.push(line);
  }

  return triangle;
}

console.table(pascalTriangle(3));
console.table(pascalTriangle(5));
console.table(pascalTriangle(10));
