function readArray(arr) {
  let max = arr[0];

	const newArr = arr.reduce((acc, index) => {
    if (max < index) {
      max = index;
    }

		if (!acc[index]) {
			acc[index] = '';
		}

		acc[index] += '*';
		return acc;
	}, []);

	for (let i = 1; i <= max; i++) {
		console.log(`${i}: ${newArr[i] || ''}`);
	}
}

const a = [1, 1, 3, 2, 1, 1, 2, 3, 1, 5];
readArray(a);
