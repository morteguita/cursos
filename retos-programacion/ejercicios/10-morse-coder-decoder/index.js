/*
 * Crea un programa que sea capaz de transformar texto natural a código
 * morse y viceversa.
 * - Debe detectar automáticamente de qué tipo se trata y realizar
 *   la conversión.
 * - En morse se soporta raya "—", punto ".", un espacio " " entre letras
 *   o símbolos y dos espacios entre palabras "  ".
 * - El alfabeto morse soportado será el mostrado en
 *   https://es.wikipedia.org/wiki/Código_morse.
 */

const morseCode = {
	A: '.-',
	B: '-...',
	C: '-.-.',
	D: '-..',
	E: '.',
	F: '..-.',
	G: '--.',
	H: '....',
	I: '..',
	J: '.---',
	K: '-.-',
	L: '.-..',
	M: '--',
	N: '-.',
	O: '---',
	P: '.--.',
	Q: '--.-',
	R: '.-.',
	S: '...',
	T: '-',
	U: '..-',
	V: '...-',
	W: '.--',
	X: '-..-',
	Y: '-.--',
	Z: '--..',
	0: '-----',
	1: '.----',
	2: '..---',
	3: '...--',
	4: '....-',
	5: '.....',
	6: '-....',
	7: '--...',
	8: '---..',
	9: '----.',
	'.': '.-.-.-',
	',': '--..--',
	'?': '..--..',
	"'": '.----.',
	'!': '-.-.--',
	'/': '-..-.',
	'(': '-.--.',
	')': '-.--.-',
	'&': '.-...',
	':': '---...',
	';': '-.-.-.',
	'=': '-...-',
	'+': '.-.-.',
	'-': '-....-',
	_: '..--.-',
	'"': '.-..-.',
	$: '...-..-',
	'@': '.--.-.',
};
const invertedMorseCode = Object.entries(morseCode).reduce(
	(acc, [key, value]) => {
		acc[value] = key;
		return acc;
	},
	{}
);

function isWord(character) {
	return /^[a-zA-Z]+$/.test(character);
}

function isNumeric(character) {
	return /^[0-9]+$/.test(character);
}

/*
 *  ..  .-.. --- ...- .  -.-- --- ..-   I Love You
 *  -.-. .... --- -.-. .- .--. .. -.-. .-.-.-  . ...  ..- -. .-  -- .- .-. -.-. .-  -.. .  -.-. . .-. . .- .-.. . ... ..--..  Chocapic. Es una marca de cereales?
 */

const input = 'Chocapic. Es una marca de cereales?';
//const input = '-.-. .... --- -.-. .- .--. .. -.-. .-.-.-  . ...  ..- -. .-  -- .- .-. -.-. .-  -.. .  -.-. . .-. . .- .-.. . ... ..--..';
let output = '';
const isInputWord = isWord(input[0]) || isNumeric(input[0]);

if (isInputWord) {
	//Regular word
	const charInput = input.split('');

	for (let i = 0; i < charInput.length; i++) {
		output +=
			(charInput[i] === ' ' ? '' : morseCode[charInput[i].toUpperCase()]) + ' ';
	}
} else {
	//Morse
	const charInput = input.split(' ');

	for (let i = 0; i < charInput.length; i++) {
		output += charInput[i] === '' ? ' ' : invertedMorseCode[charInput[i]];
	}
}

console.log(output);
