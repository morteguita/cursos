import axios from 'axios';
import { load } from 'cheerio';

async function findVideos(startId, endId) {
	const videos = [];

	for (let id = startId; id <= endId; id++) {
		const url = `https://beelup.com/player.php?id=${id}`;

		try {
			const response = await axios.get(url);
			const chrr = load(response.data);
			const pageTitle = chrr('title').text();

			if (pageTitle.includes('LA CALDERA CALI')) {
				videos.push({ id, title: pageTitle, url });
			}
		} catch (error) {
			console.error(`Error al obtener la página ${url}: ${error.message}`);
		}
	}

	return videos;
}

const startId = 10647600;
const endId = 10647804;

findVideos(startId, endId)
	.then((videos) => {
		console.log('Videos encontrados:');
		videos.forEach((video) => {
			console.log(`ID: ${video.id}, Title: ${video.title}, URL: ${video.url}`);
		});
	})
	.catch((error) => {
		console.error('Error:', error.message);
	});
