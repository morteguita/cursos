import { describe, expect, it } from 'vitest';
import { canReconfigure } from '../src/canReconfigure';

describe('canReconfigure', () => {
	//Se quita por ser redundante, solo sirve al inicio
	/*it('should be a function', () => {
		expect(canReconfigure).toBeTypeOf('function');
	});*/

	it('should throw if first parameter is missing', () => {
		expect(() => canReconfigure()).toThrow();
	});

	it('should throw if first parameter is not a string', () => {
		expect(() => canReconfigure(2)).toThrow();
	});

	it('should throw if second parameter is not a string', () => {
		expect(() => canReconfigure('a', 2)).toThrow();
	});

	it('should return a boolean', () => {
		expect(canReconfigure('a', 'b')).toBeTypeOf('boolean');
	});

	it('should return false if parameters have different length', () => {
		expect(canReconfigure('a', 'aa')).toBe(false);
	});

	it('should return false if string provided have different number of unique characters', () => {
		expect(canReconfigure('abc', 'ddd')).toBe(false);
	});

	it('should return false if parameters have different order of transformation', () => {
		expect(canReconfigure('XBOX', 'XXBO')).toBe(false);
		expect(canReconfigure('CON', 'JUU')).toBe(false);
	});
});
