import { describe, expect, it } from 'vitest';
import { fizzbuzz } from '../src/fizzbuzz';

/*
 * Escribe una funcion que al pasarle un numero:
 * - Muestra "fizz" si es multiplo de 3
 * - Muestra "buzz" si es multiplo de 5
 * - Muestra "fizzbuzz" si es multiplo de 3 y de 5
 * - Muestra el numero si no corresponde a ninguno de los anteriores.
 */

describe('fizzbuzz', () => {
	//Se quita por ser redundante, solo sirve al inicio
	/*it('should be a function', () => {
		expect(typeof fizzbuzz).toBe('function');
	});*/

	it('should throw if not number is provided as a parameter', () => {
		expect(() => fizzbuzz()).toThrow();
	});

	it('should throw aa specific message if not number is provided as a parameter', () => {
		expect(() => fizzbuzz()).toThrow('Invalid parameter');
	});

	it('should throw aa specific message if not a number is provided', () => {
		expect(() => fizzbuzz(NaN)).toThrow('Parameter must be a number');
	});

	it('should return 1 if number provided is 1', () => {
		expect(fizzbuzz(1)).toBe(1);
	});

	it('should return 2 if number provided is 2', () => {
		expect(fizzbuzz(2)).toBe(2);
	});

	it('should return "fizz" if number provided is 3', () => {
		expect(fizzbuzz(3)).toBe('fizz');
	});

	it('should return "fizz" if number provided is multiple of 3', () => {
		expect(fizzbuzz(6)).toBe('fizz');
		expect(fizzbuzz(9)).toBe('fizz');
		expect(fizzbuzz(12)).toBe('fizz');
	});

	//Se quita porque no aporta, pasa en verde de una porque se revisó que el caso ya queda cubierto
	/*it('should return 4 if number provided is 4', () => {
		expect(fizzbuzz(4)).toBe(4);
	});*/

	it('should return "buzz" if number provided is 5', () => {
		expect(fizzbuzz(5)).toBe('buzz');
	});

	it('should return "buzz" if number provided is multiple of 5', () => {
		expect(fizzbuzz(10)).toBe('buzz');
		expect(fizzbuzz(20)).toBe('buzz');
		expect(fizzbuzz(25)).toBe('buzz');
	});

	it('should return "fizzbuzz" if number provided is multiple of 3 and 5', () => {
		expect(fizzbuzz(15)).toBe('fizzbuzz');
	});
});
