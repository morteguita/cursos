import { useState } from 'react';
import { evaluate } from 'mathjs';

export const numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
export const rows = [[7, 8, 9], [4, 5, 6], [1, 2, 3], [0]];
export const operations = ['+', '-', '*', '/'];
export const equalSign = '=';

export const Calculator = () => {
	const [value, setValue] = useState('');

	const handleClickNumberOperation = (val) => {
		setValue(value.concat(val));
	};

	const handleResolveOperation = () => {
		setValue(evaluate(value));
	};

	return (
		<section style={{ width: '300px' }}>
			<h1>Calculator</h1>
			<div role="grid" style={{ textAlign: 'center', paddingBottom: '10px' }}>
				<input
					type="text"
					value={value}
					readOnly
					style={{ width: '100%', height: '40px', textAlign: 'right', fontSize: '20px' }}
				/>
			</div>
			<div role="grid" style={{ textAlign: 'center', paddingBottom: '10px' }}>
				{rows.map((row, index) => (
					<div key={index} role="row">
						{row.map((number) => (
							<button
								key={number}
								onClick={() => handleClickNumberOperation(number)}
								style={{ width: '50px', height: '50px' }}
							>
								{number}
							</button>
						))}
					</div>
				))}
			</div>
			<div role="grid" style={{ textAlign: 'center', paddingBottom: '10px' }}>
				{operations.map((operation, index) => (
					<button
						key={index}
						onClick={() => handleClickNumberOperation(operation)}
						style={{ width: '40px', height: '40px' }}
					>
						{operation}
					</button>
				))}
			</div>
			<div role="grid" style={{ textAlign: 'center', paddingBottom: '10px' }}>
				<button
					onClick={handleResolveOperation}
					style={{ width: '80px', height: '40px' }}
				>
					{equalSign}
				</button>
			</div>
		</section>
	);
};
