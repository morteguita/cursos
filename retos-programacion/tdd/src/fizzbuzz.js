export const fizzbuzz = (number) => {
	if (typeof number !== 'number') {
		throw new Error('Invalid parameter');
	}

	if (Number.isNaN(number)) {
		throw new Error('Parameter must be a number');
	}

	if (number % 3 === 0 && number % 5 === 0) {
		return 'fizzbuzz';
	}

	if (number % 3 === 0) {
		return 'fizz';
	}

	if (number % 5 === 0) {
		return 'buzz';
	}

	return number;
};
