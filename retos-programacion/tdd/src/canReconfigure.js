/**
 * https://2021.adventjs.dev/challenges/23
 * Estamos en la fábrica de Santa Claus creando regalos como si no hubiera un mañana.
 * Pensábamos que no íbamos a llegar pero Jelf Bezos ha tenido una idea genial para aprovechar las máquinas y optimizar al máximo la creación de regalos.
 * La configuración de las máquinas es un string. Podemos reconfigurarla para que haga otro regalo y, para ello, podemos cambiar cada carácter por otro.
 * Pero tiene limitaciones: al reemplazar el carácter se debe mantener el orden, no se puede asignar al mismo carácter a dos letras distintas (pero sí a si mismo) y, claro, la longitud del string debe ser el mismo.
 */

export const canReconfigure = (from, to) => {
	//Se quita porque ya queda cubierto por el caso de prueba
	/*if (from === undefined) {
		throw new Error('from is required');
	}*/

	if (typeof from !== 'string') {
		throw new Error('from must be a string');
	}

	if (typeof to !== 'string') {
		throw new Error('to must be a string');
	}

	const isSameLength = from.length === to.length;
	if (!isSameLength) {
		return false;
	}

	const hasUniqueCharacters = new Set(from).size === new Set(to).size;
	if (!hasUniqueCharacters) {
		return false;
	}

	const transformations = {};
	for (let i = 0; i < from.length; i++) {
		const fromLetter = from[i];
		const toLetter = to[i];
		const storedLetter = transformations[fromLetter];

		if (storedLetter && storedLetter !== toLetter) {
			return false;
		}

		transformations[fromLetter] = toLetter;
	}

	return true;
};