function reverse(str){
  let reversed = "";

  for(let char of str){
    reversed = char + reversed;
  }

  return reversed;
  //return str.split("").reverse().join(""); //With built-in functions
}

function reverseInt(n){
  let reversed = "";

  for(let char of n.toString()){
    reversed = char + reversed;
  }

  return parseInt(reversed) * Math.sign(n);
  //return parseInt(n.toString().split("").reverse().join("")) * Math.sign(n); //With built-in functions
}

console.log(reverse("hello"));
console.log(reverse("CodingMoney"));
console.log(reverseInt(981));
console.log(reverseInt(-91));