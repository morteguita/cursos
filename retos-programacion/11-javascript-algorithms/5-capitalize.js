function capitalize(str) {
  const words = str.split(" ");
  const wordsCap = [];

  words.map(word => {
    wordsCap.push(word[0].toUpperCase() + word.slice(1));
  })

  /*for (let word of words) {
    wordsCap.push(word[0].toUpperCase() + word.slice(1));
  }*/

  return wordsCap.join(" ");
}

function uppercase(str) {
  const lowerChars = "abcdefghijklmnopqrstuvwxyzñáéíóú";
  const upperChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZÑÁÉÍÓÚ";
  let newStr = "";

  for (let i = 0; i < str.length; i++) {
    const index = lowerChars.indexOf(str[i]);
    newStr += (index >= 0) ? upperChars[index] : str[i];
  }

  return newStr;
}

console.log(capitalize("hello"));
console.log(capitalize("hello, world"));
console.log(capitalize("this is Manuel from coding money"));
console.log(uppercase("hello"));
console.log(uppercase("hello, world"));
console.log(uppercase("this is Manuel from coding money"));