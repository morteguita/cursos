function fizzbuzz (n) {
  for(let i = 1; i <= n; i++) {
    let s = "";

    if (i % 3 === 0) {
      s += "fizz";
    }

    if (i % 5 === 0) {
      s += "buzz";
    }

    console.log(s || i);
  }
}

fizzbuzz(20);