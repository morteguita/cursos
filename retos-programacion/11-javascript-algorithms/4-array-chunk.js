function chunk(array, size) {
  if (array.length <= size) {
    return [array];
  }

  const newArray = [array.slice(0, size)];
  return newArray.concat(chunk(array.slice(size), size));
}

console.log(chunk([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 3));
console.log(chunk([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], 3));
console.log(chunk([1, 2, 3, 4], 2));
console.log(chunk([1, 2, 3, 4, 5], 2));
console.log(chunk([1], 2));