function cleanString(str) {
  const chars = "abcdefghijklmnopqrstuvwxyz";
  const arrString = [];

  for(let char of str.toLowerCase()) {
    if(chars.includes(char)) {
      arrString.push(char);
    }
  }

  return arrString;
}

function anagrams(stringA, stringB) {
  const arrStringA = cleanString(stringA);
  const arrStringB = cleanString(stringB);

  if(arrStringA.length !== arrStringB.length) {
    return false;
  }

  stringA = arrStringA.sort().join("");
  stringB = arrStringB.sort().join("");

  return stringA === stringB;
}

function createCharMap(str) {
  const chars = "abcdefghijklmnopqrstuvwxyz";
  const charMap = {};

  for(let char of str.toLowerCase()) {
    if(chars.includes(char)) {
      charMap[char] = (charMap[char] || 0) + 1;
    }
  }

  return charMap;
}

function anagrams2(stringA, stringB) {
  const charMapA = createCharMap(stringA);
  const charMapB = createCharMap(stringB);

  if(charMapA.length !== charMapB.length) {
    return false;
  }

  for(let char in charMapA) {
    if(charMapB[char] !== charMapA[char]) {
      return false;
    }
  }

  return true;
}

console.log(anagrams("abc", "cba"));
console.log(anagrams("abc", "abd"));
console.log(anagrams("abc", "caa"));
console.log(anagrams("abc", "ca"));
console.log(anagrams("coding money", "money coding"));
console.log(anagrams("RAIL! SAFETY!", "fairy tales"));
console.log(anagrams("Hi there!", "Bye there"));