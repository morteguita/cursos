function steps(n) {
	const char = '#';
  const space = ' ';

	for (let i = 1; i <= n; i++) {
		console.log("'" + char.repeat(i) + space.repeat(n - i) + "'");
	}
}

steps(9);
