/**
 * Generates a square matrix with a spiral pattern of numbers.
 *
 * @param {number} n - The dimension of the square matrix.
 * @param {number} level - The current level of the spiral pattern.
 * @param {number} total - The total number of elements in the matrix.
 * @param {number} current - The current number to be placed in the matrix.
 * @param {array} squareMatrix - The square matrix to be filled with the spiral pattern.
 * @return {array} The square matrix filled with the spiral pattern.
 */
function spiral(n, level, total, current, squareMatrix) {
  if(current > total) {
    return squareMatrix;
  }

  //if(level > 0) console.log("=========> LEVEL", level);

  //First row
  for(let i = level; i < n; i++) {
    //if(level > 0) console.log("FIRST ROW", level, i, current);
    squareMatrix[level][i] = current;
    current++;
  }

  if(current < total) {
    //Last column
    for(let i = level + 1; i < n-1; i++) {
      //if(level > 0) console.log("LAST COLUMN", i, n-1, current);
      squareMatrix[i][n-1] = current;
      current++;
    }

    //Last row (reversed)
    for(let i = n - 1; i >= level; i--) {
      //if(level > 0) console.log("LAST ROW", n-1, i, current);
      squareMatrix[n-1][i] = current;
      current++;
    }

    //First column (bottom to up)
    for(let i = n - 2; i > level; i--) {
      //if(level > 0) console.log("LAST COLUMN", i, level, current);
      squareMatrix[i][level] = current;
      current++;
    }
  }

  //if(level > 0) console.log("NEXT CALL", n - 1, level + 1, total, current);
  spiral(n - 1, level + 1, total, current, squareMatrix);
  return squareMatrix;
}

function matrix(n) {
  //n * n spiral clockwise matrix
  const m = [];

  for (let i = 0; i < n; i++) {
    m[i] = [];
  }

  const totalElements = n * n;
  return spiral(n, 0, totalElements,  1, m);
}

console.log(matrix(2));
console.log(matrix(3));
console.log(matrix(4));
console.log(matrix(5));