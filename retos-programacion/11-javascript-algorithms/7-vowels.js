function vowels(str) {
  const chars = "aeiouAEIOU";
  let q = 0;

  for(let char of str) {
    q += chars.includes(char);
  }

  return q;
}

function vowelsRegex(str) {
  const vowelRegex = str.match(/[aeiou]/gi);
  return vowelRegex ? vowelRegex.length : 0;
}

console.log(vowels("Hi there!"));
console.log(vowels("How are you?"));
console.log(vowels("Coding Money"));
console.log(vowels("why?"));
console.log(vowels("hello"));
console.log(vowels("hello, world"));
console.log(vowels("this is Manuel from coding money"));
