function palindrome(str) {
  /*const halfLen = Math.floor(str.length / 2);
  const half1 = str.slice(0, halfLen);
  const half2 = str.slice(-halfLen);

  return half1 === half2.split('').reverse().join('');*/
  let p1 = 0;
  let p2 = str.length - 1;

  while(p1 < p2) {
    if(str[p1] !== str[p2]) {
      return false;
    }

    p1++;
    p2--;
  }

  return true;
}

console.log("racecar", palindrome("racecar"));
console.log("hello", palindrome("hello"));
console.log("kayak", palindrome("kayak"));
console.log("madam", palindrome("madam"));
console.log("codingmoney", palindrome("codingmoney"));
console.log("maam", palindrome("maam"));
