function pyramid(n) {
	const char = '#';
	const space = ' ';
	const maxSpaces = (2 * n) - 1;

	for (let i = 1; i <= n; i++) {
		const pyramidChars = (2 * i) - 1;
		const halfSpaces = (maxSpaces - pyramidChars) / 2;
		const s = space.repeat(halfSpaces) + char.repeat(pyramidChars) + space.repeat(halfSpaces);

		console.log("'" + s + "'");
	}
}

pyramid(15);
