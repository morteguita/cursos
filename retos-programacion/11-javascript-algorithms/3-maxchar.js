function maxChar(str) {
  const charsMap = {};
  let max = 0;
  let maxCh = "";

  for(let char of str) {
    charsMap[char] = (charsMap[char] || 0) + 1;
  }

  for(let char in charsMap) {
    if(charsMap[char] > max) {
      max = charsMap[char];
      maxCh = char;
    }
  }

  /*Object.entries(charsMap).map((char) => {
    if(char[1] > max) {
      max = char[1];
      maxCh = char[0];
    }
  });*/
 
  return `${maxCh} - ${max} times`;
}

console.log("racecar", maxChar("racecar"));
console.log("hello", maxChar("hello"));
console.log("kayak", maxChar("kayak"));
console.log("apple 1231111", maxChar("apple 1231111"));
console.log("abcccccccd", maxChar("abcccccccd"));
