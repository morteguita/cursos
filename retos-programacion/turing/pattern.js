/*
Given a string text and a pattern. Evaluate if a given text string matches a specified pattern, using special characters for a pattern matching.

The pattern matching should adhere to the following rules:
- The symbol . represents any individual character
- The symbol * represents zero or more occurrences of the character that appears just before it

Return true if the pattern corresponds exactly to the entire text string, if not, return false.

Note:
- Your implementation should not be dependent on regular expression or similar convenient libraries
- Your implementation should be able to handle all hidden test cases correctly
*/
var solution = function (s, p) {
	let sIdx = 0,
		pIdx = 0,
		lastWildcardIdx = -1,
		sBacktrackIdx = -1,
		nextToWildcardIdx = -1;

	while (sIdx < s.length) {
		if (pIdx < p.length && (p[pIdx] === '.' || p[pIdx] === s[sIdx])) {
			// Characters match
			sIdx++;
			pIdx++;
		} else if (pIdx < p.length && p[pIdx] === '*') {
			// Wildcard, so characters match - store the index.
			lastWildcardIdx = pIdx;
			nextToWildcardIdx = ++pIdx;
			sBacktrackIdx = sIdx;
		} else if (lastWildcardIdx === -1) {
			// No match, and no wildcard has been found.
			return false;
		} else {
			// Backtrack - no match, but a previous wildcard was found.
			pIdx = nextToWildcardIdx;
			sIdx = ++sBacktrackIdx;
		}
	}

	// Check if there are only wildcards left in the pattern.
	for (let i = pIdx; i < p.length; i++) {
		if (p[i] !== '*') return false;
	}

	return true;
};

console.log(solution('aa', 'a'));
console.log(solution('aa', 'a*'));
console.log(solution('ab', '.*'));
console.log(solution('aab', 'c*a*b'));
console.log(solution('abc', 'a***abc'));
console.log(solution('mississippi', 'mis*is*p*.'));
