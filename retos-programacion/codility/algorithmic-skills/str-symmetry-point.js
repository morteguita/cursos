/*
Write a function:

function solution(S);

that, given a string S, returns the index (counting from 0) of a character such that the part of the string to the left of that character is a reversal of the part of the string to its right. The function should return −1 if no such index exists.

Note: reversing an empty string (i.e. a string whose length is zero) gives an empty string.

For example, given a string:

"racecar"

the function should return 3, because the substring to the left of the character "e" at index 3 is "rac", and the one to the right is "car".

Given a string: "x"

the function should return 0, because both substrings are empty.

Write an efficient algorithm for the following assumptions:

the length of string S is within the range [0..2,000,000].
*/
function reverseString(str) {
  return str.split("").reverse().join("");
}

function solution(S){
  if (S.length == 0) {
    return -1;
  }

  if (S.length == 1) {
    return 0;
  }

  if (S.length % 2 === 0) {
    return -1;
  }

  const half = Math.ceil(S.length / 2);
  const firstHalf = S.slice(0, half - 1);
  const secondHalf = S.slice(half);

  if (firstHalf === reverseString(secondHalf)) {
    return half - 1;
  }

  /*
  //Apparently, I did not understand the problem
  let i = 2;

  while(i <= half) {
    const firstHalf = S.slice(0, i - 1);
    const secondHalf = S.slice(i, (i * 2) - 1);

    if (firstHalf === reverseString(secondHalf)) {
      return i - 1;
    }

    i++;
  }*/

  return -1;
}

console.log(solution("racecar"));
console.log(solution("x"));
console.log(solution(""));
console.log(solution("aaaaaaaaaaaaaaa"));