/*
A string S containing only the letters "A", "B" and "C" is given. The string can be transformed by removing one occurrence of "AA", "BB" or "CC".

Transformation of the string is the process of removing letters from it, based on the rules described above. As long as at least one rule can be applied, the process should be repeated. If more than one rule can be used, any one of them could be chosen.

Write a function:

function solution(S);

that, given a string S consisting of N characters, returns any string that can result from a sequence of transformations as described above.

For example, given string S = "ACCAABBC" the function may return "AC", because one of the possible sequences of transformations is as follows:



Also, given string S = "ABCBBCBA" the function may return "", because one possible sequence of transformations is:



Finally, for string S = "BABABA" the function must return "BABABA", because no rules can be applied to string S.

Write an efficient algorithm for the following assumptions:

the length of string S is within the range [0..50,000];
string S is made only of the following characters: 'A', 'B' and/or 'C'.
*/
/*
//Less optimal
function solution(S){
  if (S.length < 2) {
    return S;
  }

  if (S.length > 50000) {
    return "";
  }

  const arrayChars = ["A", "B", "C"];

  if(arrayChars.filter(value => !S.includes(value)).length === arrayChars.length) {
    return S;
  }

  const arrayDoubleChars = arrayChars.map(value => value.repeat(2));

  while(arrayDoubleChars.filter(value => S.includes(value)).length > 0) {
    S = arrayDoubleChars.reduce((acc, cur) => acc.replace(cur, ""), S);
  }

  return S;
}*/

function solution(S) {
  if (S.length < 2) {
    return S;
  }

  if (S.length > 50000) {
    return "";
  }

  const pattern = /^[ABC]+$/;
  if (!pattern.test(S)) {
    return S;
  }

  const stack = [];

  for (const s of S) {
    if (stack.length && s === stack[stack.length - 1]) {
      stack.pop();
      continue;
    }
    stack.push(s);
  }

  return stack.join('');
}

console.log(solution("ACCAABBC"));
console.log(solution("ABCBBCBA"));
console.log(solution("BABABA"));
console.log(solution(""));
console.log(solution("Q"));
console.log(solution("DEFF"));
console.log(solution("ACBACCABCA"));
console.log(solution("ACBACBCABCA"));
console.log(solution("ACBACBBCABCA"));
