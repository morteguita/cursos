/*
An array A consisting of N integers is given. An inversion is a pair of indexes (P, Q) such that P < Q and A[Q] < A[P].

Write a function:

function solution(A);

that computes the number of inversions in A, or returns −1 if it exceeds 1,000,000,000.

For example, in the following array:

 A[0] = -1 A[1] = 6 A[2] = 3
 A[3] =  4 A[4] = 7 A[5] = 4
there are four inversions:

   (1,2)  (1,3)  (1,5)  (4,5)
so the function should return 4.

Write an efficient algorithm for the following assumptions:

N is an integer within the range [0..100,000];
each element of array A is an integer within the range [−2,147,483,648..2,147,483,647].
*/
/*
//Less optimal
function solution(A) {
  let result = 0;

  for(let i = 0; i < A.length - 1; i++){
    const rest = A.slice(i + 1);
    const lowest = rest.filter(val => A[i] > val);
    result += lowest.length || 0;
  }

	return result > 1000000000 ? -1 : result;
}
*/

function splitArray(arr) {
	if (arr.length <= 1) {
    return 0;
  }
  
  const first = arr[0];
  const rest = arr.slice(1);
  const lowest = rest.filter((val) => first > val);
  return (lowest.length || 0) + splitArray(rest);
}

function solution(A) {
	const result = splitArray(A);
	/*let result = 0;

  for(let i = 0; i < A.length - 1; i++){
    const rest = A.slice(i + 1);
    const lowest = rest.filter(val => A[i] > val);
    result += lowest.length || 0;
  }*/

	return result > 1000000000 ? -1 : result;
}

console.log(solution([-1, 6, 3, 4, 7, 4]));
console.log(solution([0]));
console.log(solution([]));
console.log(solution([1, 2, 3]));
console.log(solution([1, 1, 1]));
