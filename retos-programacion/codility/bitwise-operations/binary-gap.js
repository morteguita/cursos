function binaryGap(int) {
	const binary = int.toString(2);
	const binaryArray = binary.split('1');
  binaryArray.pop();

	let max = 0;
	binaryArray.map((element) => {
		if (element.length > max) {
			max = element.length;
		}
	});

	return max;
}

const numbers = [9, 529, 32, 1041, 561892];

for (let index = 0; index < numbers.length; index++) {
	const element = numbers[index];
	console.log(element, binaryGap(element));
}
