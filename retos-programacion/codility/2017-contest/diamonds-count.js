/*
A diamond is a quadrilateral whose four sides all have the same length and whose diagonals are parallel to the coordinate axes.

You are given N distinct points on a plane. Count the number of different diamonds that can be constructed using these points as vertices (two diamonds are different if their sets of vertices are different). Do not count diamonds whose area is empty.

Write a function:

function solution(X, Y);

that, given two arrays X and Y, each containing N integers, representing N points (where X[K], Y[K] are the coordinates of the K-th point), returns the number of diamonds on the plane.

For example, for N = 7 points whose coordinates are specified in arrays X = [1, 1, 2, 2, 2, 3, 3] and Y = [3, 4, 1, 3, 5, 3, 4], the function should return 2, since we can find two diamonds as shown in the picture below:

Given arrays: X = [1, 2, 3, 3, 2, 1], Y = [1, 1, 1, 2, 2, 2], the function should return 0, since there are no diamonds on the plane:

Write an efficient algorithm for the following assumptions:

N is an integer within the range [4..1,500];
each element of arrays X and Y is an integer within the range [0..N-1];
given N points are pairwise distinct.
*/
function getDistance(p1, p2) {
	const c1 = Math.abs(p1.x - p2.x);
	const c2 = Math.abs(p1.y - p2.y);
	const h2 = Math.pow(c1, 2) + Math.pow(c2, 2);
	return Math.sqrt(h2);
}

function getDiagonals(points, p1) {
	const findDiagonals = points
		.filter((p) => p.x != p1.x && p.y != p1.y)
		.map((p) => {
			return { x: p.x, y: p.y, long: getDistance(p, p1) };
		});

	if (findDiagonals.length === 0) {
		return [];
	}

	return findDiagonals;
}

function solution2(X, Y) {
	const points = [];

	for (const i in X) {
		points.push({ x: X[i], y: Y[i] });
	}

	for (let i = 0; i < points.length - 1; i++) {
		const findDiagonals = getDiagonals(points, points[i]);
		console.log(points[i], '===>', findDiagonals);

		/*const isDiagonal = points[i].x !== points[i + 1].x && points[i].y !== points[i + 1].y;

    if(isDiagonal){
      const long = getDistance(points[i], points[i + 1]);
      const findDiagonals = points.filter((point) => getDistance(point, points[i + 1]) === long && point.x != points[i].x && point.y != points[i].y);
      //const findDiagonals = getDiagonals(points, points[i], points[i + 1], long);
      console.log(points[i], points[i + 1], "===>", findDiagonals);
    }*/
	}

	//console.log(points);
}

function solution(X, Y) {
	const l = X.length;
	const points = [];

	for (let i = 0; i < l; i++) {
		points.push([X[i], Y[i]]);
	}

	const byMeanX = {};
	const byMeanY = {};

	for (let i = 0; i < l - 1; i++) {
		for (let j = i + 1; j < l; j++) {
			const pair = [points[i], points[j]];

			if (pair[0][1] === pair[1][1]) {
				const meanX = (pair[0][0] + pair[1][0]) / 2;
				const iMeanX = Math.floor(meanX);

				if (meanX !== pair[0][0] && meanX === iMeanX) {
					if (!byMeanX[iMeanX]) {
						byMeanX[iMeanX] = {};
					}

					const xPy = pair[0][1];

					if (!byMeanX[iMeanX][xPy]) {
						byMeanX[iMeanX][xPy] = 1;
					} else {
						byMeanX[iMeanX][xPy]++;
					}
				}
			} else if (pair[0][0] === pair[1][0]) {
				const meanY = (pair[0][1] + pair[1][1]) / 2;
				const iMeanY = Math.floor(meanY);

				if (meanY !== pair[0][1] && meanY === iMeanY) {
					if (!byMeanY[iMeanY]) {
						byMeanY[iMeanY] = {};
					}

					const yPx = pair[0][0];

					if (!byMeanY[iMeanY][yPx]) {
						byMeanY[iMeanY][yPx] = 1;
					} else {
						byMeanY[iMeanY][yPx]++;
					}
				}
			}
		}
	}

	let d = 0;

	for (const y in byMeanY) {
		for (const x in byMeanY[y]) {
			if (byMeanX[x] && byMeanX[x][y]) {
				d += byMeanX[x][y] * byMeanY[y][x];
			}
		}
	}

	return d;
}

console.log(solution([1, 1, 2, 2, 2, 3, 3], [3, 4, 1, 3, 5, 3, 4]));
console.log(solution([1, 2, 3, 3, 2, 1], [1, 1, 1, 2, 2, 2]));
