/*
Bob is about to go on a trip. But first he needs to take care of his supply of socks. Each sock has its own color. Bob wants to take as many pairs of clean socks as possible (both socks in the pair should be of the same color).

Socks are divided into two drawers: clean and dirty socks. Bob has time for only one laundry and his washing machine can clean at most K socks. He wants to pick socks for laundering in such a way that after washing he will have a maximal number of clean, same-colored pairs of socks. It is possible that some socks cannot be paired with any other sock, because Bob may have lost some socks over the years.

Bob has exactly N clean and M dirty socks, which are described in arrays C and D, respectively. The colors of the socks are represented as integers (equal numbers representing identical colors).

For example, given four clean socks and five dirty socks:



If Bob's washing machine can clean at most K = 2 socks, then he can take a maximum of three pairs of clean socks. He can wash one red sock and one green sock, numbered 1 and 2 respectively. Then he will have two pairs of red socks and one pair of green socks.

Write a function:

function solution(K, C, D);

that, given an integer K (the number of socks that the washing machine can clean), two arrays C and D (containing the color representations of N clean and M dirty socks respectively), returns the maximum number of pairs of socks that Bob can take on the trip.

For example, given K = 2, C = [1, 2, 1, 1] and D = [1, 4, 3, 2, 4], the function should return 3, as explained above.

Assume that:

K is an integer within the range [0..50];
each element of arrays C and D is an integer within the range [1..50];
C and D are not empty and each of them contains at most 50 elements.
In your solution, focus on correctness. The performance of your solution will not be the focus of the assessment.
*/
/*function countSocks(arr) {
  return arr.reduce((countMap, element) => ({
      ...countMap,
      [element]: (countMap[element] || 0) + 1,
  }), {});
}

function getSocks(socks, isOdd) {
  return Object.entries(socks).filter(([element, count]) => count % 2 === (isOdd ? 1 : 0)).sort((a, b) => a[0] - b[0]);
}

function solution(K, C, D) {
  const countCleanSocks = countSocks(C);
  //const countDirtySocks = countSocks(D);

  console.log("CLEAN BUT ODD");
  //console.log(countCleanSocks);
  //console.log(getSocks(countCleanSocks, false));
  let getCleanSocks = getSocks(countCleanSocks, true);
  console.log(getCleanSocks);
  //console.log("DIRTY");
  //console.log(countDirtySocks);
  //console.log(getSocks(countDirtySocks, false));
  //console.log(getSocks(countDirtySocks, true));

  let washing = 0;
  while (washing < K) {
    const sock = D[washing];
    const filterSock = getCleanSocks.filter(([element, count]) => parseInt(element, 10) === sock);

    if(filterSock.length > 0) {
      console.log("FILTER: ", filterSock[0][0], "==>", getCleanSocks[filterSock[0][0]]);
      getCleanSocks[filterSock[0][0]][1] = getCleanSocks[filterSock[0][0]][1] + 1;
    }

    console.log(sock, "==>", getCleanSocks);
    washing++;
  }
}*/
/*
//Not working
function countSocks(arr) {
  return arr.reduce((countMap, element) => ({
      ...countMap,
      [element]: (countMap[element] || 0) + 1,
  }), {});
}

function solution(K, C, D) {
  const socksTravel = [];
  const cleanSocks = countSocks(C);

  if (K > 0) {
    const dirtySocks = [...D];
    let washed = 0;
    let i = 0;

    while (i < dirtySocks.length) {
      const sock = dirtySocks[i];
      
      if(cleanSocks[sock]) {
        if(cleanSocks[sock] % 2 === 1) {
          cleanSocks[sock] = cleanSocks[sock] + 1;
          D[i] = 0;
          washed++;
        }
      }

      if(washed === K){
        i = dirtySocks.length;
      }

      i++;
    }
  }

  Object.entries(cleanSocks).forEach(([element, count]) => {
    const pairs = Math.floor(count / 2);
    if(pairs >= 1) {
      for(let i = 0; i < pairs; i++) {
        socksTravel.push(parseInt(element, 10));
        cleanSocks[element] = cleanSocks[element] - 2;
      }
    } 
  });
  
  //console.log("TRAVEL:", socksTravel);
  //console.log("CLEAN:", cleanSocks);
  //console.log("DIRTY:", D.filter(val => val > 0));
  return socksTravel.length;
}*/

function countSocks(arr) {
  return arr.reduce((countMap, element) => ({
      ...countMap,
      [element]: (countMap[element] || 0) + 1,
  }), {});
}

function solution(K, C, D) {
	let c = countSocks(C);
	let d = countSocks(D);

	for (const sock in c) {
		if (c[sock] % 2 === 1) {
			if (K > 0 && d[sock] !== undefined) { //Can wash the sock
				c[sock] += 1;
				d[sock] -= 1;
				K -= 1;
			} else {
				c[sock] -= 1; //Discard the sock
			}
		}
	}

	let remainingSocks = Math.floor(K / 2);

  if (remainingSocks > 0) {
    for (const sock in d) {
      if (remainingSocks > 0) {
        const washedSocks = Math.min(Math.floor(d[sock] / 2), remainingSocks);
        c[sock] = (c[sock] || 0) + (washedSocks * 2);
        remainingSocks -= washedSocks;
      }
    }
  }

	return Object.values(c).reduce(
		(sum, count) => sum + Math.floor(count / 2),
		0
	);
}

console.log(solution(2, [1, 2, 1, 1], [1, 4, 3, 2, 4]));
console.log(solution(2, [1, 2, 1, 1, 1, 1], [1, 4, 3, 2, 4]));
console.log(solution(3, [1, 2, 1, 1, 1, 4, 4, 3, 1], [1, 4, 3, 2, 4]));
console.log(solution(2, [1], [3, 2, 5, 5]));
