/*
Write a function:

function solution(A);

that, given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.

For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.
Given A = [1, 2, 3], the function should return 4.
Given A = [−1, −3], the function should return 1.

Write an efficient algorithm for the following assumptions:

N is an integer within the range [1..100,000];
each element of array A is an integer within the range [−1,000,000..1,000,000].
*/

function solution(A) {
  const arrPos = A.filter(a => a > 0);

  if(arrPos.length > 0) {
      const max = Math.max(...arrPos);
      let min = max + 1;

      for(let i = 1; i <= max; i++){
          const exists = arrPos.indexOf(i);
          
          if(exists === -1 && min > i) {
              min = i;
          }
      }

      return min;
  }

  return 1;
}

function solution2(A) {
  const filteredArray = A.filter(value => value > 0);
  const sortedArray = filteredArray.sort((a, b) => a - b);

  let smallest = 1;
  for (const num of sortedArray) {
      if (num === smallest) {
          smallest++;
      } else if (num > smallest) {
          return smallest;
      }
  }

  return smallest;
}

function solution3(A) {
  const arrPos = A.filter(a => a > 0);
  const set = new Set(arrPos);

  for (let i = 1; i <= arrPos.length + 1; i++) {
      if (!set.has(i)) {
          return i;
      }
  }

  return 1;
}

// Ejemplos de prueba
console.log(solution3([1, 3, 6, 4, 1, 2]));
console.log(solution3([1, 2, 3]));
console.log(solution3([-1, -3])); 