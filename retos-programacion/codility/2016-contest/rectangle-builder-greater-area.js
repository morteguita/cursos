/*
Halfling Woolly Proudhoof is an eminent sheep herder. He wants to build a pen (enclosure) for his new flock of sheep. The pen will be rectangular and built from exactly four pieces of fence (so, the pieces of fence forming the opposite sides of the pen must be of equal length). Woolly can choose these pieces out of N pieces of fence that are stored in his barn. To hold the entire flock, the area of the pen must be greater than or equal to a given threshold X.

Woolly is interested in the number of different ways in which he can build a pen. Pens are considered different if the sets of lengths of their sides are different. For example, a pen of size 1×4 is different from a pen of size 2×2 (although both have an area of 4), but pens of sizes 1×2 and 2×1 are considered the same.

Write a function:

function solution(A, X);

that, given an array A of N integers (containing the lengths of the available pieces of fence) and an integer X, returns the number of different ways of building a rectangular pen satisfying the above conditions. The function should return −1 if the result exceeds 1,000,000,000.

For example, given X = 5 and the following array A:

  A[0] = 1
  A[1] = 2
  A[2] = 5
  A[3] = 1
  A[4] = 1
  A[5] = 2
  A[6] = 3
  A[7] = 5
  A[8] = 1

the function should return 2. The figure above shows available pieces of fence (on the left) and possible to build pens (on the right). The pens are of sizes 1x5 and 2x5. Pens of sizes 1×1 and 1×2 can be built, but are too small in area. It is not possible to build pens of sizes 2×3 or 3×5, as there is only one piece of fence of length 3.

Write an efficient algorithm for the following assumptions:

N is an integer within the range [0..100,000];
X is an integer within the range [1..1,000,000,000];
each element of array A is an integer within the range [1..1,000,000,000].
*/

/*while(area > X && i < X) {
  if (sortedPieces[i + 1]) {
    area = parseInt(sortedPieces[i][0], 10) * parseInt(sortedPieces[i + 1][0], 10);
    console.log(area, X);

    if(area >= X) {
      result++;
    } else {
      area = X;
      i = X;
    }
  }

  i++;
}*/

function dividePieces(arr) {
	const repeatedPieces = [[]];

	return arr.flatMap(([value, count]) => {
		if (count > 2) {
			const repetitions = parseInt(count, 10) || 1;

			if (repetitions > 2) {
				const result = [];
				let remaining = repetitions;

				while (remaining > 0) {
					const size = Math.min(remaining, 2);

					if (!repeatedPieces[value]) {
						repeatedPieces[value] = [];
					}

					if (!repeatedPieces[value][size]) {
						repeatedPieces[value][size] = 0;
					}

					repeatedPieces[value][size] += 1;

					if (repeatedPieces[value][size] <= 2) {
						result.push([value, size]);
					}

					remaining -= size;
				}

				return result;
			}

			return Array.from({ length: repetitions }, () => [value, 1]);
		}

		return [[value, count]];
	});
}

function findValidAreas(pieces, X) {
  if(pieces.length < 1) {
    return [];
  }

  let result = [];
  const initialPieces = pieces[0];
  const finalPieces = pieces.slice(1);

  for(let i = 0; i < finalPieces.length; i++) {
    const area = parseInt(initialPieces[0], 10) * parseInt(finalPieces[i], 10);

    if(area >= X){
      const max = Math.max(parseInt(initialPieces[0], 10), parseInt(finalPieces[i], 10));
      const min = Math.min(parseInt(initialPieces[0], 10), parseInt(finalPieces[i], 10));
      result.push([max, min]);
    }
  }

  return result.concat(findValidAreas(finalPieces, X));
}

function solution(A, X) {
  if (A.length < 4) {
    return 0;
  }

	const pieces = A.reduce((acc, val) => {
		acc[val] = (acc[val] || 0) + 1;
		return acc;
	}, {});

	const dividedPieces = dividePieces(Object.entries(pieces));
	const sortedPieces = dividedPieces
		.filter(([key, val]) => val > 1)
		.sort((a, b) => b[0] - a[0]);

  const validPieces = findValidAreas(sortedPieces, X);
  const uniquePieces = {};

  const noRepeatedPieces = validPieces.filter((item) => {
    const stringifiedItem = JSON.stringify(item);

    if (!uniquePieces[stringifiedItem]) {
      uniquePieces[stringifiedItem] = true;
      return true;
    }

    return false;
  });

  const result = noRepeatedPieces.length;
	return result > 1000000000 ? -1 : result;
}

console.log(solution([1, 2, 5, 1, 1, 2, 3, 5, 1], 5));
console.log(solution([], 1));
console.log(solution([6, 6, 6, 6, 6, 6], 10));
console.log(solution([2, 3, 2, 3, 2, 3, 2, 3], 9));
console.log(solution([2, 3, 2, 3, 2, 3, 2, 3, 4, 4], 9));
