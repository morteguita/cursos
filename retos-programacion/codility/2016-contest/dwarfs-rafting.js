/*
A company of dwarfs is travelling across the New Zealand. On reaching the Clutha River the dwarfs need to get across, but recent storms have washed away the bridge. Luckily, a small ferry, in the form of a square raft, is operating.

The raft is square and has N rows of seats, numbered from 1 to N. Each row contains N seats, labeled with consecutive letters (A, B, C, etc.). Each seat is identified by a string composed of its row number followed by its column number; for example, "9C" denotes the third seat in the 9th row.

The raft has already been loaded with barrels in some seat positions, and other seats are already occupied by dwarfs. Our company of dwarfs may only take the remaining unoccupied seats. The ferryman wants to accommodate as many dwarfs as possible, but the raft needs to be stable when making the crossing. That is, the following conditions must be satisfied:

the front and back halves of the raft (in terms of the rows of seats) must each contain the same number of dwarfs;
similarly, the left and right sides of the raft (in terms of the columns of seats) must each contain the same number of dwarfs.
You do not have to worry about balancing the barrels; you can assume that their weights are negligible.

For example, a raft of size N = 4 is shown in the following illustration:

Barrels are marked as brown squares, and seats that are already occupied by dwarfs are labeled d.

The positions of the barrels are given in string S. The occupied seat numbers are given in string T. The contents of the strings are separated by single spaces and may appear in any order. For example, in the diagram above, S = "1B 1C 4B 1D 2A" and T = "3B 2D".

In this example, the ferryman can accommodate at most six more dwarfs, as indicated by the green squares in the following diagram:

The raft is then balanced: both left and right halves have the same number of dwarfs (four), and both front and back halves have the same number of dwarfs (also four).

Write a function:

function solution(N, S, T);

that, given the size of the raft N and two strings S, T that describes the positions of barrels and occupied seats, respectively, returns the maximum number of dwarfs that can fit on the raft. If it is not possible to balance the raft with dwarfs, your function should return -1.

For instance, given N = 4, S = "1B 1C 4B 1D 2A" and T = "3B 2D", your function should return 6, as explained above.

Assume that:

N is an even integer within the range [2..26];
strings S, T consist of valid seat numbers, separated with spaces;
each seat number can appear no more than once in the strings;
no seat number can appear in both S and T simultaneously.
In your solution, focus on correctness. The performance of your solution will not be the focus of the assessment.
*/
const COLS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
const EMPTY_SEAT = '';
const BARREL_SEAT = 'B';
const DWARF_SEAT = 'd';

function occupySeat(matrix, arr, str) {
	arr.split(' ').map((s, i) => {
		const splitPos = s.split('');
		const row = parseInt(splitPos[0], 10) - 1;
		const col = COLS.indexOf(splitPos[1]);
		matrix[row][col] = str;
		return true;
	});
}

function checkQuadrant(x, y, matrix, half) {
	const x1 = x + half - 1;
	const y1 = y + half - 1;
	let availables = 0;
	let dwarfs = 0;

	for (let i = x; i <= x1; i++) {
		for (let j = y; j <= y1; j++) {
			if (matrix[i][j] !== BARREL_SEAT) {
				availables++;
			}

			if (matrix[i][j] === DWARF_SEAT) {
				dwarfs++;
			}
		}
	}

	return [availables, dwarfs];
}

function solution(N, S, T) {
	if (N < 2 || N % 2 != 0) {
		return 0;
	}

	const matrix = Array.from({ length: N }, () => Array(N).fill(EMPTY_SEAT));
	const half = N / 2;

	if (S.length > 0) occupySeat(matrix, S, BARREL_SEAT);
	if (T.length > 0) occupySeat(matrix, T, DWARF_SEAT);

	let [availableQ1, dwarfsQ1] = checkQuadrant(0, 0, matrix, half);
	let [availableQ2, dwarfsQ2] = checkQuadrant(0, half, matrix, half);
	let [availableQ3, dwarfsQ3] = checkQuadrant(half, 0, matrix, half);
	let [availableQ4, dwarfsQ4] = checkQuadrant(half, half, matrix, half);

	if (
		availableQ1 === 0 &&
		availableQ2 === 0 &&
		availableQ3 === 0 &&
		availableQ4 === 0
	) {
		return -1;
	}

	//Front: Q1 and Q2, Back: Q3 and Q4, Left: Q1 and Q3, Right: Q2 and Q4
	//Diagonal 1: Q1 and Q4, Diagonal 2: Q2 and Q3. Diagonals must have same quantity
	const minimumSeatsQ1 = Math.min(availableQ1, availableQ4);
	const minimumSeatsQ2 = Math.min(availableQ2, availableQ3);
	const minimumSeatsQ3 = Math.min(availableQ2, availableQ3);
	const minimumSeatsQ4 = Math.min(availableQ1, availableQ4);

	//Remove the seats previously occupied by dwarfs
	const sitNewDwarfsQ1 = minimumSeatsQ1 - dwarfsQ1;
	const sitNewDwarfsQ2 = minimumSeatsQ2 - dwarfsQ2;
	const sitNewDwarfsQ3 = minimumSeatsQ3 - dwarfsQ3;
	const sitNewDwarfsQ4 = minimumSeatsQ4 - dwarfsQ4;

	if (
		sitNewDwarfsQ1 < 0 ||
		sitNewDwarfsQ2 < 0 ||
		sitNewDwarfsQ3 < 0 ||
		sitNewDwarfsQ4 < 0
	) {
		return -1;
	}

	return sitNewDwarfsQ1 + sitNewDwarfsQ2 + sitNewDwarfsQ3 + sitNewDwarfsQ4;
}

console.log(solution(4, '1B 1C 4B 1D 2A', '3B 2D')); //6
console.log(solution(4, '1B 1C 4B 1D 2A', '')); //8
console.log(solution(4, '1A 1B 1D 2A 3B 3C 4B 4C', '')); //6
console.log(solution(2, '', '')); //4
console.log(solution(26, '', '')); //676
console.log(solution(2, '', '1A 1B 2A 2B')); //0
console.log(solution(2, '1A 1B 2A 2B', '')); //-1
console.log(solution(4, '1A 4D', '1D')); //13
console.log(solution(2, '', '1B')); //3
