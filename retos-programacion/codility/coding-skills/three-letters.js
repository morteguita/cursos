/*
Write a function solution that, given two integers A and B, returns a string containing exactly A letters 'a' and exactly B letters 'b' with no three consecutive letters being the same (in other words, neither "aaa" nor "bbb" may occur in the returned string).

Examples:

1. Given A = 5 and B = 3, your function may return "aabaabab". Note that "abaabbaa" would also be a correct answer. Your function may return any correct answer.
2. Given A = 3 and B = 3, your function should return "ababab", "aababb", "abaabb" or any of several other strings.
3. Given A = 1 and B = 4, your function should return "bbabb", which is the only correct answer in this case.

Assume that:

A and B are integers within the range [0..100];
at least one solution exists for the given A and B.
In your solution, focus on correctness. The performance of your solution will not be the focus of the assessment.
*/
function splitIntoChunks(inputString, chunkSize) {
	const result = [];

	for (let i = 0; i < inputString.length; i += chunkSize) {
		result.push(inputString.slice(i, i + chunkSize));
	}

	return result;
}

function solution(A, B) {
	if (A === 0 || B === 0) {
		return '';
	}

	const globalLimit = 2;
	const w1 = 'a';
	const w2 = 'b';
	const wordW1 = w1.repeat(A);
	const wordW2 = w2.repeat(B);

	let limitW1 = Math.ceil(A / B);
	let limitW2 = Math.ceil(B / A);

	while (limitW1 > globalLimit) {
		limitW1 = Math.ceil(limitW1 / 2);
	}

	while (limitW2 > globalLimit) {
		limitW2 = Math.ceil(limitW2 / 2);
	}

	const wordW1Chunks = splitIntoChunks(wordW1, limitW1);
	const wordW2Chunks = splitIntoChunks(wordW2, limitW2);
	const maxChunk = Math.max(wordW1Chunks.length, wordW2Chunks.length);
	const isW1Bigger = wordW1Chunks.length >= wordW2Chunks.length;

	let result = '';

	for (let i = 0; i < maxChunk; i++) {
		const ch1 = wordW1Chunks[i] || '';
		const ch2 = wordW2Chunks[i] || '';

		if (isW1Bigger) {
			result += ch1 + ch2;
		} else {
			result += ch2 + ch1;
		}
	}

	return result;
}

console.log(solution(5, 3)); //"aabaabab", "abaabbaa"
console.log(solution(3, 3)); //"ababab", "aababb", "abaabb"
console.log(solution(1, 4)); //"bbabb"
console.log(solution(4, 1)); //"aabaa"
console.log(solution(8, 4));
console.log(solution(4, 8));
console.log(solution(2, 8));
console.log(solution(0, 0));
console.log(solution(0, 1));
console.log(solution(1, 0));
console.log(solution(1, 1));
