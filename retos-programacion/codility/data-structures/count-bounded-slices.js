/*
An integer K and a non-empty array A consisting of N integers are given.

A pair of integers (P, Q), such that 0 ≤ P ≤ Q < N, is called a slice of array A.

A bounded slice is a slice in which the difference between the maximum and minimum values in the slice is less than or equal to K. More precisely it is a slice, such that max(A[P], A[P + 1], ..., A[Q]) − min(A[P], A[P + 1], ..., A[Q]) ≤ K.

The goal is to calculate the number of bounded slices.

For example, consider K = 2 and array A such that:

    A[0] = 3
    A[1] = 5
    A[2] = 7
    A[3] = 6
    A[4] = 3
There are exactly nine bounded slices: (0, 0), (0, 1), (1, 1), (1, 2), (1, 3), (2, 2), (2, 3), (3, 3), (4, 4).

Write a function:

function solution(K, A);

that, given an integer K and a non-empty array A of N integers, returns the number of bounded slices of array A.

If the number of bounded slices is greater than 1,000,000,000, the function should return 1,000,000,000.

For example, given:

    A[0] = 3
    A[1] = 5
    A[2] = 7
    A[3] = 6
    A[4] = 3
the function should return 9, as explained above.

Write an efficient algorithm for the following assumptions:

N is an integer within the range [1..100,000];
K is an integer within the range [0..1,000,000,000];
each element of array A is an integer within the range [−1,000,000,000..1,000,000,000].
*/
function solution(K, A) {
	if (K > 1000000000) {
		return 1000000000;
	}

	const N = A.length;
	let P = 0;

	let result = 0;
	let maxQueue = [];
	let minQueue = [];

	for (let Q = 0; Q < N; Q++) {
		// Actualiza la cola de máximos
		while (maxQueue.length > 0 && A[Q] > A[maxQueue[maxQueue.length - 1]]) {
			maxQueue.pop();
		}
		maxQueue.push(Q);

		// Actualiza la cola de mínimos
		while (minQueue.length > 0 && A[Q] < A[minQueue[minQueue.length - 1]]) {
			minQueue.pop();
		}
		minQueue.push(Q);

		// Verifica si la diferencia entre el máximo y el mínimo es mayor que K
		while (A[maxQueue[0]] - A[minQueue[0]] > K) {
			// Mueve el puntero izquierdo (P)
			P++;
			// Elimina elementos fuera del rango
			while (maxQueue.length > 0 && maxQueue[0] < P) {
				maxQueue.shift();
			}
			while (minQueue.length > 0 && minQueue[0] < P) {
				minQueue.shift();
			}
		}

		// Cuenta los slices válidos
		result += Q - P + 1;
	}

	return result;
}

const K = 2;
const A = [3, 5, 7, 6, 3];
console.log(solution(K, A));
