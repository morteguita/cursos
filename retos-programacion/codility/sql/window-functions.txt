PostgreSQL ofrece varias funciones de ventana (window functions) que te permiten realizar cálculos a lo largo de un conjunto de filas relacionadas con la fila actual. Aquí hay algunas funciones de ventana comunes en PostgreSQL:

==> 1. ROW_NUMBER():
Asigna un número de fila único a cada fila en una partición de resultados.

SELECT
  column1,
  column2,
  ROW_NUMBER() OVER (PARTITION BY column3 ORDER BY column4) AS row_num
FROM your_table;

==> 2. RANK() y DENSE_RANK():
Asignan un rango a las filas dentro de una partición. RANK() puede dejar huecos en el rango en caso de empates, mientras que DENSE_RANK() no.

SELECT
  column1,
  column2,
  RANK() OVER (PARTITION BY column3 ORDER BY column4) AS rank,
  DENSE_RANK() OVER (PARTITION BY column3 ORDER BY column4) AS dense_rank
FROM your_table;

==> 3. LEAD() y LAG():
Permiten acceder a valores de filas subsiguientes (LEAD) o anteriores (LAG) dentro de una partición.

SELECT
  column1,
  column2,
  LEAD(column2) OVER (PARTITION BY column3 ORDER BY column4) AS lead_value,
  LAG(column2) OVER (PARTITION BY column3 ORDER BY column4) AS lag_value
FROM your_table;

==> 4. SUM(), AVG(), MIN(), MAX(), COUNT():
Realizan cálculos agregados dentro de una partición.

SELECT
  column1,
  column2,
  SUM(column2) OVER (PARTITION BY column3) AS sum_value,
  AVG(column2) OVER (PARTITION BY column3) AS avg_value,
  MIN(column2) OVER (PARTITION BY column3) AS min_value,
  MAX(column2) OVER (PARTITION BY column3) AS max_value,
  COUNT(column2) OVER (PARTITION BY column3) AS count_value
FROM your_table;

===> 5. COALESCE
Se utiliza para devolver el primer valor no nulo de una lista de expresiones

COALESCE(valor1, valor2, ..., valorN)
SELECT COALESCE(null, 'Valor No Nulo', 'Otro Valor No Nulo');




Function

Description

row_number () → bigint

Returns the number of the current row within its partition, counting from 1.

rank () → bigint

Returns the rank of the current row, with gaps; that is, the row_number of the first row in its peer group.

dense_rank () → bigint

Returns the rank of the current row, without gaps; this function effectively counts peer groups.

percent_rank () → double precision

Returns the relative rank of the current row, that is (rank - 1) / (total partition rows - 1). The value thus ranges from 0 to 1 inclusive.

cume_dist () → double precision

Returns the cumulative distribution, that is (number of partition rows preceding or peers with current row) / (total partition rows). The value thus ranges from 1/N to 1.

ntile ( num_buckets integer ) → integer

Returns an integer ranging from 1 to the argument value, dividing the partition as equally as possible.

lag ( value anycompatible [, offset integer [, default anycompatible ]] ) → anycompatible

Returns value evaluated at the row that is offset rows before the current row within the partition; if there is no such row, instead returns default (which must be of a type compatible with value). Both offset and default are evaluated with respect to the current row. If omitted, offset defaults to 1 and default to NULL.

lead ( value anycompatible [, offset integer [, default anycompatible ]] ) → anycompatible

Returns value evaluated at the row that is offset rows after the current row within the partition; if there is no such row, instead returns default (which must be of a type compatible with value). Both offset and default are evaluated with respect to the current row. If omitted, offset defaults to 1 and default to NULL.

first_value ( value anyelement ) → anyelement

Returns value evaluated at the row that is the first row of the window frame.

last_value ( value anyelement ) → anyelement

Returns value evaluated at the row that is the last row of the window frame.

nth_value ( value anyelement, n integer ) → anyelement

Returns value evaluated at the row that is the n'th row of the window frame (counting from 1); returns NULL if there is no such row.