/*
Given a table events with the following structure:

  create table events (
      event_type integer not null,
      value integer not null,
      time timestamp not null,
      unique(event_type, time)
  );
write an SQL query that, for each event_type that has been registered more than once, returns the difference between the latest (i.e. the most recent in terms of time) and the second latest value. The table should be ordered by event_type (in ascending order).

For example, given the following data:

   event_type | value      | time
  ------------+------------+--------------------
   2          | 5          | 2015-05-09 12:42:00
   4          | -42        | 2015-05-09 13:19:57
   2          | 2          | 2015-05-09 14:48:30
   2          | 7          | 2015-05-09 12:54:39
   3          | 16         | 2015-05-09 13:19:57
   3          | 20         | 2015-05-09 15:01:09
your query should return the following rowset:

   event_type | value
  ------------+-----------
   2          | -5
   3          | 4
For the event_type 2, the latest value is 2 and the second latest value is 7, so the difference between them is −5.

The names of the columns in the rowset don't matter, but their order does.
*/
create table events (
  event_type integer not null,
  value integer not null,
  time timestamp not null,
  unique(event_type, time)
);
  
INSERT INTO events (event_type, value, time) VALUES (2, 5, '2015-05-09 12:42:00'),
(4, -42, '2015-05-09 13:19:57'),
(2, 2, '2015-05-09 14:48:30'),
(2, 7, '2015-05-09 12:54:39'),
(3, 16, '2015-05-09 13:19:57'),
(3, 20, '2015-05-09 15:01:09');

-- Using join
SELECT e1.event_type, (e1.value - e2.value) AS value
FROM events e1
  JOIN events e2 ON e1.event_type = e2.event_type
    AND e1.time = (SELECT time FROM events temp1 WHERE temp1.event_type = e1.event_type ORDER BY time DESC LIMIT 1)
    AND e2.time = (SELECT time FROM events temp2 WHERE temp2.event_type = e2.event_type ORDER BY time DESC LIMIT 1 OFFSET 1);

-- Using with
WITH ranked_values AS (
  SELECT
  	event_type,
    value,
    LEAD(value) OVER (PARTITION BY event_type ORDER BY time DESC) AS lead,
    ROW_NUMBER() OVER (PARTITION BY event_type ORDER BY time DESC) AS row_desc
  FROM events
),
row_values AS (
  SELECT event_type, value - lead AS total
  FROM ranked_values
  WHERE row_desc = 1
)
SELECT *
FROM row_values
WHERE total IS NOT NULL;

WITH 
cte AS (
  SELECT event_type, time, value
  FROM events e
  WHERE (
      SELECT COUNT(*)
      FROM events e1
      WHERE e1.event_type = e.event_type
          AND e1.time >= e.time
  ) <= 2
),
cte2 AS (
  SELECT event_type, LAG(value) OVER (PARTITION BY event_type ORDER BY time DESC) - value AS value
  FROM cte
)
SELECT *
FROM cte2
WHERE value IS NOT NULL;

-- Using subqueries
SELECT event_type, value
FROM
(
    -- LEAD is the next row
    SELECT event_type, value - LEAD(value) OVER (PARTITION BY event_type ORDER BY time DESC) AS value
    -- LAG is the previous row
    -- SELECT event_type, LAG(value) OVER (PARTITION BY event_type ORDER BY time DESC) - value AS value
    FROM events e
    WHERE (
        SELECT COUNT(*)
        FROM events e1
        WHERE e1.event_type = e.event_type
            AND e1.time >= e.time
    ) <= 2
    ORDER BY event_type, time DESC
) AS ef
WHERE value IS NOT NULL;
