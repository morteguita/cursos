create table teams (
  team_id integer not null,
  team_name varchar(30) not null,
  unique(team_id)
);

create table matches (
  match_id integer not null,
  host_team integer not null,
  guest_team integer not null,
  host_goals integer not null,
  guest_goals integer not null,
  unique(match_id)
);

INSERT INTO teams (team_id, team_name) VALUES
  (10, 'Give'),
  (20, 'Never'),
  (30, 'You'),
  (40, 'Up'),
  (50, 'Gonna');

INSERT INTO matches (match_id, host_team, guest_team, host_goals, guest_goals) VALUES
  (1, 30, 20, 1, 0),
  (2, 10, 20, 1, 2),
  (3, 20, 50, 2, 2),
  (4, 10, 30, 1, 0),
  (5, 30, 50, 0, 1);

WITH cte AS
(
SELECT
	host_team, guest_team,
	CASE
    	WHEN host_goals > guest_goals THEN 3
        WHEN host_goals = guest_goals THEN 1
        ELSE 0
    END AS host_points,
    CASE 
    	WHEN guest_goals > host_goals THEN 3
        WHEN host_goals = guest_goals THEN 1
        ELSE 0
    END AS guest_points
FROM matches
),
cte_host AS (
  SELECT host_team as team_id, SUM(host_points) AS points
  FROM cte
  GROUP BY host_team
),
cte_guest AS (
  SELECT guest_team as team_id, SUM(guest_points) AS points
  FROM cte
  GROUP BY guest_team
),
cte_total AS (
  SELECT team_id, SUM(points) AS pts
  FROM (
    SELECT *
    FROM cte_host
    UNION ALL
    SELECT *
    FROM cte_guest
  ) AS c
  GROUP BY team_id
)
SELECT t.team_id, t.team_name, COALESCE(pts, 0) num_points
FROM cte_total AS c
RIGHT JOIN teams t ON t.team_id = c.team_id
ORDER BY num_points DESC, t.team_id ASC;

-- SELECT * FROM cte_total;