CREATE TABLE employees (
  id INT PRIMARY KEY,
  name VARCHAR(100),
  department VARCHAR(50),
  salary INT
);

INSERT INTO employees VALUES
(1, 'John Doe', 'IT', 60000),
(2, 'Jane Smith', 'HR', 55000),
(3, 'Bob Johnson', 'IT', 70000),
(4, 'Alice Williams', 'Finance', 65000),
(5, 'Charlie Brown', 'Marketing', 75000),
(6, 'Diana Miller', 'HR', 60000),
(7, 'Eddie Davis', 'IT', 72000),
(8, 'Fiona Clark', 'Marketing', 68000),
(9, 'George Taylor', 'Finance', 71000),
(10, 'Helen Turner', 'Marketing', 70000);

-- 1. Escribe una consulta SQL para encontrar el nombre y salario del empleado con el salario más alto.
SELECT name, salary FROM employees ORDER BY salary DESC LIMIT 1;

-- 1.1. Dada la tabla employees, encuentra a los empleados que tienen el salario más alto en cada departamento.
WITH cte AS (
  SELECT e1.name, e1.department, e1.salary
  FROM employees e1
  WHERE e1.id = (SELECT id FROM employees temp1 WHERE temp1.department = e1.department ORDER BY salary DESC LIMIT 1)
)
SELECT * FROM cte ORDER BY salary DESC;

-- 1.2. Encuentra el promedio de salarios en el departamento de TI (IT) en la tabla employees.
SELECT AVG(salary) FROM employees WHERE department = 'IT';

-- 2. Escribe una consulta SQL para obtener el total de ventas para cada cliente.
CREATE TABLE orders (
  order_id INT PRIMARY KEY,
  customer_id INT,
  order_date DATE,
  total_amount DECIMAL(10, 2)
);

INSERT INTO orders VALUES
(1, 101, '2023-01-15', 150.50),
(2, 102, '2023-02-20', 200.75),
(3, 103, '2023-03-10', 120.00),
(4, 101, '2023-04-05', 90.25),
(5, 102, '2023-05-12', 180.30),
(6, 104, '2023-06-18', 250.00),
(7, 103, '2023-07-01', 130.50),
(8, 101, '2023-08-08', 300.40),
(9, 104, '2023-09-14', 170.75),
(10, 102, '2023-10-20', 220.90);

SELECT customer_id, SUM(total_amount) AS sales FROM orders GROUP BY customer_id;


-- 3. Escribe una consulta SQL para encontrar el nombre y la edad de los estudiantes mayores de 20 años.
CREATE TABLE students (
  student_id INT PRIMARY KEY,
  name VARCHAR(100),
  birthdate DATE
);

INSERT INTO students VALUES
(1, 'Michael Johnson', '2000-03-15'),
(2, 'Emily Davis', '1999-05-20'),
(3, 'Daniel Smith', '2006-02-10'),
(4, 'Sophia Taylor', '1998-11-05'),
(5, 'Matthew Brown', '2001-08-18'),
(6, 'Emma Clark', '2000-06-25'),
(7, 'Christopher Miller', '1999-04-12'),
(8, 'Ava Turner', '2005-01-30'),
(9, 'Andrew Wilson', '2004-09-22'),
(10, 'Olivia Harris', '1998-07-08');

-- AGE substracts two dates
SELECT name, birthdate FROM students WHERE EXTRACT(YEAR FROM AGE(CURRENT_DATE, birthdate)) > 20;


-- 4. Escribe una consulta SQL para contar el número de libros publicados por cada autor.
CREATE TABLE books (
  book_id INT PRIMARY KEY,
  title VARCHAR(200),
  author VARCHAR(100),
  publication_year INT
);

INSERT INTO books VALUES
(1, 'The Great Gatsby', 'F. Scott Fitzgerald', 1925),
(2, 'To Kill a Mockingbird', 'Harper Lee', 1960),
(3, '1984', 'George Orwell', 1949),
(4, 'The Catcher in the Rye', 'J.D. Salinger', 1951),
(5, 'Brave New World', 'Aldous Huxley', 1932),
(6, 'The Lord of the Rings', 'J.R.R. Tolkien', 1954),
(7, 'Pride and Prejudice', 'Jane Austen', 1813),
(8, 'The Hobbit', 'J.R.R. Tolkien', 1937),
(9, 'The Chronicles of Narnia', 'C.S. Lewis', 1950),
(10, 'One Hundred Years of Solitude', 'Gabriel García Márquez', 1967);

SELECT author, COUNT(*) as quantity FROM books GROUP BY author ORDER BY quantity DESC;


-- 5. Escribe una consulta SQL para encontrar los productos que están por debajo del nivel de reorden.
CREATE TABLE inventory (
  product_id INT PRIMARY KEY,
  product_name VARCHAR(200),
  stock_quantity INT,
  reorder_level INT
);

INSERT INTO inventory VALUES
(1, 'Laptop', 50, 10),
(2, 'Smartphone', 20, 100),
(3, 'Tablet', 30, 5),
(4, 'Desktop PC', 20, 8),
(5, 'Printer', 15, 3),
(6, 'Camera', 7, 17),
(7, 'Headphones', 40, 15),
(8, 'External Hard Drive', 35, 12),
(9, 'Wireless Mouse', 60, 18),
(10, 'Monitor', 8, 25);

SELECT *, reorder_level - stock_quantity AS buying_quantity FROM inventory WHERE stock_quantity < reorder_level;


-- 6. Usando las tablas students y books, encuentra a todos los estudiantes que nacieron después del año de publicación del libro que tiene el título más largo.
SELECT name, birthdate
FROM students
WHERE EXTRACT(YEAR FROM birthdate) > (SELECT publication_year FROM books ORDER BY LENGTH(title) DESC LIMIT 1);

-- 7. Encuentra a todos los estudiantes que han nacido en el mismo año que la fecha del libro más antiguo.
SELECT name, birthdate
FROM students
WHERE EXTRACT(YEAR FROM birthdate) = (SELECT publication_year FROM books ORDER BY publication_year ASC LIMIT 1);

-- 8. Write a query that returns a list of receivers that have received at least 1024 USD in at most 3 transactions. There can be more than 3 transters to that account, as long as some 3 or fewer transactions usd_value total to at least 1024 USD. The table should be ordered by name (in ascending order).
CREATE TABLE transactions (
  sent_from VARCHAR NOT NULL,
  receiver VARCHAR NOT NULL,
  date DATE NOT NULL,
  usd_value INT NOT NULL
);

INSERT INTO transactions (sent_from, receiver, date, usd_value) VALUES
('Jonas', 'Willhelm', '2000-01-01', 200),
('Jonas', 'Timpson', '2002-09-27', 1024),
('Jonas', 'Bjorn', '2001-03-16', 512),
('Willhelm', 'Bjorn', '2010-12-17', 100),
('Willhelm', 'Bjorn', '2004-03-22', 10),
('Brown', 'Bjorn', '2013-03-20', 500),
('Bjorn', 'Willhelm', '2007-06-02', 400),
('Bjorn', 'Willhelm', '2001-03-16', 400),
('Bjorn', 'Willhelm', '2001-03-16', 200);

WITH cte1 AS (
  SELECT receiver, usd_value, ROW_NUMBER() OVER (PARTITION BY receiver ORDER BY usd_value DESC) AS row_count
  FROM transactions t
),
cte2 AS (
  SELECT receiver, SUM(usd_value) AS total
  FROM cte1
  WHERE row_count <= 3
  GROUP BY receiver
)
SELECT *
FROM cte2
WHERE total >= 1024
ORDER BY receiver;

--
create table opinions (
          id int not null primary key,
          place varchar(255) not null,
          opinion varchar(255) not null
      );
      
insert into opinions values (1, 'mount nawo oz', 'recommended');
insert into opinions values (2, 'mount nawo oz', 'not recommended');
insert into opinions values (3, 'codility', 'recommended');
insert into opinions values (4, 'codility', 'recommended');
insert into opinions values (5, 'codility', 'recommended');
insert into opinions values (6, 'qr week', 'recommended');
insert into opinions values (7, 'qr week', 'not recommended');
insert into opinions values (8, 'cafe worst', 'not recommended');
insert into opinions values (9, 'mount nawo oz', 'recommended');

select place
from opinions 
group by place
having sum(case when opinion = 'recommended' then 1
                when opinion = 'not recommended' then -1
           end) > 0
order by place

-- 
CREATE TABLE credit_cards (
  card_number VARCHAR PRIMARY KEY,
  cardholder_name VARCHAR NOT NULL,
  credit_limit INT NOT NULL
);

CREATE TABLE transactions (
  transaction_id SERIAL PRIMARY KEY,
  card_number VARCHAR REFERENCES credit_cards(card_number),
  transaction_date DATE NOT NULL,
  amount INT NOT NULL
);

INSERT INTO credit_cards VALUES
  ('1234567890123456', 'John Doe', 3000),
  ('9876543210987654', 'Jane Smith', 8000);

INSERT INTO transactions (card_number, transaction_date, amount) VALUES
  ('1234567890123456', '2023-01-15', 2000),
  ('9876543210987654', '2023-01-16', 1200),
  ('1234567890123456', '2023-01-20', 500),
  ('9876543210987654', '2023-01-25', 1000),
  ('1234567890123456', '2023-01-28', 700),
  ('9876543210987654', '2023-02-02', 1500);

-- ¿Cómo seleccionarías el límite de crédito actual para un titular de tarjeta específico?
SELECT cardholder_name, credit_limit FROM credit_cards;

-- Encuentra el total gastado por cada titular de tarjeta en el mes de enero de 2023.
SELECT c.card_number, c.cardholder_name, SUM(t.amount) AS total
FROM credit_cards c
  INNER JOIN transactions t ON c.card_number = t.card_number
WHERE EXTRACT(YEAR FROM t.transaction_date) = '2023' AND EXTRACT(MONTH FROM t.transaction_date) = '01'
GROUP BY c.card_number, c.cardholder_name;

-- ¿Cómo determinarías si una transacción es elegible para el pago a plazos (cuotas)?
WITH cte AS (
  SELECT c.card_number, c.cardholder_name, SUM(t.amount) AS total_credits, c.credit_limit
  FROM credit_cards c
  	INNER JOIN transactions t ON c.card_number = t.card_number
  GROUP BY c.card_number, c.cardholder_name
)
SELECT c.*, CASE WHEN c.credit_limit > total_credits THEN true ELSE false END AS is_elegible
FROM cte c;

-- Escribe una consulta para calcular el saldo actual en una tarjeta después de todas las transacciones.
SELECT c.card_number, c.cardholder_name, credit_limit - SUM(t.amount) AS difference
  FROM credit_cards c
  	INNER JOIN transactions t ON c.card_number = t.card_number
GROUP BY c.card_number, c.cardholder_name;

-- Escribe una consulta para identificar transacciones atípicas (por ejemplo, transacciones significativamente mayores al promedio).
-- Media
SELECT card_number, AVG(amount) AS media
FROM transactions
GROUP BY card_number;

-- Mediana
WITH ranked_values AS (
  SELECT
  	card_number,
    amount,
    ROW_NUMBER() OVER (PARTITION BY card_number ORDER BY amount) AS row_asc,
    ROW_NUMBER() OVER (PARTITION BY card_number ORDER BY amount DESC) AS row_desc
  FROM transactions
)
SELECT card_number, AVG(amount) AS mid
FROM (
  SELECT card_number, amount
  FROM ranked_values
  WHERE row_asc = row_desc OR row_asc + 1 = row_desc OR row_asc = row_desc + 1
) AS s
GROUP BY card_number;

-- Moda
SELECT card_number, amount, COUNT(amount) as freq
FROM transactions
GROUP BY card_number, amount
ORDER BY freq DESC;

-- RANK y DENSE_RANK, RANK salta posiciones en el ranking cuando hay empate, DENSE_RANK no
SELECT
  card_number,
  amount,
  RANK() OVER (PARTITION BY card_number ORDER BY amount DESC) as ranking
FROM transactions;

SELECT
  card_number,
  amount,
  DENSE_RANK() OVER (PARTITION BY card_number ORDER BY amount DESC) as ranking
FROM transactions;