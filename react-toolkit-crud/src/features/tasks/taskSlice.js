import { createSlice } from '@reduxjs/toolkit';

const initialState = [
	{
		id: '1',
		title: 'Task 1',
		description: 'Description 1',
		completed: false,
	},
	{
		id: '2',
		title: 'Task 2',
		description: 'Description 2',
		completed: false,
	},
];

export const taskSlice = createSlice({
	name: 'tasks',
	initialState,
	reducers: {
		addTask: (state, action) => {
			return [...state, action.payload];
		},
		deleteTask: (state, action) => {
			const taskFound = state.find((task) => task.id === action.payload);

			if (taskFound) {
				state.splice(state.indexOf(taskFound), 1);
			}
		},
		updateTask: (state, action) => {
			const { id, task } = action.payload;
			const taskFound = state.find((task) => task.id === id);

			if (taskFound) {
				taskFound.title = task.title;
				taskFound.description = task.description;
			}
		},
	},
});

export const { addTask, deleteTask, updateTask } = taskSlice.actions;
export default taskSlice.reducer;
