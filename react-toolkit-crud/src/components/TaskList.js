import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { deleteTask } from '../features/tasks/taskSlice';

function TaskList() {
	const stateTasks = useSelector((state) => state.tasks);
	const dispatch = useDispatch();

	const handleDelete = (id) => {
		dispatch(deleteTask(id));
	};

	return (
		<div className="w-4/6">
			<header className="flex justify-between items-center py-4">
				<h2 className="font-bold">Task List ({stateTasks.length})</h2>
				<Link
					to={`/new`}
					className="bg-indigo-600 px-2 py-1 rounded-sm text-sm"
				>
					New Task
				</Link>
			</header>
			<section>
				<div className="grid grid-cols-3 gap-4">
					{stateTasks.map((task) => (
						<div key={task.id} className="bg-neutral-800 p-4 rounded-md">
							<header className="flex justify-between mb-3">
								<h3 className="font-bold">{task.title}</h3>
								<div className="flex gap-x-2">
									<Link
										to={`/edit/${task.id}`}
										className="bg-zinc-600 px-2 py-1 text-xs rounded-md"
									>
										Edit
									</Link>
									<button
										onClick={() => handleDelete(task.id)}
										className="bg-red-500 px-2 py-1 text-xs rounded-md"
									>
										Delete
									</button>
								</div>
							</header>
							<p>{task.description}</p>
						</div>
					))}
				</div>
			</section>
		</div>
	);
}

export default TaskList;
