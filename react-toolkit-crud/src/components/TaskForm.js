import { useEffect, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { v4 as uuid } from 'uuid';

import { addTask, updateTask } from '../features/tasks/taskSlice';

function TaskForm() {
	const [task, setTask] = useState({
		title: '',
		description: '',
	});
	const dispatch = useDispatch();
	const navigate = useNavigate();
	const params = useParams();
	const tasks = useSelector((state) => state.tasks);

	useEffect(() => {
		if (params.id) {
			const taskFound = tasks.find((task) => task.id === params.id);

			if (taskFound) {
				setTask(taskFound);
			}
		}
	}, [tasks, params.id]);

	const handleChange = (e) => {
		setTask({
			...task,
			[e.target.name]: e.target.value,
		});
	};

	const handleSubmit = (e) => {
		e.preventDefault();

		if (task.title.trim() === '' || task.description.trim() === '') {
			alert('Please enter a title and a description');
			return;
		}

		if (params.id) {
			dispatch(updateTask({ id: params.id, task }));
			navigate('/');
			return;
		}

		dispatch(
			addTask({
				...task,
				id: uuid(),
				completed: false,
			})
		);
		navigate('/');
	};

	return (
		<div className="w-4/6">
			<header className="flex justify-between items-center py-4">
				<h2 className="font-bold">Create Task</h2>
				<Link to={`/`} className="bg-indigo-600 px-2 py-1 rounded-sm text-sm">
					Return to Task List
				</Link>
			</header>
			<section>
				<form onSubmit={handleSubmit} className="bg-zinc-800 max-w-sm p-4">
					<label htmlFor="title" className="font-bold block text-sm mb-2">
						Task:
					</label>
					<input
						type="text"
						name="title"
						placeholder="title"
						value={task.title}
						onChange={handleChange}
						className="w-full p-2 rounded-md bg-zinc-600 mb-2"
					/>
					<label htmlFor="description" className="font-bold block text-sm mb-2">
						Description:
					</label>
					<textarea
						name="description"
						placeholder="description"
						onChange={handleChange}
						value={task.description}
						className="w-full p-2 rounded-md bg-zinc-600 mb-2"
					></textarea>
					<div className="flex justify-end">
						<button
							type="submit"
							className="bg-indigo-600 px-2 py-1 rounded-sm text-sm"
						>
							Save
						</button>
					</div>
				</form>
			</section>
		</div>
	);
}

export default TaskForm;
