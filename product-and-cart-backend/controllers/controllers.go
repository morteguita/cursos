package controllers

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/morteguita/cursos/productandcartbackend/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const connectionString = "mongodb://localhost:27017/?readPreference=primary&ssl=false"
const dbName = "product-and-cart"
const tableProducts = "products"
const tableOrders = "orders"
const tableCurrencies = "currencies"

var collectionProducts *mongo.Collection
var collectionOrders *mongo.Collection
var collectionCurrencies *mongo.Collection

func init() {
	clientOptions := options.Client().ApplyURI(connectionString)
	client, err := mongo.Connect(context.TODO(), clientOptions)
	showError(err, "init")
	fmt.Println("MongoDB connection success")

	collectionProducts = client.Database(dbName).Collection(tableProducts)
	fmt.Printf("Collection %s instance is ready\n", tableProducts)

	collectionOrders = client.Database(dbName).Collection(tableOrders)
	fmt.Printf("Collection %s instance is ready\n", tableOrders)

	collectionCurrencies = client.Database(dbName).Collection(tableCurrencies)
	fmt.Printf("Collection %s instance is ready\n", tableCurrencies)
}

func setHeaders(w *http.ResponseWriter) {
	(*w).Header().Set("Content-Type", "application/json")
}

func showError(err error, funct string) {
	if err != nil {
		fmt.Println(funct+" ERROR ", err)
	}
}

/* Mongo functions */
func getAllDBProducts() []primitive.M {
	filter := bson.D{}
	cur, err := collectionProducts.Find(context.Background(), filter)
	showError(err, "getAllDBProducts")

	var products []primitive.M
	for cur.Next(context.Background()) {
		var product bson.M
		err := cur.Decode(&product)
		showError(err, "getAllDBProducts Decode")
		products = append(products, product)
	}
	defer cur.Close(context.Background())

	return products
}

func getRecommendedDBProducts(size int64) []primitive.M {
	filter := bson.D{{"$sample", bson.D{{"size", size}}}}
	opts := options.Aggregate().SetMaxTime(2 * time.Second)
	cur, err := collectionProducts.Aggregate(context.Background(), mongo.Pipeline{filter}, opts)
	showError(err, "getAllDBProducts")

	var products []primitive.M
	for cur.Next(context.Background()) {
		var product bson.M
		err := cur.Decode(&product)
		showError(err, "getAllDBProducts Decode")
		products = append(products, product)
	}
	defer cur.Close(context.Background())

	return products
}

func getOneDBProduct(productId string) bson.M {
	id, _ := primitive.ObjectIDFromHex(productId)
	filter := bson.M{"_id": id}

	var product bson.M
	err := collectionProducts.FindOne(context.Background(), filter).Decode(&product)
	showError(err, "getOneDBProduct")

	return product
}

func createProductDB(product models.Product) primitive.ObjectID {
	inserted, err := collectionProducts.InsertOne(context.Background(), product)
	showError(err, "createProductDB")
	return inserted.InsertedID.(primitive.ObjectID)
}

func getAllDBOrders() []primitive.M {
	filter := bson.D{}
	findOptions := options.Find()
	findOptions.SetSort(bson.D{{"date", -1}})
	cur, err := collectionOrders.Find(context.Background(), filter, findOptions)
	showError(err, "getAllDBOrders")

	var orders []primitive.M
	for cur.Next(context.Background()) {
		var order bson.M
		err := cur.Decode(&order)
		showError(err, "getAllDBOrders Decode")

		orders = append(orders, order)
	}
	defer cur.Close(context.Background())

	return orders
}

func createOrderDB(order models.Order) primitive.ObjectID {
	inserted, err := collectionOrders.InsertOne(context.Background(), order)
	showError(err, "createOrderDB")
	return inserted.InsertedID.(primitive.ObjectID)
}

func getAllDBCurrencies() []primitive.M {
	filter := bson.D{}
	cur, err := collectionCurrencies.Find(context.Background(), filter)
	showError(err, "getAllDBCurrencies")

	var currencies []primitive.M
	for cur.Next(context.Background()) {
		var currency bson.M
		err := cur.Decode(&currency)
		showError(err, "getAllDBCurrencies Decode")
		currencies = append(currencies, currency)
	}
	defer cur.Close(context.Background())

	return currencies
}

func createCurrencyDB(currency models.Currency) primitive.ObjectID {
	inserted, err := collectionCurrencies.InsertOne(context.Background(), currency)
	showError(err, "createCurrencyDB")
	return inserted.InsertedID.(primitive.ObjectID)
}

/* Server functions */
func GetAllProducts(w http.ResponseWriter, r *http.Request) {
	setHeaders(&w)
	allProducts := getAllDBProducts()
	json.NewEncoder(w).Encode(allProducts)
}

func GetRecommendedProducts(w http.ResponseWriter, r *http.Request) {
	setHeaders(&w)
	params := mux.Vars(r)
	size, _ := strconv.ParseInt(params["size"], 0, 8)
	recommendedProducts := getRecommendedDBProducts(size)
	json.NewEncoder(w).Encode(recommendedProducts)
}

func GetProduct(w http.ResponseWriter, r *http.Request) {
	setHeaders(&w)
	params := mux.Vars(r)
	product := getOneDBProduct(params["id"])
	json.NewEncoder(w).Encode(product)
}

func CreateProduct(w http.ResponseWriter, r *http.Request) {
	setHeaders(&w)
	var product models.Product
	_ = json.NewDecoder(r.Body).Decode(&product)
	id := createProductDB(product)
	product.ID = id
	json.NewEncoder(w).Encode(product)
}

func GetAllOrders(w http.ResponseWriter, r *http.Request) {
	setHeaders(&w)
	allOrders := getAllDBOrders()
	json.NewEncoder(w).Encode(allOrders)
}

func CreateOrder(w http.ResponseWriter, r *http.Request) {
	setHeaders(&w)
	var order models.Order
	_ = json.NewDecoder(r.Body).Decode(&order)
	id := createOrderDB(order)
	order.ID = id
	json.NewEncoder(w).Encode(order)
}

func GetAllCurrencies(w http.ResponseWriter, r *http.Request) {
	setHeaders(&w)
	allCurrencies := getAllDBCurrencies()
	json.NewEncoder(w).Encode(allCurrencies)
}

func CreateCurrency(w http.ResponseWriter, r *http.Request) {
	setHeaders(&w)
	var currency models.Currency
	_ = json.NewDecoder(r.Body).Decode(&currency)
	id := createCurrencyDB(currency)
	currency.ID = id
	json.NewEncoder(w).Encode(currency)
}
