package main

import (
	"fmt"
	"net/http"

	"gitlab.com/morteguita/cursos/productandcartbackend/router"
)

func main() {
	fmt.Println("MongoDB API")
	fmt.Println("Server initialized...")

	r := router.Router()
	http.ListenAndServe(":8000", r)
}
