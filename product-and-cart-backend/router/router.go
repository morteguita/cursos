package router

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/morteguita/cursos/productandcartbackend/controllers"
)

func enableCORS(router *mux.Router) {
	router.PathPrefix("/").HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "http://localhost:8080")
	}).Methods(http.MethodOptions)
	router.Use(middlewareCors)
}

func middlewareCors(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, req *http.Request) {
			w.Header().Set("Access-Control-Allow-Origin", "http://localhost:8080")
			w.Header().Set("Access-Control-Allow-Credentials", "true")
			w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
			w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
			next.ServeHTTP(w, req)
		})
}

func Router() *mux.Router {
	router := mux.NewRouter()
	enableCORS(router)

	router.HandleFunc("/api/currencies", controllers.GetAllCurrencies).Methods("GET")
	router.HandleFunc("/api/currency", controllers.CreateCurrency).Methods("POST")
	router.HandleFunc("/api/recommended-products/{size}", controllers.GetRecommendedProducts).Methods("GET")
	router.HandleFunc("/api/products", controllers.GetAllProducts).Methods("GET")
	router.HandleFunc("/api/product/{id}", controllers.GetProduct).Methods("GET")
	router.HandleFunc("/api/product", controllers.CreateProduct).Methods("POST")
	router.HandleFunc("/api/orders", controllers.GetAllOrders).Methods("GET")
	router.HandleFunc("/api/order", controllers.CreateOrder).Methods("POST")

	return router
}
