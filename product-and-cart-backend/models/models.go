package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Currency struct {
	ID     primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Name   string             `json:"name"`
	Symbol string             `json:"symbol"`
}

type Price struct {
	Currency string  `json:"currency"`
	Value    float32 `json:"value"`
}

type Product struct {
	ID     primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Name   string             `json:"name"`
	Icon   string             `json:"icon"`
	Type   string             `json:"type"`
	Prices []Price            `json:"prices"`
}

type ProductOrder struct {
	ID       primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Price    float32            `json:"price"`
	Quantity uint               `json:"quantity"`
}

type Order struct {
	ID       primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Products []ProductOrder     `json:"products"`
	Date     string             `json:"date"`
	Currency string             `json:"currency"`
	Total    float32            `json:"total"`
}
