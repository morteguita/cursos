import { createApp } from 'vue'
import App from './App.vue'
import store from './store'
import { createRouter, createWebHistory } from 'vue-router'
import VueProgressBar from "@aacassandra/vue3-progressbar";
import ListCharacters from './components/ListCharacters.vue'

const routes = [
  { path: '/', component: ListCharacters },
  { path: '/index', component: ListCharacters },
]

const router = createRouter({
  history: createWebHistory(),
  routes: routes
})

const optionsProgressBar = {
  color: 'var(--background-orange)',
  failedColor: 'var(--background-red)',
  thickness: '4px',
  transition: {
    speed: '0.2s',
    opacity: '0.5s',
    termination: 300,
  },
  autoRevert: true,
  location: 'top',
  inverse: false,
};

createApp(App).use(store).use(VueProgressBar, optionsProgressBar).use(router).mount('#app')
