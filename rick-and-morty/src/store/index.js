import { createStore } from 'vuex'

export default createStore({
  state: {
    characters: [],
    charactersFilter: [],
    currentCharacter: {},
    currentCharacterEpisodes: [],
    currentPage: 1,
    pages: 0,
    status: '',
    name: '',
  },
  mutations: {
    //Modifican los states
    setCharacters(state, payload){
      state.characters = payload;
    },
    setCharactersFilter(state, payload){
      state.charactersFilter = payload;
    },
    setCurrentCharacter(state, currentCharacter){
      state.currentCharacter = currentCharacter;
    },
    setCurrentCharacterEpisodes(state, currentCharacterEpisodes){
      state.currentCharacterEpisodes = currentCharacterEpisodes;
    },
    setCurrentPage(state, currentPage){
      state.currentPage = currentPage;
    },
    setPages(state, pages){
      state.pages = pages;
    },
    setStatus(state, status){
      state.status = status;
    },
    setName(state, name){
      state.name = name;
    }
  },
  actions: {
    //Acciones para modificar los states con las mutations
    async getCharacters({commit}, page = 1){
      //Obtiene los personajes segun la pagiina
      try {
        const response = await fetch(`${process.env.VUE_APP_URL_CHARACTERS}?page=${page}`);
        const data = await response.json();
        
        //Se setean las variables
        commit('setCharacters', data.results);
        commit('setCharactersFilter', data.results);
        commit('setPages', data.info.pages);
        commit('setCurrentPage', page);

        //Se ejecutan los filtros ingresados
        this.dispatch('filter');
      } catch (error) {
        console.error(error);
      }
    },
    async setCurrentCharacter({commit}, id = 1){
      //Obtiene el personaje segun el id
      try {
        const response = await fetch(`${process.env.VUE_APP_URL_CHARACTERS}/${id}`);
        const data = await response.json();
        const episodes = [];

        //Episodios del personaje
        if(typeof(data.episode) !== 'undefined'){
          for(const ep of data.episode){
            const responseEpisode = await fetch(ep);
            const dataEpisode = await responseEpisode.json();
            episodes.push(dataEpisode);
          }
        }

        commit('setCurrentCharacter', data);
        commit('setCurrentCharacterEpisodes', episodes);
      } catch (error) {
        console.error(error);
      }
    },
    setStatus({commit}, status){
      //Actualiza el estado y filtra
      commit('setStatus', status);
      this.dispatch('filter');
    },
    setName({commit}, name){
      //Actualiza el nombre y filtra
      commit('setName', name);
      this.dispatch('filter');
    },
    filter({commit, state}){
      //Filtra por los atributos definidos
      const formatName = state.name.toLowerCase();
      let results = state.characters.filter((character) => {
        return character.status.includes(state.status);
      });
      results = results.filter((character) => {
        const characterName = character.name.toLowerCase();
        if(characterName.includes(formatName)){
          return character;
        }
      });
      commit('setCharactersFilter', results);
    },
  },
  modules: {
  }
})
