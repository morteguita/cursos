package controller

import (
	"encoding/json"
	"getgoing2/model"
	"getgoing2/views"
	"net/http"
)

func crud() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodPost {
			data := views.PostRequest{}
			json.NewDecoder(r.Body).Decode(&data)

			//Take data and save it
			if err := model.CreateTodo(data.Name, data.Todo); err != nil {
				w.Write([]byte("Some error"))
				return
			}

			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusCreated) //201
			json.NewEncoder(w).Encode(data)
		} else if r.Method == http.MethodGet {
			var data []views.PostRequest
			var err error

			name := r.URL.Query().Get("name")
			if name == "" {
				data, err = model.ReadAll()
			} else {
				data, err = model.ReadByName(name)
			}

			if err != nil {
				w.Write([]byte(err.Error()))
			}

			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK) //200
			json.NewEncoder(w).Encode(data)
		} else if r.Method == http.MethodDelete {
			name := r.URL.Path[1:] //Remove the "/" in the path url

			if name != "" {
				if err := model.DeleteTodo(name); err != nil {
					w.Write([]byte("Some error"))
					return
				}

				data := views.DeleteRequest{
					Status: "Item deleted",
				}

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusOK) //200
				json.NewEncoder(w).Encode(data)
			} else {
				w.WriteHeader(http.StatusBadRequest) //400
			}
		}
	}
}
