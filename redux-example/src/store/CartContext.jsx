import { createContext, useReducer } from 'react';

export const CartContext = createContext({
	items: [],
	addItem: (item) => {},
	updateQuantity: (id, quantity) => {},
	removeItem: (id) => {},
	clearCart: () => {},
});

export default function CartContextProvider({ children }) {
	const [cartState, dispatchCartAction] = useReducer(cartReducer, {
		items: [],
	});

	function addItem(item) {
		dispatchCartAction({ type: 'ADD_ITEM', payload: item });
	}

	function updateQuantity(id, quantity) {
		dispatchCartAction({ type: 'UPDATE_ITEM', payload: { id, quantity } });
	}

	function removeItem(id) {
		dispatchCartAction({ type: 'REMOVE_ITEM', payload: id });
	}

	function clearCart() {
		dispatchCartAction({ type: 'CLEAR_CART' });
	}

	return (
		<CartContext.Provider
			value={{
				items: cartState.items,
				addItem: addItem,
				updateQuantity: updateQuantity,
				removeItem: removeItem,
				clearCart: clearCart,
			}}
		>
			{children}
		</CartContext.Provider>
	);
}

function cartReducer(state, action) {
	switch (action.type) {
		case 'ADD_ITEM':
			const item = state.items.find((item) => item.id === action.payload.id);

			if (item) {
				return {
					...state,
					items: state.items.map((item) =>
						item.id === action.payload.id
							? { ...item, quantity: item.quantity + 1 }
							: item
					),
				};
			}

			return {
				...state,
				items: [...state.items, { ...action.payload, quantity: 1 }],
			};
		case 'UPDATE_ITEM':
      if (action.payload.quantity <= 0) {
        return {
          ...state,
          items: state.items.filter((item) => item.id !== action.payload.id),
        };
      }

			return {
				...state,
				items: state.items.map((item) =>
					item.id === action.payload.id
						? { ...item, quantity: action.payload.quantity }
						: item
				),
			};
		case 'REMOVE_ITEM':
			return {
				...state,
				items: state.items.filter((item) => item.id !== action.payload),
			};
		case 'CLEAR_CART':
			return { ...state, items: [] };
		default:
			return state;
	}
}
