import './App.scss';
import CartContextProvider from './store/CartContext';
import Products from './components/Products';
import Header from './components/Header';

function App() {
	return (
		<CartContextProvider>
			<div className="App">
				<Header />
        <Products />
			</div>
		</CartContextProvider>
	);
}

export default App;
