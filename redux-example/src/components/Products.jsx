import './Products.scss';

import { DUMMY_PRODUCTS } from '../dummy-products.js';
import ProductItem from './ProductItem.jsx';

function Products() {
	return (
		<section>
			<h2>Our Products</h2>
			<div className="productsGrid">
				{DUMMY_PRODUCTS.map((product) => (
					<ProductItem key={product.id} product={product} />
				))}
			</div>
		</section>
	);
}

export default Products;
