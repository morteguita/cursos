import { useContext } from 'react';
import logo from '../logo.svg';
import './Header.scss';

import { CartContext } from '../store/CartContext';

function Header() {
	const { items } = useContext(CartContext);
	const totalItems = items.reduce((acc, item) => item.quantity + acc, 0);

	return (
		<header className="appHeader">
			<div className="appGrid">
				<img src={logo} className="appLogo" alt="logo" />
				<div>
					<p className="cartItems">Cart items: <b>{totalItems}</b></p>
				</div>
			</div>
		</header>
	);
}

export default Header;
