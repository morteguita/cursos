import './ProductItem.scss';
import { useContext } from 'react';

import { CartContext } from '../store/CartContext';

function ProductItem({ product }) {
	const { items, addItem, updateQuantity } = useContext(CartContext);
	const itemExists = items.filter((item) => item.id === product.id);
	const item = itemExists.length > 0 ? itemExists[0] : null;
  const formattedTotalPrice = (item ? item.quantity : 0) * product.price;

	return (
		<div className="product">
			<h3>{product.title}</h3>
			<p>{product.description}</p>
			<p className="priceContainer">Unit price: <span className="price">${product.price}</span></p>
			<p className="priceContainer">Total price: <span className="price">${formattedTotalPrice.toFixed(2)}</span></p>
			<div className="btnContainer">
				{!item && (
					<button className="btnAddCart" onClick={() => addItem(product)}>
						Add to cart
					</button>
				)}
				{item && (
					<div>
						<button
							className="btnRemoveCart"
							onClick={() =>
								updateQuantity(product.id, item.quantity - 1)
							}
						>
							-
						</button>
						<span className="quantity">{item.quantity}</span>
						<button
							className="btnAddCart"
							onClick={() =>
								updateQuantity(product.id, item.quantity + 1)
							}
						>
							+
						</button>
					</div>
				)}
			</div>
		</div>
	);
}

export default ProductItem;
