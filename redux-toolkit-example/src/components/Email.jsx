import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { changeEmail } from '../redux/userSlice';

export const Email = () => {
	const dispatch = useDispatch();
	const user = useSelector((state) => state.user);
	const [email, setEmail] = useState('');

	useEffect(() => {
		setEmail(user.email);
	}, [user.email]);

	const handleChangeEmail = () => {
		dispatch(changeEmail(email));
	};

	return (
		<div className="Email">
			<h1>Email</h1>
			<input
				type="email"
				value={email}
				placeholder="email"
				onChange={(e) => setEmail(e.target.value)}
			/>
			<button onClick={handleChangeEmail}>Change Email</button>
		</div>
	);
};
