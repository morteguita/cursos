import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import CestaView from '../views/CestaView.vue'
import FavoritosView from '../views/FavoritosView.vue'
import HistorialView from '../views/HistorialView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/cesta',
    name: 'cesta',
    component: CestaView
  },
  {
    path: '/favoritos',
    name: 'favoritos',
    component: FavoritosView
  },
  {
    path: '/historial',
    name: 'historial',
    component: HistorialView
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
