package models

import (
	"database/sql"
	"fmt"
	"log"
)

var con *sql.DB

func Connect() *sql.DB {
	db, err := sql.Open("mysql", "root@tcp(localhost:3306)/golang")

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to database...")
	con = db
	return db
}

func GetConnection() *sql.DB {
	return con
}
