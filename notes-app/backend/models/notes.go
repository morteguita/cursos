package models

type Notes struct {
	Id          int32  `json:"id"`
	Title       string `json:"title"`
	Note        string `json:"note"`
	DateCreated string `json:"dateCreated"`
	DateUpdated string `json:"dateUpdated"`
	Completed   bool   `json:"completed"`
}
