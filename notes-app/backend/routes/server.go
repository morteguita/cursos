package routes

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

var app *fiber.App

func Setup() {
	app = fiber.New()
	app.Use(cors.New(cors.Config{
		AllowOrigins: "http://localhost:8080",
		AllowHeaders: "Origin, Content-Type, Accept",
	}))

	app.Get("/api/allnotes", GetAllNotes)
	app.Get("/api/note/:id", GetNote)
	app.Put("/api/note/:id", UpdateNote)
	app.Post("/api/note", CreateNote)
	app.Delete("/api/note/:id", DeleteNote)

	app.Listen(":5000")
}
