package routes

import (
	"database/sql"
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/morteguita/cursos/notes-app/models"
)

func GetAllNotes(c *fiber.Ctx) error {
	//SQL Query for all notes
	rows, err := models.GetConnection().Query("SELECT * FROM notes_app")

	if err != nil {
		return c.JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	allRows := []models.Notes{}
	for rows.Next() {
		data := models.Notes{}
		rows.Scan(&data.Id, &data.Title, &data.Note, &data.DateCreated, &data.DateUpdated, &data.Completed)
		allRows = append(allRows, data)
	}

	return c.JSON(allRows)
}

func GetNote(c *fiber.Ctx) error {
	//Get the id
	id, err := strconv.Atoi(c.Params("id"))

	if err != nil {
		return c.JSON(fiber.Map{
			"id":      id,
			"message": err.Error(),
		})
	}

	//SQL Query for the actual note
	var note = models.Notes{}
	row := models.GetConnection().QueryRow("SELECT * FROM notes_app WHERE Id = ?", id)
	err = row.Scan(&note.Id, &note.Title, &note.Note, &note.DateCreated, &note.DateUpdated, &note.Completed)

	if err != nil && err != sql.ErrNoRows {
		return c.JSON(fiber.Map{
			"message": err.Error(),
		})
	} else if err == sql.ErrNoRows {
		return c.JSON(fiber.Map{})
	}

	return c.JSON(note)
}

func UpdateNote(c *fiber.Ctx) error {
	//Get the id
	id, err := strconv.Atoi(c.Params("id"))

	if err != nil {
		return c.JSON(fiber.Map{
			"id":      id,
			"message": err.Error(),
		})
	}

	//Get the parameters
	note := new(models.Notes)

	if err := c.BodyParser(note); err != nil {
		return err
	}

	//Update the note in the DB
	tm := time.Now()
	frmt := tm.Format("2006-01-02 15:04:05")

	updateQ, err := models.GetConnection().Query("UPDATE notes_app SET Title = ?, Note = ?, DateUpdated = ?, Completed = ? WHERE Id = ?", note.Title, note.Note, frmt, note.Completed, id)

	if err != nil {
		return c.JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	defer updateQ.Close()

	return c.JSON(fiber.Map{
		"id":      id,
		"message": "Note successfully updated!",
	})
}

func CreateNote(c *fiber.Ctx) error {
	note := new(models.Notes)

	if err := c.BodyParser(note); err != nil {
		return c.JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	//Create the note in the DB
	tm := time.Now()
	frmt := tm.Format("2006-01-02 15:04:05")

	insertQ, err := models.GetConnection().Query("INSERT INTO notes_app (Title, Note, DateCreated, Completed) VALUES (?, ?, ?, ?)", note.Title, note.Note, frmt, note.Completed)

	if err != nil {
		return c.JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	defer insertQ.Close()

	return c.JSON(fiber.Map{
		"message": "Note successfully created!",
	})
}

func DeleteNote(c *fiber.Ctx) error {
	//Get the id
	id, err := strconv.Atoi(c.Params("id"))

	if err != nil {
		return c.JSON(fiber.Map{
			"id":      id,
			"message": err.Error(),
		})
	}

	//Delete the note in the DB
	deleteQ, err := models.GetConnection().Query("DELETE FROM notes_app WHERE Id = ?", id)

	if err != nil {
		return c.JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	defer deleteQ.Close()

	return c.JSON(fiber.Map{
		"id":      id,
		"message": "Note successfully deleted!",
	})
}
