package main

import (
	"gitlab.com/morteguita/cursos/notes-app/models"
	"gitlab.com/morteguita/cursos/notes-app/routes"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	db := models.Connect()
	defer db.Close()
	routes.Setup()
}
