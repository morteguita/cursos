import { createApp } from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import App from './App.vue'
import VueProgressBar from "@aacassandra/vue3-progressbar"
import { library } from "@fortawesome/fontawesome-svg-core"
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome"
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { dom } from "@fortawesome/fontawesome-svg-core"

//Iconos
library.add(fas)
library.add(fab)
library.add(far)
dom.watch()

//Barra de progreso
const optionsProgressBar = {
  color: '#000099',
  failedColor: '#FF0000',
  thickness: '4px',
  transition: {
    speed: '0.2s',
    opacity: '0.5s',
    termination: 300,
  },
  autoRevert: true,
  location: 'top',
  inverse: false,
}

//Filtros generales
const filters = {
  capitalize (value) {
    value = value.toString()
    return value.charAt(0).toUpperCase() + value.slice(1)
  },
  replace (st, rep, repWith) {
    const result = st.split(rep).join(repWith)
    return result;
  }
}

//Configuracion del app
const app = createApp(App)

app.config.globalProperties.servidorBackend = `http://localhost:5000/api`
app.config.globalProperties.filters = filters

app.component('font-awesome-icon', FontAwesomeIcon)
app.use(VueAxios, axios)
app.use(VueProgressBar, optionsProgressBar)
app.mount('#app')
