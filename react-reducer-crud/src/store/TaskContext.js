import { createContext, useReducer } from 'react';

const initialState = {
	tasks: [
		{
			id: '1',
			title: 'Task 1',
			description: 'Description 1',
			completed: false,
		},
		{
			id: '2',
			title: 'Task 2',
			description: 'Description 2',
			completed: false,
		},
	],
};

const initialContext = {
	tasks: [],
	addTask: (task) => {},
	updateTask: (id, task) => {},
	deleteTask: (id) => {},
};

export const TASK_ACTIONS = {
	ADD_TASK: 'ADD_TASK',
	UPDATE_TASK: 'UPDATE_TASK',
	DELETE_TASK: 'DELETE_TASK',
};

export const TaskContext = createContext(initialContext);

export default function TaskContextProvider({ children }) {
	const [taskState, dispatchTaskAction] = useReducer(taskReducer, initialState);

	function addTask(task) {
		dispatchTaskAction({ type: TASK_ACTIONS.ADD_TASK, payload: task });
	}

	function updateTask(id, task) {
		dispatchTaskAction({
			type: TASK_ACTIONS.UPDATE_TASK,
			payload: { id, task },
		});
	}

	function deleteTask(id) {
		dispatchTaskAction({ type: TASK_ACTIONS.DELETE_TASK, payload: id });
	}

	return (
		<TaskContext.Provider
			value={{
				tasks: taskState.tasks,
				addTask,
				updateTask,
				deleteTask,
			}}
		>
			{children}
		</TaskContext.Provider>
	);
}

function taskReducer(state, action) {
	switch (action.type) {
		case TASK_ACTIONS.ADD_TASK:
			return {
				...state,
				tasks: [...state.tasks, action.payload],
			};
		case TASK_ACTIONS.UPDATE_TASK:
			return {
				...state,
				tasks: state.tasks.map((task) =>
					task.id === action.payload.id ? action.payload.task : task
				),
			};
		case TASK_ACTIONS.DELETE_TASK:
			return {
				...state,
				tasks: state.tasks.filter((task) => task.id !== action.payload),
			};
		default:
			return state;
	}
}
