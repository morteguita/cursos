import { BrowserRouter, Routes, Route } from 'react-router-dom';

import './App.css';
import TaskForm from './components/TaskForm';
import TaskList from './components/TaskList';
import TaskContextProvider from './store/TaskContext';

function App() {
	return (
		<TaskContextProvider>
			<div className="bg-zinc-900 h-screen text-white">
				<div className="flex items-center justify-center h-full">
					<BrowserRouter>
						<Routes>
							<Route path="/" element={<TaskList />} />
							<Route path="/new" element={<TaskForm />} />
							<Route path="/edit/:id" element={<TaskForm />} />
						</Routes>
					</BrowserRouter>
				</div>
			</div>
		</TaskContextProvider>
	);
}

export default App;
