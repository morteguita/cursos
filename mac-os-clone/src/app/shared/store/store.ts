import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DESKTOP } from '../config/applications';
import { Folder } from '../models/folder';
import { FolderSelection } from '../models/folder-selection';
import { BaseStore } from './base-store';
import { State } from './state';

const initialState: State = {
    activeApplication: DESKTOP,
    selectedFolderIds: [],
    deletedFolderIds: [],
    folders: [
        {
            id: 1,
            title: 'desktop'
        },
        {
            id: 2,
            title: 'untitled',
            parentFolderId: 1
        },
        {
            id: 3,
            title: 'tutorials',
            parentFolderId: 1
        },
        {
            id: 4,
            title: 'assets',
            parentFolderId: 3
        }
    ]
};

@Injectable({providedIn: 'root'})
export class Store extends BaseStore {

    trashItemsCount$: Observable<number> = this.select((state) => {
        return state.deletedFolderIds.length;
    });

    desktopFolders$: Observable<Folder[]> = this.select((state) => {
        return state.folders
            .filter(f => f.parentFolderId === 1 && !state.deletedFolderIds.includes(f.id))
            .map(f => {
                return {
                    ...f,
                    selected: state.selectedFolderIds.includes(f.id)
                }
            });
    });

    tutorialFolders$: Observable<Folder[]> = this.select((state) => {
        return state.folders
            .filter(f => f.parentFolderId === 3 && !state.deletedFolderIds.includes(f.id))
            .map(f => {
                return {
                    ...f,
                    selected: state.selectedFolderIds.includes(f.id)
                }
            });
    });

    trashFolders$: Observable<Folder[]> = this.select((state) => {
        return state.folders
            .filter(f => state.deletedFolderIds.includes(f.id));
    });

    activeApplication$: Observable<string> = this.select((state) => {
        return state.activeApplication;
    });

    constructor() {
        super(initialState);
    }   

    setActiveApplication(activeAppId = DESKTOP) {
        this.setState({
            activeApplication: activeAppId  
        });
    }

    toggleFolder(folderSelection: FolderSelection) {
        if(folderSelection.selectedMultiple) {
            this.setState({
                selectedFolderIds: [
                    ...this.state.selectedFolderIds,
                    folderSelection.id
                ]
            });
        } else {
            this.setState({
                selectedFolderIds: [
                    folderSelection.id
                ]
            });
        }
    }

    unselectAllFolders() {
        this.setState({
            selectedFolderIds: []
        });
    }

    deleteSelectedFolders() {
        this.setState({
            deletedFolderIds: [
                ...this.state.deletedFolderIds,
                ...this.state.selectedFolderIds
            ]
        });
    }

    addNewFolder(): void {
        const milliseconds = new Date().getTime();
        const title = `New folder`;
        const countFolders = this.state.folders.filter(f => f.title.indexOf(title) >= 0).length;
        const newFolder: Folder = {
            id: milliseconds,
            title: countFolders == 0 ? title : `${title} (${countFolders + 1})`,
            parentFolderId: 1
        };
        this.setState({
            folders: [
                ...this.state.folders,
                newFolder
            ]
        });
    }
}