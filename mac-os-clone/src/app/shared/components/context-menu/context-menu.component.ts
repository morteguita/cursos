import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Store } from '../../store/store';

@Component({
  selector: 'app-context-menu',
  templateUrl: './context-menu.component.html',
  styleUrls: ['./context-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContextMenuComponent {

  @Input() target: any;

  items: MenuItem[] = [
    {
      label: 'New folder',
      command: () => this.store.addNewFolder()
    },
    {
      label: 'Get info'
    },
    {
      label: 'Change background desktop'
    },
    {
      label: 'Use stacks'
    },
    {
      label: 'Show view options'
    }
  ];

  constructor(private store: Store) { }

}
