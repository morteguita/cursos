import { ChangeDetectionStrategy, Component } from '@angular/core';
import { TerminalService } from 'primeng/terminal';
import { TerminalCommand } from 'src/app/shared/config/terminal-command';

@Component({
  selector: 'app-terminal',
  templateUrl: './terminal.component.html',
  styleUrls: ['./terminal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [TerminalService]
})
export class TerminalComponent {

  constructor(private terminalService: TerminalService) {
    this.terminalService.commandHandler.subscribe(command => {
      const response = this.getCommandResponse(command);
      this.terminalService.sendResponse(response);
    });
  }

  private getCommandResponse(command: string) {
    let response = '';
    switch (command.toUpperCase()) {
      case TerminalCommand.Author: response = 'Developer Thing'; break;
      case TerminalCommand.Ui: response = 'PrimeNG'; break;
      case TerminalCommand.Framework: response = 'Angular 14'; break;
      default: response = 'Unknown command'; break;
    }
    return `> ${response}`;
  }
}
