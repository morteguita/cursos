import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DesktopComponent } from './containers';

@NgModule({
  imports: [RouterModule.forChild([
    {
      path: '',
      component: DesktopComponent
    }
  ])],
  exports: [RouterModule]
})
export class ApplicationsRoutingModule { }
