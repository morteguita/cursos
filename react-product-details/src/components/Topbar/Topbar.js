import React from 'react';
import logo from '../../img/amazon-logo.png';
import classes from './Topbar.module.css';

const Topbar = (props) => {
  return (
    <header className="App-header">
      <nav className={classes.topbar}>
        <img src={logo} alt="Amazon Logo" />
      </nav>
    </header>
  );
}

export default Topbar;