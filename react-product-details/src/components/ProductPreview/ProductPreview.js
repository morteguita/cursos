import React from 'react';
import classes from './ProductPreview.module.css';

const ProductPreview = (props) => {
  const data = props.data;
  const productData = data.productData;
  const currentDate = new Date();
  const currentHour = currentDate.getHours().toString().padStart(2, '0');
  const currentMinute = currentDate.getMinutes().toString().padStart(2, '0');
  const selectedOption = productData.colorOptions.find(color => color.id === data.currentColorId);

  return (
    <div className={classes.productPreview}>
      <img src={selectedOption.imageUrl} alt={selectedOption.styleName} />
        {
          data.currentFeatureId === 1 ? 
            <div className={classes.timeSection}>
              <p>{currentHour} <span className={classes.timeSeparator}>:</span> {currentMinute}</p>
            </div>
            :
            <div className={classes.heartBeatSection}>
              <i className="fas fa-heartbeat"></i>
              <p>{productData.heartRate}</p>
            </div>
        }
    </div>
  );
}

export default ProductPreview;