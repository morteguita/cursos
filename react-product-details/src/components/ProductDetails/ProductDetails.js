import React from 'react';
import classes from './ProductDetails.module.css';

const ProductDetails = (props) => {
  const data = props.data;
  const productData = data.productData;
  const searchUrl = 'https://www.amazon.com/s?k=' + productData.title;

  const colorOptions = productData.colorOptions.map(item => {
    const classArr = [classes.productImage]

    if (item.id === data.currentColorId) {
      classArr.push(classes.productImageSelected)
    }

    return (
      <img key={item.id} className={classArr.join(' ')} src={item.imageUrl} alt={item.styleName} onClick={() => {props.onColorOptionClick(item.id)}} />
    )
  })

  const featureList = productData.featureList.map(item => {
    const classArr = [classes.featureItem]

    if (item.id === data.currentFeatureId) {
      classArr.push(classes.featureItemSelected)
    }

    return ( 
      <button key={item.id} className={classArr.join(' ')} onClick={() => {props.onFeatureItemClick(item.id)}}>{item.name}</button>
    )    
  })

  return (
    <div className={classes.productData}>
      <h1 className={classes.productTitle}>{productData.title}</h1>
      <p className={classes.productDescription}>{productData.description}</p>

      <h3 className={classes.sectionHeading}>Select Color</h3>
      <div>
        {colorOptions}
      </div>

      <h3 className={classes.sectionHeading}>Features</h3>
      <div>
        {featureList}
      </div>

      <a className={classes.primaryButton} href={searchUrl} target="_blank" rel="noreferrer">Buy Now</a>
    </div>
  );
}

export default ProductDetails;