const ProductData = {
  title: 'FitBit 23 - The Smartest Watch',
  description: 'FitBit 23 is a cutting-edge smartwatch that combines style and functionality. With precise timekeeping and advanced heart rate monitoring capabilities, it is the ultimate companion for your active lifestyle. Stay on top of your schedule while keeping a close eye on your heart rate, ensuring you achieve your fitness goals with ease. Embrace the future of wearable technology with the FitBit 23 and experience the perfect blend of innovation and convenience.',
  heartRate: 77,
  colorOptions: [
    {
      id: 1,
      styleName: 'Black Strap',
      imageUrl: 'https://imgur.com/iOeUBV7.png'
    },
    {
      id: 2,
      styleName: 'Red Strap',
      imageUrl: 'https://imgur.com/PTgQlim.png'
    },
    {
      id: 3,
      styleName: 'Blue Strap',
      imageUrl: 'https://imgur.com/Mplj1YR.png'
    },
    {
      id: 4,
      styleName: 'Purple Strap',
      imageUrl: 'https://imgur.com/xSIK4M8.png'
    },
  ],
  featureList: [
    {
      id: 1,
      name: 'Time'
    },
    {
      id: 2,
      name: 'Heart Rate'
    }    
  ]
}

Object.freeze(ProductData); //This line of code just makes your object as a constant. No values can be updated.

export default ProductData;