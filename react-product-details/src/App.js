import React, {Component} from 'react';
import classes from './App.module.css';
import ProductData from './utils/ProductData';
import Topbar from './components/Topbar/Topbar';
import ProductPreview from './components/ProductPreview/ProductPreview';
import ProductDetails from './components/ProductDetails/ProductDetails';

class App extends Component {
  state = {
    productData: ProductData,
    currentColorId: 1,
    currentFeatureId: 1
  }

  onColorOptionClick = (id) => {
    this.setState({currentColorId: id})
  }

  onFeatureItemClick = (id) => {
    this.setState({currentFeatureId: id})
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.state.currentColorId !== nextState.currentColorId || this.state.currentFeatureId !== nextState.currentFeatureId;
  }

  render () {
    return (
      <div className="App">
        <Topbar />
        <div className={classes.mainContainer}>
          <ProductPreview data={this.state} />
          <ProductDetails data={this.state} onColorOptionClick={this.onColorOptionClick} onFeatureItemClick={this.onFeatureItemClick} />
        </div>
      </div>
    )
  }
}

export default App;
