import React, { Component } from 'react';
import './App.css';
import BlogCard from './BlogCard';
import { isArrayEmpty as checkIfArrayIsEmpty } from './Utils';

class App extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      showBlogs: true,
      blogArr: [
        {
          id: 1,
          title: `New Adventures`,
          description: `Immerse Yourself in the Action with the New Adventure Title: Discover a world full of excitement and challenges in our latest video game release. Explore breathtaking landscapes, face formidable enemies, and unlock powerful abilities as you dive into this incredible adventure. Are you ready to accept the challenge and become the legendary hero?`,
          likeCount: 0
        },
        {
          id: 2,
          title: `Be the Ultimate Hero`,
          description: `Get Ready for Battle in the Most Anticipated Action Game of the Year! Master your combat skills, recruit powerful allies, and take on challenging boss battles in this epic storyline. With cutting-edge graphics and immersive gameplay, this game will transport you to a world full of thrills and adrenaline. Do you have what it takes to be the ultimate hero?`,
          likeCount: 0
        },
        {
          id: 3,
          title: `Explore a New Universe!`,
          description: `Unleash Your Imagination in the Most Captivating RPG Game: Explore a universe of magic and fantasy in the most captivating role-playing game. Venture into mysterious lands, complete epic quests, and form alliances with legendary characters in this unique adventure. Immerse yourself in a captivating narrative, enhance your skills, and unlock powerful spells as you delve into a world full of secrets and dangers. Are you prepared to embark on this thrilling odyssey?`,
          likeCount: 0
        }
      ]
    }

    console.log('Constructor');
  }

  onHideButtonClick = () => {
    this.setState((prevState, prevProps) => {
      return { showBlogs: !prevState.showBlogs }
    })
  }

  onLikeButtonClick = (id) => {
    const updatedBlogList = this.state.blogArr;
    const blog = updatedBlogList.find(blog => blog.id === id);
    blog.likeCount = blog.likeCount + 1;

    this.setState((prevState, prevProps) => {
      return { blogArr: updatedBlogList }
    })
  }

  onDislikeButtonClick = (id) => {
    const updatedBlogList = this.state.blogArr;
    const blog = updatedBlogList.find(blog => blog.id === id);
    blog.likeCount = blog.likeCount > 0 ? blog.likeCount - 1 : 0;

    this.setState((prevState, prevProps) => {
      return { blogArr: updatedBlogList }
    })
  }

  render() {
    console.log('Render');
    const blogCards = checkIfArrayIsEmpty(this.state.blogArr)
      ? []
      : this.state.blogArr.map((card, index) => {
          return (
            <BlogCard
              key={index}
              id={card.id}
              title={card.title}
              description={card.description}
              likeCount={card.likeCount}
              onLikeButtonClick={() => {this.onLikeButtonClick(card.id)}}
              onDislikeButtonClick={() => {this.onDislikeButtonClick(card.id)}}
            />
          )
        })

    return (
      <div className="App">
        <button className="App-button" onClick={this.onHideButtonClick}>
          { this.state.showBlogs ? 'Hide list' : 'Show list' }
        </button>
        <br />
        {
          this.state.showBlogs ? blogCards : null
        }
      </div>
    )
  }

  shouldComponentUpdate() {
    console.log('Should Component Update');
    return true;
  }

  componentDidMount() {
    console.log('Component Did Mount');
  }

  componentWillUnmount() {
    console.log('Component Will Unmount');
  }
}

export default App;
