import React from 'react';
import classes from './BlogCard.module.css';

const BlogCard = (props) => {
  console.log('BlogCard Render')
  return (
    <div className={classes.NewBlogCard} key={props.id}>
      <h3>{props.title}</h3>
      <p>{props.description}</p>
      <p>Like count: <span className={classes.LikeCount}>{props.likeCount}</span></p>
      <button onClick={props.onLikeButtonClick}>Like</button> 
      { props.likeCount > 0 ? <button onClick={props.onDislikeButtonClick}>Dislike</button> : null }
    </div>
  );
}

export default BlogCard;
