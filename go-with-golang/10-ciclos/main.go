package main

import "fmt"

func main() {
	fmt.Println("Days in golang")

	days := []string{"Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"}
	fmt.Println(days)

	for d := 0; d < len(days); d++ {
		fmt.Println(days[d])
	}

	fmt.Println("===============================================")

	for i := range days {
		fmt.Println(days[i])
	}

	fmt.Println("===============================================")

	for index, value := range days {
		fmt.Printf("El indice es %v y el valor es %v \n", index, value)
	}

	fmt.Println("===============================================")

	for _, value := range days {
		fmt.Printf("El día es %v \n", value)
	}

	fmt.Println("===============================================")

	rogueValue := 1

	for rogueValue <= 10 {
		fmt.Printf("El valor es %v \n", rogueValue)
		rogueValue++
	}
}
