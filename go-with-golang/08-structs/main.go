package main

import "fmt"

type User struct {
	Name   string
	Email  string
	Status bool
	Age    int
}

func main() {
	//No hay herencia, ni super ni padres
	fmt.Println("Structs in golang")

	persona := User{"Manuel Ortega", "morteguita@hotmail.com", true, 36}
	fmt.Println(persona)
	fmt.Printf("Detalles del usuario son: %+v \n", persona)
	fmt.Printf("El nombre es %v y el correo %v \n", persona.Name, persona.Email)
}
