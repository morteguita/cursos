package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

const ruta = "./logFile.txt"

func main() {
	fmt.Println("Archivos en golang")

	content := "This needs to go in a file - LearnCodeOnline.in"

	file, err := os.Create(ruta)
	checkNilErr(err)

	defer file.Close()

	length, err := io.WriteString(file, content)
	checkNilErr(err)

	fmt.Println("Length is:", length)
	readFile()
}

func readFile() {
	dataByte, err := ioutil.ReadFile(ruta)
	checkNilErr(err)

	fmt.Println("Text bytes are:", dataByte)
	fmt.Println("Text content is:", string(dataByte))
}

func checkNilErr(err error) {
	if err != nil {
		panic(err)
	}
}
