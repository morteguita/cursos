package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	//Lee desde consola
	welcome := "Welcome to user input"
	fmt.Println(welcome)

	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Enter the rating for our Pizza:")

	//Valor, error
	input, _ := reader.ReadString('\n')

	fmt.Println("Thanks for the rating:", input)
	fmt.Printf("Type of this rating is %T \n", input)
}
