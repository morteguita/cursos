package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

//Modelos
type Course struct {
	//json:"-" se usa para no mostrar el atributo en json
	CourseId    string  `json:"courseid"`
	CourseName  string  `json:"coursename"`
	CoursePrice int     `json:"price"`
	Author      *Author `json:"author"`
}

type Author struct {
	Fullname string `json:"fullname"`
	Website  string `json:"website"`
}

//DB falsa
var courses []Course

//Middleware - helper
func (c *Course) IsEmpty() bool {
	return c.CourseId == "" && c.CourseName == ""
}

//Controladores
func serveHome(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("<h1>Welcome to API by LearnCodeOnline</h1>"))
}

func getAllCourses(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Get ALL courses")
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(courses)
}

func getOneCourse(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Get ONE course")
	w.Header().Set("Content-Type", "application/json")

	//Obtener los parametros de la peticion
	params := mux.Vars(r)

	//Iterar los cursos para encontrar el del Id correspondiente
	for _, course := range courses {
		if params["id"] == course.CourseId {
			json.NewEncoder(w).Encode(course)
			return
		}
	}

	json.NewEncoder(w).Encode("No course found for the given ID")
}

func createOneCourse(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Create ONE course")
	w.Header().Set("Content-Type", "application/json")

	//Si el body viene vacio
	if r.Body == nil {
		json.NewEncoder(w).Encode("Please send some data")
		return
	}

	var course Course
	_ = json.NewDecoder(r.Body).Decode(&course)

	if course.IsEmpty() {
		json.NewEncoder(w).Encode("No data inside JSON")
		return
	}

	//Verificar que solo haya un nombre de cada curso
	for _, courseRange := range courses {
		if course.CourseName == courseRange.CourseName {
			json.NewEncoder(w).Encode("The course " + course.CourseName + " already exists")
			return
		}
	}

	rand.Seed(time.Now().UnixNano())
	course.CourseId = strconv.Itoa(rand.Intn(100))
	courses = append(courses, course)
	json.NewEncoder(w).Encode(course)
}

func updateOneCourse(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Update ONE course")
	w.Header().Set("Content-Type", "application/json")

	//Obtener los parametros de la peticion
	params := mux.Vars(r)

	//Iterar los cursos para encontrar el del Id correspondiente, borrarlo y adicionar el nuevo
	for index, course := range courses {
		if params["id"] == course.CourseId {
			courses = append(courses[:index], courses[index+1:]...) //Se quita el elemento del indice
			var newCourse Course
			_ = json.NewDecoder(r.Body).Decode(&newCourse)
			newCourse.CourseId = params["id"]
			courses = append(courses, newCourse)
			json.NewEncoder(w).Encode(newCourse)
			return
		}
	}

	json.NewEncoder(w).Encode("No course found for the given ID")
}

func deleteOneCourse(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Delete ONE course")
	w.Header().Set("Content-Type", "application/json")

	//Obtener los parametros de la peticion
	params := mux.Vars(r)

	//Iterar los cursos para encontrar el del Id correspondiente y borrarlo
	for index, course := range courses {
		if params["id"] == course.CourseId {
			courses = append(courses[:index], courses[index+1:]...) //Se quita el elemento del indice
			json.NewEncoder(w).Encode("Course with id " + params["id"] + " deleted")
			return
		}
	}

	json.NewEncoder(w).Encode("No course found for the given ID")
}

//Funcion principal
func main() {
	fmt.Println("API - LearnCodeOnline.in")
	r := mux.NewRouter()

	courses = append(courses, Course{CourseId: "2", CourseName: "ReactJS", CoursePrice: 299, Author: &Author{Fullname: "Hitesh Choudhary", Website: "lco.dev"}})
	courses = append(courses, Course{CourseId: "4", CourseName: "MERN Stack", CoursePrice: 199, Author: &Author{Fullname: "Hitesh Choudhary", Website: "lco.dev"}})

	//Rutas
	r.HandleFunc("/", serveHome).Methods("GET")
	r.HandleFunc("/courses", getAllCourses).Methods("GET")
	r.HandleFunc("/course/{id}", getOneCourse).Methods("GET")
	r.HandleFunc("/course", createOneCourse).Methods("POST")
	r.HandleFunc("/course/{id}", updateOneCourse).Methods("PUT")
	r.HandleFunc("/course/{id}", deleteOneCourse).Methods("DELETE")

	//Iniciar el servidor
	log.Fatal(http.ListenAndServe(":4000", r))
}
