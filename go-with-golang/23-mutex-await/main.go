package main

import (
	"fmt"
	"sync"
)

func main() {
	fmt.Println("Race condition - LearnCodeOnline.in")

	wg := &sync.WaitGroup{}
	mut := &sync.RWMutex{}

	var score = []int{0}

	wg.Add(3)
	go func(w *sync.WaitGroup, m *sync.RWMutex) {
		fmt.Println("One R")
		m.Lock()
		score = append(score, 1)
		m.Unlock()
		w.Done()
	}(wg, mut)

	//wg.Add(1)
	go func(w *sync.WaitGroup, m *sync.RWMutex) {
		fmt.Println("Two R")
		m.Lock()
		score = append(score, 2)
		m.Unlock()
		w.Done()
	}(wg, mut)

	go func(w *sync.WaitGroup, m *sync.RWMutex) {
		fmt.Println("Three R")
		m.Lock()
		score = append(score, 3)
		m.Unlock()
		w.Done()
	}(wg, mut)

	go func(w *sync.WaitGroup, m *sync.RWMutex) {
		fmt.Println("Four R")
		m.RLock()
		fmt.Println(score)
		m.RUnlock()
		w.Done()
	}(wg, mut)

	wg.Wait()
	fmt.Println(score)
}
