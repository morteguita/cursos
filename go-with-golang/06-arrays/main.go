package main

import (
	"fmt"
	"sort"
)

func main() {
	fmt.Println("Welcome to arrays in golang")

	//Arrays
	var fruitList [4]string
	fruitList[0] = "Apple"
	fruitList[1] = "Orange"
	fruitList[2] = "Peach"
	fruitList[3] = "Banana"

	fmt.Println("Fruit list is: ", fruitList)
	fmt.Println("Fruit list is: ", len(fruitList))

	var vegList = [5]string{"potato", "beans", "mushroom"}
	fmt.Println("Fruit list is: ", vegList)
	fmt.Println("Vegy list is: ", len(vegList))
	fmt.Println("==============================================")

	//Slices
	var fruitLst = []string{"Apple", "Orange", "Peach"}
	fmt.Printf("Type of fruitLst is %T \n", fruitLst)

	fruitLst = append(fruitLst, "Coconut", "Mango", "Banana")
	fmt.Println("Fruit list 1 is: ", fruitLst[2:5]) //Desde m hasta n-1
	fmt.Println("Fruit list 2 is: ", fruitLst[:3])

	highScores := make([]int, 4)
	highScores[0] = 234
	highScores[1] = 945
	highScores[2] = 465
	highScores[3] = 867

	highScores = append(highScores, 777)

	sort.Ints(highScores)
	fmt.Println(highScores)
	fmt.Println(sort.IntsAreSorted(highScores))
	fmt.Println("==============================================")

	//Quitar un elemento segun su indice
	var courses = []string{"reactjs", "javascript", "swift", "python", "ruby", "golang"}
	fmt.Println(courses)

	var index int = 2
	courses = append(courses[:index], courses[index+1:]...)
	fmt.Println(courses)
	courses = append(courses[:index], courses[index+1:]...)
	fmt.Println(courses)
}
