## Lista de comandos

```
go mod tidy
go mod verify
go list
go list all
go list -m all
go list -m -versions github.com/gorilla/mux
go mod why github.com/gorilla/mux
go mod graph
go mod edit -go 1.16
go mod edit -module "version"
go mod vendor
```