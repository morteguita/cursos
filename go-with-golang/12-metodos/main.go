package main

import "fmt"

type User struct {
	Name   string
	Email  string
	Status bool
	Age    int
}

func (u User) GetStatus() {
	fmt.Println("Is user active:", u.Status)
}

//El * es para el puntero, para acceder directo a la posicion del objeto en memoria y manipularlo directamente
func (u *User) NewMail(mail string) {
	u.Email = mail
	fmt.Println("Email of this user is:", u.Email)
}

func main() {
	fmt.Println("Methods in golang")

	persona := User{"Manuel Ortega", "morteguita@hotmail.com", true, 36}
	fmt.Println(persona)
	fmt.Printf("Detalles del usuario son: %+v \n", persona)
	fmt.Printf("El nombre es %v y el correo %v \n", persona.Name, persona.Email)

	persona.GetStatus()
	persona.NewMail("test@golang.com")
	fmt.Println(persona)
}
