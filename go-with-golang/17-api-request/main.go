package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

const hostUrl = "http://localhost:8000"

func main() {
	fmt.Println("Welcome to web verb video")

	PerformGetRequest()
	fmt.Println("============================================================================================================")
	PerformPostJsonRequest()
	fmt.Println("============================================================================================================")
	PerformPostFormRequest()
}

func PerformPostFormRequest() {
	const myUrl = hostUrl + "/postform"

	//Formdata
	data := url.Values{}
	data.Add("firstName", "Alejandro")
	data.Add("lastName", "Ortega")
	data.Add("email", "aorta@blood.org")

	response, err := http.PostForm(myUrl, data)
	checkNilErr(err)
	defer response.Body.Close()

	content, _ := ioutil.ReadAll(response.Body)
	fmt.Println(content)
	fmt.Println(string(content))
}

func PerformPostJsonRequest() {
	const myUrl = hostUrl + "/post"

	requestBody := strings.NewReader(`
		{
			"coursename": "Go with golang",
			"price": 0,
			"platform": "learncodeonline.in"
		}
	`)

	response, err := http.Post(myUrl, "application/json", requestBody)
	checkNilErr(err)
	defer response.Body.Close()

	content, _ := ioutil.ReadAll(response.Body)
	fmt.Println(content)
	fmt.Println(string(content))
}

func PerformGetRequest() {
	const myUrl = hostUrl + "/get"

	response, err := http.Get(myUrl)
	checkNilErr(err)
	defer response.Body.Close()

	fmt.Println("Status code:", response.StatusCode)
	fmt.Println("Content length:", response.ContentLength)

	var responseString strings.Builder
	content, _ := ioutil.ReadAll(response.Body)
	byteCount, _ := responseString.Write(content)

	fmt.Println(content)
	fmt.Println(string(content))

	fmt.Println("Byte count is:", byteCount)
	fmt.Println(responseString.String())
}

func checkNilErr(err error) {
	if err != nil {
		panic(err)
	}
}
