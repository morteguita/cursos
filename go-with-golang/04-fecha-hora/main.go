package main

import (
	"fmt"
	"time"
)

func main() {
	//Fecha y hora
	fmt.Println("Welcome to Time Study in golang")

	presentTime := time.Now()

	fmt.Println(presentTime)
	fmt.Println(presentTime.Format("2006-01-02")) //Formato YYYY-MM-DD
	fmt.Println(presentTime.Format("2006-01-02 15:04:05 Monday"))

	createdDate := time.Date(1985, time.August, 18, 21, 50, 0, 0, time.UTC)
	fmt.Println(createdDate)
	fmt.Println(createdDate.Format("2006-01-02 15:04:05 Monday"))
}
