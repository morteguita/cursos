package main

import (
	"fmt"
	"sync"
)

func main() {
	fmt.Println("Channels in golang")

	myCh := make(chan int, 1)
	wg := &sync.WaitGroup{}

	wg.Add(2)

	//goroutine para READ ONLY
	go func(ch <-chan int, wg *sync.WaitGroup) {
		defer wg.Done()
		val, isChannelOpen := <-myCh
		fmt.Println(val, isChannelOpen)
	}(myCh, wg)

	//goroutine para SEND ONLY
	go func(ch chan<- int, wg *sync.WaitGroup) {
		defer wg.Done()
		myCh <- 5
		close(myCh)
	}(myCh, wg)

	wg.Wait()
}
