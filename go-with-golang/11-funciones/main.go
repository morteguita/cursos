package main

import "fmt"

func main() {
	fmt.Println("Funciones en golang")
	greeter()

	result := adder(3, 5)
	fmt.Println("Result is:", result)

	proResult := proAdder(3, 5, 7, 9, 11)
	fmt.Println("Pro Result is:", proResult)

	proResult, message := proAdderMessage(3, 5, 7, 9, 11)
	fmt.Println(message, ":", proResult)
}

func adder(val1, val2 int) int {
	return val1 + val2
}

func greeter() {
	fmt.Println("Namaste from golang")
}

func proAdder(values ...int) int {
	total := 0

	for _, val := range values {
		total += val
	}

	return total
}

func proAdderMessage(values ...int) (int, string) {
	total := 0

	for _, val := range values {
		total += val
	}

	return total, "Hi from ProResult with Message"
}
