package main

import (
	"fmt"
	"net/url"
)

const urlPagina string = "https://lco.dev:3000/learn?coursename=golang&paymentid=123456"

func main() {
	fmt.Println("Handling URLs in golang")
	fmt.Println(urlPagina)

	//Parsear la url
	result, _ := url.Parse(urlPagina)

	fmt.Println(result.Scheme)
	fmt.Println(result.Host)
	fmt.Println(result.Path)
	fmt.Println(result.Port())
	fmt.Println(result.RawQuery)

	qparams := result.Query()

	fmt.Printf("The type of query params are: %T\n", qparams)
	fmt.Println(qparams["coursename"])

	for ind, val := range qparams {
		fmt.Println("Key is:", ind)
		fmt.Println("Param is:", val)
	}

	//Construir una url con la struct de URL
	partsOfUrl := &url.URL{
		Scheme:   "https",
		Host:     "lco.dev",
		Path:     "/tutcss",
		RawQuery: "user=mortega",
	}
	anotherURL := partsOfUrl.String()

	fmt.Println(anotherURL)
}
