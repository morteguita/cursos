package main

import (
	"encoding/json"
	"fmt"
)

type course struct {
	Name     string   `json:"coursename"`
	Price    int      `json:"price"`
	Platform string   `json:"website"`
	Password string   `json:"-"`
	Tags     []string `json:"tags,omitempty"`
}

func main() {
	fmt.Println("Welcome to JSON")
	EncodeJson()
	fmt.Println("==========================================================================")
	DecodeJson()
}

func EncodeJson() {
	lcoCourses := []course{
		{"ReactJS Bootcamp", 299, "Learncodeonline.in", "abc123", []string{"web-dev", "react", "JS"}},
		{"MERN Bootcamp", 199, "Learncodeonline.in", "def456", []string{"full-stack", "MongoDB", "Express", "React", "Node"}},
		{"Angular Bootcamp", 399, "Learncodeonline.in", "ghi789", nil},
	}

	finalJson, err := json.MarshalIndent(lcoCourses, "", "\t") //Returns the JSON encoding (https://pkg.go.dev/encoding/json#Marshal)
	checkNilErr(err)

	fmt.Println(string(finalJson))
}

func DecodeJson() {
	jsonDataFromWeb := []byte(`
	{
		"coursename": "ReactJS Bootcamp",
		"price": 299,
		"website": "Learncodeonline.in",
		"tags": ["web-dev","react","JS"]
	}`)

	var lcoCourse course
	checkValid := json.Valid(jsonDataFromWeb)

	if checkValid {
		fmt.Println("JSON is valid")
		json.Unmarshal(jsonDataFromWeb, &lcoCourse)
		fmt.Printf("%#v\n", lcoCourse)
	} else {
		fmt.Println("JSON is NOT valid")
	}

	//Adicionar datos al objeto
	var myOnlineData map[string]interface{}
	json.Unmarshal(jsonDataFromWeb, &myOnlineData)
	fmt.Printf("%#v\n", myOnlineData)

	for k, v := range myOnlineData {
		fmt.Printf("Key is %v, value is %v and type is %T\n", k, v, v)
	}
}

func checkNilErr(err error) {
	if err != nil {
		panic(err)
	}
}
