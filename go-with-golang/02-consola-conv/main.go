package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	//Lectura de datos de consola y conversion
	fmt.Println("Welcome to our Pizza App")
	fmt.Println("Please rate our pizza between 1 and 5")

	reader := bufio.NewReader(os.Stdin)
	input, err := reader.ReadString('\n')

	if err != nil {
		fmt.Println("Error en ReadString", err)
	} else {
		numRating, err2 := strconv.ParseFloat(strings.TrimSpace(input), 64) //Se quitan los espacios para evitar el error

		if err2 != nil {
			fmt.Println("Error en ParseFloat", err2)
		} else {
			fmt.Println("Thanks for the rating, ", numRating)
		}
	}
}
