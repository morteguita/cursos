package main

import "fmt"

func main() {
	fmt.Println("Welcome to pointers")

	var ptr *int
	fmt.Println("Value of pointer is", ptr)

	myNumber := 36
	var pntr = &myNumber

	fmt.Println("Value of pointer address is", pntr)
	fmt.Println("Value of pointer is", *pntr)

	*pntr = *pntr * 2
	fmt.Println("Value of pointer MULT is", *pntr)
	fmt.Println("New value is", myNumber)
}
