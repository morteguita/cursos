package main

import "fmt"

func main() {
	//La sentencia defer marca la llamada a la función “deferreada” como llamada a ejecutar justo antes de finalizar la ejecución de la función en dónde se haya ejecutado la sentencia.
	defer fmt.Println("world!")
	defer fmt.Println("goodbye")
	fmt.Println("Hello")
	myDefer() //Primero van los numero porque la funcion no esta defer, solo su contenido, que se ejecuta al terminar la funcion
	defer fmt.Println("golang,")
}

func myDefer() {
	for i := 0; i <= 5; i++ {
		defer fmt.Println(i)
	}
}
