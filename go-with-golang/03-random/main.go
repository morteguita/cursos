package main

import (
	"fmt"
	"math/big"

	//"math/rand"
	"crypto/rand"
)

func main() {
	//Numeros aleatorios
	fmt.Println("Welcome to math in golang")

	//Con "math/rand"
	/*rand.Seed(time.Now().UnixNano()) //La semilla hace que salga un aleatorio diferente cada vez
	fmt.Println(rand.Intn(5) + 1)*/

	//Con "crypto/rand"
	myRandom, err := rand.Int(rand.Reader, big.NewInt(5))

	if err != nil {
		fmt.Println("Error en rand.Int", err)
	} else {
		fmt.Println(myRandom.Int64() + 1)
	}
}
