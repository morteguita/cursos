package main

import (
	"fmt"
	"net/http"

	"gitlab.com/morteguita/cursos/gowithgolang/mongoapi/router"
)

func main() {
	fmt.Println("MongoDB API")
	fmt.Println("Server initialized...")

	r := router.Router()
	http.ListenAndServe(":4000", r)
}
