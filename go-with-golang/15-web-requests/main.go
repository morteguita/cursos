package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

const urlPagina = "https://lco.dev"

func main() {
	fmt.Println("Web requests")

	response, err := http.Get(urlPagina)
	checkNilErr(err)
	defer response.Body.Close() //Siempre se debe cerrar la conexion

	fmt.Printf("Response is of type: %T\n", response)
	fmt.Println()

	dataBytes, err := ioutil.ReadAll(response.Body)
	checkNilErr(err)

	content := string(dataBytes)
	fmt.Println(content)
}

func checkNilErr(err error) {
	if err != nil {
		panic(err)
	}
}
