export interface LoginPayload {
  email: string,
  password: string,
}

export interface RegisterPayload {
  name: string,
  email: string,
  password: string,
  password_confirmation: string
}

export interface User {
  id: number,
  name: string,
  email: string,
  email_verified_at?: Date,
  two_factor_secret?: string,
  two_factor_recovery_codes?: number,
  two_factor_confirmed_at?: Date,
  remember_token?: string,
  created_at: Date,
  updated_at: Date,
}

export interface Link {
  id: number,
  short_link: string,
  full_link: string,
  views: number,
  created_at: Date,
  updated_at: Date,
}

export interface ErrorResponse {
  message: string,
  errors: Record<string, string[]>,
}

interface PageLink {
  id: number,
  url: string | null,
  label: string,
  active: boolean,
}

export interface PaginatedResponse<T> {
  current_page: number,
  data: T[],
  first_page_url: string | null,
  from: number,
  last_page: number,
  last_page_url: string | null,
  links: PageLink[],
  next_page_url: string | null,
  path: string,
  per_page: number,
  prev_page_url: string | null,
  to: number,
  total: number,
}