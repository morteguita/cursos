import 'dotenv/config'
import http from 'http'
import { app, APP_PORT } from './app.js'

const server = http.createServer(app)
server.listen(APP_PORT, () => {
    console.log(`Server ready at http://localhost:${APP_PORT}`)
})