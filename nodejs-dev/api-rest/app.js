import express from 'express'
import helmet from 'helmet'
import cors from 'cors'
import morgan from 'morgan'
import cookieParser from 'cookie-parser'

const APP_PORT = process.env.PORT || 3000
const app = express()

const corsOptions = {
    origin: process.env.NODE_ENV === 'development' ? `http://localhost:${APP_PORT}` : `informativo.dev`,
    credentials: true
}

app.use(helmet())
app.use(cors(corsOptions))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(morgan('combined'))
app.use(cookieParser())

app.get('/', (req,  res) => {
    res.send('Hello, world!')
})

export {app, APP_PORT}