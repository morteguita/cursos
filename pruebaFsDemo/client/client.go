package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"
)

const (
	CONN_HOST = "localhost"
	CONN_PORT = "8888"
	CONN_TYPE = "tcp"
)

func main() {
	conn, err := net.Dial(CONN_TYPE, CONN_HOST+":"+CONN_PORT)
	if err != nil {
		log.Fatal(err)
	}

	mainRoutine := make(chan struct{})
	go handleConnection(conn, mainRoutine)

	if _, err := io.Copy(conn, os.Stdin); err != nil {
		log.Fatal(err)
	}

	conn.Close()
	<-mainRoutine
}

//Maneja el hilo de la conexion
func handleConnection(conn net.Conn, mainRoutine chan struct{}) {
	fmt.Print(">> ")
	io.Copy(os.Stdout, conn)

	//Llama a la goroutine
	mainRoutine <- struct{}{}
}
