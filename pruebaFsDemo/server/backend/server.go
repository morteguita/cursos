package main

import (
	"bufio"
	"fmt"
	"log"
	"net"

	"github.com/mortega7/pruebaFs/server/backend/controllers"
	"github.com/mortega7/pruebaFs/server/backend/models"
)

const (
	CONN_HOST = "localhost"
	CONN_PORT = "8888"
	CONN_TYPE = "tcp"
)

func main() {
	listen, err := net.Listen(CONN_TYPE, CONN_HOST+":"+CONN_PORT)
	if err != nil {
		log.Fatal(err)
	}

	controllers.Channels = []models.ChannelRoom{
		{Name: "channel-1"},
		{Name: "channel-2"},
		{Name: "channel-3"},
	}

	go broadcaster()

	for {
		conn, err := listen.Accept()
		if err != nil {
			log.Print(err)
			continue
		}

		go handle(conn)
	}

}

func handle(conn net.Conn) {
	defer conn.Close()

	//Se crea el usuario
	newUser := models.User{
		Address: conn.RemoteAddr().String(),
		Conn:    conn,
	}
	controllers.Users = append(controllers.Users, newUser)
	fmt.Println("Nuevo Usuario:", newUser.Address)

	//Se leen los comandos enviados por los clientes
	input := bufio.NewScanner(conn)
	for input.Scan() {
		//fmt.Println("Usuario Pointer:", &user)
		messageToOwnUser, messageToOtherUsers := controllers.DecodeCommand(input.Text(), newUser.Address)
		user := controllers.FindUserByAddress(newUser.Address)

		if messageToOwnUser != "" {
			controllers.UserMessages <- newMessage(messageToOwnUser, *user)
		}
		if messageToOtherUsers != "" {
			controllers.Messages <- newMessage(messageToOtherUsers, *user)
		}
	}

	//Se quita el usuario desconectado
	for i, u := range controllers.Users {
		if u.Address == conn.RemoteAddr().String() {
			controllers.Users = append(controllers.Users[:i], controllers.Users[i+1:]...)
			break
		}
	}
}

//Se crea un nuevo mensaje
func newMessage(msg string, user models.User) models.Message {
	message := models.Message{
		Text:    "$ " + msg,
		Address: user.Conn.RemoteAddr().String(),
		Channel: user.Channel,
	}
	return message
}

//Envia el mensaje a los otros usuarios que esten en el mismo canal
func broadcaster() {
	for {
		select {
		case msg := <-controllers.UserMessages:
			//Mensajes para el mismo usuario
			for _, u := range controllers.Users {
				if msg.Address == u.Conn.RemoteAddr().String() {
					fmt.Fprintln(u.Conn, msg.Text)
					fmt.Fprint(u.Conn, ">> ")
				}
			}
		case msg := <-controllers.Messages:
			for _, u := range controllers.Users {
				//Mismo usuario, no envia el mensaje
				if msg.Address == u.Conn.RemoteAddr().String() {
					fmt.Fprint(u.Conn, ">> ")
					continue
				}

				//Usuarios del mismo canal, envia el mensaje
				if msg.Channel.Name == u.Channel.Name {
					fmt.Fprintln(u.Conn, "\n"+msg.Text)
					fmt.Fprint(u.Conn, ">> ")
				}
			}
		}
	}
}
